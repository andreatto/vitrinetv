<?php

class Campanha extends Controller {
    
    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
        
    }

    public function index() {
        $this->View->arrListUrl = $this->model->listing();
        $this->View->render('campanha/index');
    }
    
    public function form() {
        $this->View->dataUrl = $this->model->form();
        $this->View->render('campanha/form');
    }
    
    public function formEdit($param) {
        //echo 'id: '.$param;
        $this->View->dataUrl = $this->model->formEdit($param);
        $this->View->render('campanha/form');
    }
    
    public function inserir() {
        $this->View->dataUrl = $this->model->insert();
        $this->View->render('campanha/form');
    }
    
    public function editar($param) {
        $this->View->dataUrl = $this->model->edit($param);
        $this->View->arrListUrl = $this->model->listing();
        $this->View->render('campanha/index');
    }
    
    public function delete($param) {
        $this->View->dataUrl = $this->model->delete($param);
        $this->View->arrListUrl = $this->model->listing();
        $this->View->render('campanha/index');
    }
}