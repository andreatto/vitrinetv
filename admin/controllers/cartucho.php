<?php

class Cartucho extends Controller {

    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
    }

    public function index($param) {
        //$tela = $this->model->pegarTela($param);
        //$this->View->dataUrl = $this->model->listing($tela);
        //$this->View->dataCart = $this->model->myCartucho($param);
        $this->View->dataUrl = $this->model->listingProd();
        $this->View->dataCart = $this->model->getCartucho($param);
        $this->View->render('cartucho/index');
    }

    public function form($param) {
        $this->View->dataCart = $this->model->cartucho(); 
        $this->View->dataCartBy = $this->model->cartuchoBy(); 
        $this->View->dataUrl = $this->model->form($param);
        $this->View->render('modulo/form');
    }

    public function inserir($param) {
        $this->model->insert($param);
        //$this->model->insertRelCart($param);
        //$this->View->dataCart = $this->model->cartucho(); 
        //$this->View->dataCartBy = $this->model->cartuchoBy(); 
        //$this->View->dataUrl = $this->model->form($param);
        $this->View->render('cartucho/index');
    }

    public function formEdit($param) {
        $this->View->dataUrl = $this->model->formEdit($param);
        $this->View->dataRelCart = $this->model->getMyCartucho($param);
        $this->View->dataCart = $this->model->cartucho(); 
        $this->View->dataCartBy = $this->model->cartuchoBy();
        $this->View->render('modulo/form');
    }

    public function editar($param) {
        $tela = $this->model->edit($param);
        $this->model->deleteRelCart($param);
        $this->model->editRelCart($param);
        $this->View->dataUrl = $this->model->listing($tela[0]);
        $this->View->render('modulo/index');
    }

    public function delete($param) {
        $tela = $this->model->pegarTela($param);
        $this->model->delete($param);
        $this->model->deleteRelCart($param);
        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->render('modulo/index');
    }
}
    