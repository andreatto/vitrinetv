<?php

class Customer extends Controller
{

    public function __construct()
    {
        parent::__construct();

        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        }
    }
    /*
    public function index() {
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('customer/index');
    }
     */
    public function form()
    {
        $this->View->dataUrl = $this->model->form();
        $this->View->render('customer/form');
    }


    public function formEdit()
    {
        $this->View->dataUrl = $this->model->formEdit();
        $this->View->render('customer/form');
    }

    public function editar($param)
    {
        $arr = $this->model->edit($param);
        $this->model->editImg($arr);
        $this->View->dataUrl = $this->model->formEdit();
        $this->View->render('customer/form');
    }

    /*
    public function inserir() {
        $this->View->dataUrl = $this->model->insert();
        $this->View->render('customer/form');
    }

    public function editar($param) {
        $this->model->edit($param);
        $this->View->arrListUrl = $this->model->listing();
        $this->View->render('customer/index');
    }

    public function delete($param) {
        $this->model->delete($param);
        $this->View->arrListUrl = $this->model->listing();
        $this->View->render('customer/index');
    }
     */

}
