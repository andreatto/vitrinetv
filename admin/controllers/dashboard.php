<?php

class Dashboard extends Controller {

    function __construct() {

        parent::__construct();

        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
    }

    public function index() {
        $this->View->render('dashboard/index');
    }
    
    public function logout() {
        Session::destroy();
        header('Location: ../login');
        exit;
    }
}
