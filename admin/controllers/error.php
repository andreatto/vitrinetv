<?php

class Error extends Controller {

    function __construct() {
        parent::__construct();
        //echo 'We are Error __construct!';
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        }
    }
    
    public function index () {
        $this->View->msg = 'Esta página não existe !';
        $this->View->render('error/index');
    }

}