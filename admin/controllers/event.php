<?php

class Event extends Controller {

    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
    }

    public function index( $idModulo ) {
        $arrModulo = $this->model->getDataModulo( $idModulo );
        $this->View->dataEvento = $this->model->getEvents( $arrModulo );
        $this->View->render('events/index');
    }

    public function form( $idModulo ) {
        $arrModulo = $this->model->getDataModulo( $idModulo );
        $this->View->dataUrl = $this->model->listingEventsAll();
        $this->View->dataModulo = $this->model->getDataModulo( $idModulo );
        $this->View->dataEvento = $this->model->getEvents( $arrModulo );
        $this->View->render('events/form');
    }

    public function inserir( $idModulo ) {
        $this->model->insertRelView( $idModulo );
        $arrModulo = $this->model->getDataModulo( $idModulo );
        $this->View->dataEvento = $this->model->getEvents( $arrModulo );
        $this->View->dataModulo = $this->model->getDataModulo( $idModulo );
        $this->View->render('events/index');
    }

    public function formEdit( $idModulo ) {
        $this->View->actn = 'editar';
        $arrModulo = $this->model->getDataModulo( $idModulo );
        $this->View->dataModulo = $this->model->getDataModulo( $idModulo );
        $this->View->dataUrl = $this->model->listingEventsAll();
        $this->View->dataEvento = $this->model->getEvents( $arrModulo );
        $this->View->render('events/form');
    }

    public function editar( $idModulo ) {
        $this->View->actn = 'editar';
        $arrModulo = $this->model->getDataModulo( $idModulo );
        $this->model->editRelView( $arrModulo );
        $this->View->dataEvento = $this->model->getEvents( $arrModulo );
        $this->View->dataModulo = $this->model->getDataModulo( $idModulo );
        $this->View->render('events/index');
    }
/*
    public function delete($param) {
        $tela = $this->model->getDataModulo($param);
        $this->model->delete($param);
        $this->model->deleteRelCart($param);
        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->render('menu/index');
    }
*/
}    