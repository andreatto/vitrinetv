<?php

class Eventos extends Controller {
    
    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
        
    }

    public function index() {        
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('eventos/index');
    }
    
    public function form() {
        $this->View->dataUrl = $this->model->form();
        $this->View->render('eventos/form');
    }

    public function inserir() {
        $arr = $this->model->insert();
        $arrId = $this->model->insertImg( $arr );
        $this->View->dataUrl = $this->model->form();
        $this->View->render('eventos/form');
    }

    public function formEdit( $idEvento ) {
        $this->View->dataUrl = $this->model->formEdit( $idEvento );
        $this->View->dataimg = $this->model->imgEvento( $idEvento );
        $this->View->render('eventos/form');
    }

    public function editar( $param ) {
        $arr = $this->model->edit( $param );        
        $this->model->editImg( $arr );
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('eventos/index');
    }
    
    public function delete( $idEvento ) {
        //$par = $param;
        $res = $this->model->emUsoTv( $idEvento );
        if ( $res === FALSE ){
            $this->model->delete( $idEvento );
            $this->model->deleteImg( $idEvento );
        }
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('eventos/index');
    }

}