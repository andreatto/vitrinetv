<?php

class Galeria extends Controller {

    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
    }

    public function index($param) {
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataUrl = $this->model->getProductGaleria($arrTela);
        $this->View->render('galeria/index');
    }

    public function form($param) {
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataProd = $this->model->listingProd($arrTela);
        $this->View->render('galeria/form');
    }

    public function inserir($param) {
        $this->model->insertRelView($param);
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataUrl = $this->model->getProductGaleria($arrTela);
        $this->View->render('galeria/index');
    }

    public function formEdit($param) {
        $this->View->actn = 'editar';
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataProd = $this->model->listingProd($arrTela);
        $this->View->dataUrl = $this->model->getProductGaleria($arrTela);
        $this->View->render('galeria/form');
    }

    public function editar($param) {
        $this->View->actn = 'editar';
        $arrMod = $this->model->pegarTela($param);
        $this->model->editRelView($arrMod);
        $this->View->dataUrl = $this->model->getProductGaleria($arrMod);
        $this->View->render('galeria/index');
    }
/*
    public function delete($param) {
        $tela = $this->model->pegarTela($param);
        $this->model->delete($param);
        $this->model->deleteRelCart($param);
        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->render('galeria/index');
    }
*/
}    