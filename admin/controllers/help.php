<?php

class Help extends Controller {

    function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        }
    }
    
    public function index () {
        $this->View->render("help/index");
    }
    
    public function other ($arg = false){
        require 'models/help_model.php';
        $model = new Help_Model();
        
        //$this->View->blah = blah();
        $model->fctnTxt();
        
    }

}