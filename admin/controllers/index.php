<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit; 
        }
    }

    public function index() {
        $this->View->render("index/index");
    }

    public function details() {
        $this->View->render("index/index");
    }

}
