<?php

class Login extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->View->login("login/index");
    }

    public function run() {
        $this->model->run();
    }
    
    public function unLogin() {
        
    }

}
