<?php

class Menu extends Controller {

    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
    }

    public function index($param) {
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataUrl = $this->model->getProductMenu($arrTela);
        $this->View->render('menu/index');
    }

    public function form($param) {
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataProd = $this->model->listingProd($arrTela);
        $this->View->render('menu/form');
    }

    public function inserir($param) {
        $this->model->insertRelView($param);
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataUrl = $this->model->getProductMenu($arrTela);
        $this->View->render('menu/index');
    }

    public function formEdit($param) {
        $this->View->actn = 'editar';
        $arrTela = $this->model->pegarTela($param);
        $this->View->dataProd = $this->model->listingProd($arrTela);
        $this->View->dataUrl = $this->model->getProductMenu($arrTela);
        $this->View->render('menu/form');
    }

    public function editar($param) {
        $this->View->actn = 'editar';
        $arrMod = $this->model->pegarTela($param);
        $this->model->editRelView($arrMod);
        $this->View->dataUrl = $this->model->getProductMenu($arrMod);
        $this->View->render('menu/index');
    }
/*
    public function delete($param) {
        $tela = $this->model->pegarTela($param);
        $this->model->delete($param);
        $this->model->deleteRelCart($param);
        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->render('menu/index');
    }
*/
}    