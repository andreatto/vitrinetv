<?php

class Modulo extends Controller
{

    public function __construct()
    {
        parent::__construct();

        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        }

    }

    public function index($tela)
    {
        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->dataCart = $this->model->myCartucho($tela);
        $this->View->idReturnTela = $tela;
        $this->jsFunction();
        $this->View->render('modulo/index');
    }

    public function form($param)
    {
        $this->View->dataCart = $this->model->cartucho();
        $this->View->dataCartBy = $this->model->cartuchoBy();
        $this->View->dataUrl = $this->model->form($param);
        $this->View->render('modulo/form');
    }

    public function inserir($numTela)
    {
        //echo 'numero da tela : '. $numTela . "</br>";
        
        
        $Mod_id = $this->model->insert($numTela);

        $this->model->insertRelModCart();
        $this->model->insertSequenceCarouselData($Mod_id[3]);
        $this->View->dataCart = $this->model->cartucho();
        $this->View->dataCartBy = $this->model->cartuchoBy();
        $this->View->dataUrl = $this->model->form($numTela);
        $this->View->render('modulo/form');
    }

    public function formEdit($param)
    {
        $this->View->dataUrl = $this->model->formEdit($param);
        $this->View->dataRelCart = $this->model->getMyCartucho($param);
        $this->View->dataCart = $this->model->cartucho();
        $this->View->dataCartBy = $this->model->cartuchoBy();
        $this->jsFunction();
        $this->View->render('modulo/form');
    }

    public function editModuloAtivarConfig($modId, $target)
    {
        $this->model->editModuloAtivar($modId, $target);
        $this->jsFunction();
    }

    public function editar($param)
    {
        $tela = $this->model->edit($param);
        $this->model->editRelCart($param);

        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->dataCart = $this->model->myCartucho($tela);
        $this->jsFunction();
        $this->View->render('modulo/index');
    }

    public function delete($modId)
    {
        $tela = $this->model->pegarTela($modId);
        $this->model->delete($modId);
        $this->model->deleteRelCart($modId);
        $arrView = $this->model->selectView($modId);
        $this->model->deleteView($arrView);

        $ordModId = $this->model->selectUniqueCarousel($modId);
        $this->model->deleteSequenceCarousel($ordModId);

        $this->View->dataUrl = $this->model->listing($tela);
        $this->View->dataCart = $this->model->myCartucho($tela);
        $this->jsFunction();
        $this->View->render('modulo/index');
    }

    public function updateSequenceCarousel($param)
    {
        $this->model->updateSequenceCarouselData($param);
    }

    public function jsFunction()
    {
        $this->View->js = array("modulo/js/model.js");
        $this->View->arrSrsJs = [
            '<!-- Sortable drag/drop modulos IMG -->',
            '<script src="https://code.jquery.com/jquery-1.12.4.js"></script>',
            '<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>'
        ];
    }
}
    