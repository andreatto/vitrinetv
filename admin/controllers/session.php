<?php

class Session extends Controler {
    public function __construct() {
        parent::__construct();

        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        } 
    }
    
    public function logout() {
        Session::destroy();
        header('Location: ../login');
        exit;
    }
    
}