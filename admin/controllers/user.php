<?php

class User extends Controller {

    public function __construct() {
        parent::__construct();
        
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location: login');
            exit;
        }
    }

    public function index() {
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('user/index');
    }
    
    public function form() {
        $this->View->dataUrl = $this->model->form();
        $this->View->render('user/form');
    }

    public function inserir() {
        $arr = $this->model->insert();
        $this->model->insertImg( $arr );
        $this->View->render('user/form');
    }
    
    public function formEdit($param) {
        $this->View->dataUrl = $this->model->formEdit($param);
        $this->View->render('user/form');
    }

    public function editar($param) {
        $arr = $this->model->edit($param);
        $this->model->editImg($arr);
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('user/index');
    }

    public function delete($param) {
        $this->model->delete($param);
        $this->model->deleteImg($param);
        $this->View->dataUrl = $this->model->listing();
        $this->View->render('user/index');
    }

}