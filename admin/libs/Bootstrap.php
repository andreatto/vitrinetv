<?php

class Bootstrap {

    function __construct() {

        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        
        //print_r($url); echo "<br/>";
        
        if (empty($url[0])) {
            require 'controllers/index.php';
            $controller = new index();
            $controller->index();
            return false;
        };

        $file = 'controllers/' . $url[0] . '.php';
        if (file_exists($file)) {
            require $file;
        } else {
            $this->error();
        }

        $controller = new $url[0];
        $controller->loadModel($url[0]);
        
        // calling methods
        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                //$controller->$url[]();
                //$controller->function();
                //{}()
                $controller->{$url[1]}($url[2]);
            } else {
                $this->error();
            }
        } else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $this->error();
                }
                
            } else {
                $controller->index();
            }
        }
    }

    public function error() {
        require 'controllers/error.php';
        $this->error = new error();
        $this->error->index();
        return false;
    }

}
