<?php

class Database extends PDO {
    
    function __construct() {
        //parent::__construct($dsn, $username, $password, $options);
        //parent::__construct('mysql:host=127.0.0.1;dbname=db_menu', 'root', '');
        
        try {
            parent::__construct(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASS);
            //Echo 'Conectado: ';
        } catch (PDOException $e) {
            echo 'ERRO: ' . $e->getMessage();
        }
    }
}
