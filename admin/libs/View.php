<?php

class View {

    function __construct() {
        //echo 'View Construct!   <br/>';
    }
    
    public function render($name, $noInclude) {
        
        if ($noInclude == true) {
            require 'views/' . $name . '.php';
        } else {
            require 'views/header.php';
            require 'views/' . $name . '.php';
            require 'views/footer.php';             
        }
    }

    public function login($name, $noInclude) {
        if ($noInclude == true) {
            require 'views/' . $name . '.php';
        } else {
            //require 'views/header.php';
            require 'views/' . $name . '.php';
            //require 'views/footer.php';
        }
    }
    
}