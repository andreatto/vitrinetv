<?php

class Campanha_Model extends Model {

    public function __construct($param) {
        parent::__construct();
    }

    public function listing() {
        $qry = $this->db->prepare('SELECT * FROM tb_campanha WHERE cust_id = :id');
        $qry->execute(array(
            ':id' => Session::get('userCust')
        ));
        return $qry->fetchAll();
    }
    
    public function form() {
        $actn = 'inserir';
        return $actn;
    }
   
    public function formEdit($param) {
       
        $idParam = $param;
        $actn = 'editar';
        
        try {
            $sth = $this->db->prepare(' SELECT * FROM tb_campanha WHERE camp_id = :id AND cust_id = :userCust');
            $sth->execute(array (
                ':id' => $idParam,
                ':userCust' => Session::get('userCust')
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar listar categoria: ");
        }
        
    }
    
    public function insert() {
        $actn = 'inserir';
        try {
            $sth = $this->db->prepare(' Insert into tb_campanha
                                        (camp_id, camp_nome, cust_id) 
                                        VALUES 
                                        (:camp_id, :camp_nome, :id) ');                           

            $sth->execute(array(
                ':camp_id' => $_POST['id'] = Null,
                ':camp_nome' => $_POST['nome'],
                ':id' => Session::get('userCust')
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar Produto: " . $_POST['nome']);
            //header('location: form');
        }
        
    }
    
    public function edit($param) {
        $idParam = $param;
        
        try {
            $sth = $this->db->prepare(' UPDATE tb_campanha 
                                        SET camp_nome = :camp_nome
                                        WHERE camp_id = :camp_id
                                        AND cust_id = :userCust
                                      ');
            
            $sth->execute(array(
                ':camp_id' => $idParam,
                ':camp_nome' => $_POST['nome'],
                ':userCust' => Session::get('userCust')
            ));

            return $sth->fetchAll();
            
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar produto: ' . $_POST['nome']);
        }
    }
    
    public function delete($param) {
        $idParam = $param;
        try{
            $sth = $this->db->prepare('DELETE FROM tb_campanha WHERE camp_id = :id AND cust_id = :userCust ');
            $sth->execute(array(
                    ':id' => $idParam,
                    ':userCust' => Session::get('userCust')
                ));
            $sth->fetchAll();
        }  catch (PDOException $e){
            return $e->getMEsaage('Erro ao deletar esta categoria: ' . $_POST['nome']);
        }
    }

}   