<?php

class Cartucho_Model extends Model {

    public function __construct() {
        parent::__construct();
    }
/*
    public function listing($param) {
        try {
            $lst = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_modulo
                                        WHERE
                                            num_tela = :tela
                                        AND
                                            cust_id = :id
                                    ');
            $lst->execute(array(
                ':tela' => $param,
                ':id' => Session::get('userCust')
            ));

            $arrMod = $lst->fetchAll();

            session_start();
            $_SESSION["idTv"]=$param;
            $_SESSION["idMod"]=$arrMod[0];

        return $arrMod;
        
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao listar Modulos: ');
        }
    }
*/
    public function listingProd() {
        $lst = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cadProduto as prod
                                    inner join tb_categoria as cat
                                        on cat.cat_id = prod.tb_categoria_cat_id
                                    inner join tb_medida as med
                                        on med.med_id = prod.tb_medida_med_id
                                    inner join tb_campanha as cam
                                        on cam.camp_id = prod.tb_campanha_camp_id
                                    WHERE 
                                        prod.cust_id = :id 
                                ');
        $lst->execute(array(
                ':id' => Session::get('userCust')
        ));
        return $lst->fetchAll();
    }

    public function form($param) {
        $actn = 'inserir';
        return $res = [$actn, $param];
    }

    public function insert($param) {

        echo 'Inserir Cartucho 90 <br/>';
        if(!empty($_REQUEST['cartucho'])):
	       foreach ($_REQUEST['cartucho'] as $value) {
                # code...
                echo '1 code... '.$value . '<br/>';
            }
        endif;
        if(empty($_REQUEST['cartucho'])):
	        echo 'Vaizia code...<br/>';
            echo 'Vaizia code... '.$param . '<br/>';
        endif;

        echo 'code... '.$param . '<br/>';
        exit();
        
    }

    public function insertRelCart() {
        $actn = 'inserir';
        
        try {
            for ($i =0;$i < 12; $i++) {
                $sth = $this->db->prepare(' INSERT into tb_view
                                                (view_id, cust_id, tela, mod_id, cart_id, prod_id) 
                                            VALUES 
                                                (:id, :custId, :tela, :modId, :cartId, :prodId) 
                                        ');                           
                $sth->execute(array(
                    ':id' => Null,
                    ':custId' => Session::get('userCust'),
                    ':tela' => $_SESSION["idTv"],
                    ':modId' => Session::set('moduloId'),
                    ':cartId' => $param,
                    ':prodId' => $prod
                ));                     
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar modulo com cartuchos: ");
        }   
    }

    public function formEdit($param) {
        $actn = 'editar';
        try {
            $sth = $this->db->prepare(' SELECT * FROM tb_modulo 
                                        WHERE
                                            mod_id = :modulo
                                        AND
                                            cust_id = :userCust 
                                    ');
            $sth->execute(array (
                ':modulo' => $param,
                ':userCust' => Session::get('userCust')
            ));
            return $res = [$actn, $param, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar listar modulos: ");
        }
    }

    public function edit($param) {
        try {
            // Update modulo
            $sth = $this->db->prepare(' UPDATE 
                                            tb_modulo 
                                        SET 
                                            mod_nome = :nome
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                        ');
            
            $sth->execute(array(
                ':id' => $param,
                ':nome' => $_POST['nome'],
                ':userCust' => Session::get('userCust')
            ));
            $tela = $this->pegarTela($param);
            return $arr = [$tela, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar produto: ' . $_POST['nome']);
        }
    }

    public function editRelCart($param) {   

        try {
            foreach ($_POST['chckCart'] as $key => $cart) {
                $sth = $this->db->prepare(' INSERT into tb_rel_mod_cart
                                                (mod_cart_id, cust_id, mod_id, cart_id, mod_cart_flag_ativo) 
                                            VALUES 
                                                (:id, :custId, :modId, :cartId, :flagAtivo) 
                                        ');                           
                $sth->execute(array(
                    ':id' => Null,
                    ':custId' => Session::get('userCust'),
                    ':modId' => $param,
                    ':cartId' => $cart,
                    ':flagAtivo' => 1
                ));                     
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar modulo com cartuchos: ");
        }   
    }

    public function deleteRelCart($param) {
        try{
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_rel_mod_cart 
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                    ':id' => $param,
                    ':userCust' => Session::get('userCust')
                ));
            return $sth->fetchAll();
        }  catch (PDOException $e){
            return $e->getMEsaage('Erro ao deletar esta relacao: ' . $_POST['nome']);
        }
    }

    public function delete($param) {
        try{
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_modulo 
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                    ':id' => $param,
                    ':userCust' => Session::get('userCust')
                ));
            return $sth->fetchAll();
        }  catch (PDOException $e){
            return $e->getMEsaage('Erro ao deletar esta categoria: ' . $_POST['nome']);
        }
    }

    public function pegarTela($param) {
        // Para retornar id tela
        $sthTela = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_modulo 
                                        WHERE 
                                            mod_id = :id                                          
                                        AND
                                            cust_id = :userCust 
                                    ');
        $sthTela->execute(array (
            ':id' => $param,
            ':userCust' => Session::get('userCust')
        ));
        $arrTela = $sthTela->fetchAll();
        foreach ($arrTela as $value) {
            $tela = $value[3];
        }
        return $tela;
    }

    public function cartucho() {
        // Para retornar id tela
        $sth = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cartucho 
                                    WHERE 
                                        cart_flag_ativo = 1
                                    AND
                                        cart_flag_free = 1 
                                ');
        $sth->execute();
        return $sth->fetchAll();
    }

    public function cartuchoBy() {
        // Para retornar id tela
        $sth = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cartucho 
                                    WHERE 
                                        cart_flag_ativo = 1
                                    AND
                                        cart_flag_buy = 1 
                                    AND
                                        cart_flag_free = 0
                                ');
        $sth->execute();
        return $sth->fetchAll();
    }

    public function myCartucho($param) {     

        $sth = $this->db->prepare(' SELECT * FROM tb_modulo as modu
                                    inner join tb_rel_mod_cart as rel
                                    on modu.mod_id = rel.mod_id
                                    inner join tb_cartucho as cart
                                    on cart.cart_id = rel.cart_id
                                    WHERE
                                        modu.num_tela = :tela
                                    AND
                                        modu.cust_id = rel.cust_id 
                                    AND
                                        modu.cust_id = :userCust 
                                ');
        $sth->execute(array (
            ':tela' => $param,
            ':userCust' => Session::get('userCust')
        ));

        return $sth->fetchAll();
    }

    public function getMyCartucho($param) {

        $sth = $this->db->prepare(' SELECT * 
                                    from tb_rel_mod_cart as rel
                                    inner join tb_cartucho as cart
                                    on cart.cart_id = rel.cart_id
                                    where 
                                        rel.mod_id = :modulo
                                    AND
                                        rel.cust_id = :userCust
                                    AND 
                                        rel.mod_cart_flag_ativo = 1
                                ');
        $sth->execute(array (
            ':modulo' => $param,
            ':userCust' => Session::get('userCust')
        ));
	    return $sth->fetchAll();
    }

    public function getCartucho($param) {
        // Para retornar id tela
        $sth = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_cartucho 
                                        WHERE 
                                            cart_id = :cartId
                                        And
                                            cart_flag_ativo = 1
                                        AND
                                            cart_flag_free = 1 
                                    ');
        $sth->execute(array (
            ':cartId' => $param,
            ':userCust' => Session::get('userCust')
        ));
        
        return $sth->fetchAll();
    }
}