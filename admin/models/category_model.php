<?php

class Category_Model extends Model {

    public function __construct($param) {
        parent::__construct();
    }

    public function listing() {
        $qry = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_categoria 
                                    WHERE
                                        cust_id = :id
                                ');
        $qry->execute(array(
            ':id' => Session::get('userCust')
        ));
        return $qry->fetchAll();
    }
    
    public function form() {
        $actn = 'inserir';
        return $actn;
    }
   
    public function formEdit($param) {
       
        $idParam = $param;
        $actn = 'editar';
        
        try {
            $sth = $this->db->prepare(' SELECT * 
                                        FROM 
                                            tb_categoria 
                                        WHERE 
                                            cat_id = :id 
                                        AND
                                            cust_id = :custId
                                    ');
            $sth->execute(array (
                ':id' => $idParam,
                ':custId' => Session::get('userCust')
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar listar categoria: ");
        }
        
    }
    
    public function insert() {
        $actn = 'inserir';
        try {
            $sth = $this->db->prepare(' Insert into 
                                        tb_categoria
                                        (cat_id, cat_nome, cust_id) 
                                        VALUES 
                                        (:cat_id, :cat_nome, :custId) 
                                    ');                           

            $sth->execute(array(
                ':cat_id' => $_POST['id'] = Null,
                ':cat_nome' => $_POST['nome'],
                ':custId' => Session::get('userCust')
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar Produto: " . $_POST['nome']);
            //header('location: form');
        }
        
    }
    
    public function edit($param) {
        $idParam = $param;
        
        try {
            $sth = $this->db->prepare(' UPDATE 
                                            tb_categoria 
                                        SET 
                                            cat_nome = :cat_nome
                                        WHERE 
                                            cat_id = :cat_id
                                        AND
                                            cust_id = :custId
                                      ');
            
            $sth->execute(array(
                ':cat_id' => $idParam,
                ':cat_nome' => $_POST['nome'],
                ':custId' => Session::get('userCust')
            ));

            $sth->fetchAll();
            
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar produto: ' . $_POST['nome']);
        }
    }
    
    public function delete($param) {
        $idParam = $param;
        try{
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_categoria 
                                        WHERE 
                                            cat_id = :id
                                        AND
                                            cust_id = :custId                                            
                                    ');
            $sth->execute(array(
                    ':id' => $idParam,
                    ':custId' => Session::get('userCust')
                ));
            $sth->fetchAll();
        }  catch (PDOException $e){
            return $e->getMEsaage('Erro ao deletar esta categoria: ' . $_POST['nome']);
        }
    }

}   