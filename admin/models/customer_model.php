<?php

class Customer_Model extends Model
{

    public function __construct($param)
    {
        parent::__construct();
    }

    public function listing()
    {
        $qry = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_customer 
                                    WHERE 
                                        cust_id = :id
                                ');
        $qry->execute(array(
            ':id' => Session::get('userCust'),
            ':custId' => Session::get('userCust')
        ));
        return $qry->fetchAll($param);
    }

    public function form()
    {
        $actn = 'form';
    }

    public function formEdit()
    {
        $actn = 'form';
        try {
            $sth = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_customer as cust
                                        inner join 
                                            tb_endereco as ende
                                                on ende.cust_id = :id
                                        inner join 
                                            tb_contact as cont
                                                on cont.cust_id = :id 
                                        inner join 
                                            tb_img as img
                                                ON img.cust_id = :id 
                                                AND img.tb_user_user_id is null
                                                AND img.tb_cadProduto_prod_id is null
                                        WHERE
                                            cust.cust_id = :id 
                                     ');
            $sth->execute(array(
                ':id' => Session::get('userCust')
            ));

            return $res = array($actn, $sth->fetchAll());

        } catch (PDOException $e) {
            return $e->getMessage("Erro ao editar dados da empresa: ");
        }
    }

    public function edit($param)
    {

        $file = $_FILES['imgs'];

        try {
            $sth = $this->db->prepare(' UPDATE 
                                            tb_customer 
                                        SET 
                                            cust_rs = :rs, 
                                            cust_fantasia = :fantasia,
                                            cust_cnpj = :cnpj,
                                            cust_insc_est = :inscEst,
                                            cust_contato = :contato
                                        WHERE 
                                            cust_id = :id
                                        AND
                                            cust_id = :custId
                                    ');

            $sth->execute(array(
                ':id' => $param,
                ':rs' => $_POST['rs'],
                ':fantasia' => $_POST['fantasia'],
                ':cnpj' => $_POST['cnpj'],
                ':inscEst' => $_POST['inscEst'],
                ':contato' => $_POST['contato'],
                ':custId' => Session::get('userCust')
            ));

            $ende = $this->db->prepare(' UPDATE 
                                            tb_endereco 
                                        SET 
                                            end_street = :street, 
                                            end_number = :numero,
                                            end_bairro = :bairro,
                                            end_cep = :cep,
                                            end_city = :city,
                                            end_estado = :estado,
	                                        end_country = :pais
                                        WHERE 
                                            cust_id = :id
                                        AND
                                            cust_id = :custId
                                    ');

            $ende->execute(array(
                ':id' => $param,
                ':street' => $_POST['street'],
                ':numero' => $_POST['number'],
                ':bairro' => $_POST['bairro'],
                ':cep' => $_POST['cep'],
                ':city' => $_POST['cidade'],
                ':estado' => $_POST['estado'],
                ':pais' => $_POST['pais'],
                ':custId' => Session::get('userCust')
            ));

            $cont = $this->db->prepare(' UPDATE 
                                            tb_contact 
                                        SET 
                                            cont_telefone = :telefone, 
                                            cont_cel = :cel,
                                            cont_email = :email,
                                            cont_site = :site
                                        WHERE 
                                            cust_id = :id
                                        AND
                                            cust_id = :custId
                                    ');

            $cont->execute(array(
                ':id' => $param,
                ':telefone' => $_POST['telefone'],
                ':cel' => $_POST['celular'],
                ':email' => $_POST['email'],
                ':site' => $_POST['site'],
                ':custId' => Session::get('userCust')
            ));

            $imgCust = $this->db->prepare(' SELECT 
                                                *
                                            FROM 
                                                tb_img 
                                            WHERE 
                                                cust_id = :id
                                            AND
                                                tb_user_user_id is null
                                            AND
                                                tb_cadProduto_prod_id is null
                                            AND
                                                cust_id = :custId
                                        ');

            $imgCust->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));

            return $ret = array($param, $file, $sth->fetchAll(), $ende->fetchAll(), $cont->fetchAll(), $imgCust->fetchAll());

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar a iamgem da empresa: ');
        }
    }

    public function editImg($param)
    {

        $id = $param[0];
        $file = $param[1];
        $arrImgn = $param[5];

        foreach ($arrImgn as $key => $value) {
            $arrImg[0] = $value[0];
            $arrImg[2] = $value[2];
        }

        // Se a foto estiver sido selecionada
        if ($file['name'] != '') :

            $size = $file['size'];
            // return $dirpath:   /home/nteck763/public_html/smartpeeper
        $dirpath = realpath(dirname(getcwd()));

        $pathImg = $dirpath . "/admin/img/customer/" . $arrImg[2];

            // Verifica se o arquivo é uma imagem
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $file["type"])) :
            return $error[1] = "Isso não é uma imagem.";
        else :
                

                //$percent = 0.5;
                //$iWidth = 767 * $percent;
                //$iHeight = 511 * $percent; // desired image result dimensions
        $iWidth = 200;
        $iHeight = 220;
        $iJpgQuality = 95;

        if ($_FILES) {
                    // if no errors and size less than 250kb
            if (!$_FILES['imgs']['error']) {

                if (is_uploaded_file($_FILES['imgs']['tmp_name'])) {

                            // new unique filename
                    $nomeSave = md5(time() . rand());
                    $sTempFileName = $dirpath . '/admin/img/customer/' . $nomeSave;

                            // move uploaded file into cache folder
                    move_uploaded_file($_FILES['imgs']['tmp_name'], $sTempFileName);

                            // change file permission to 644
                    @chmod($sTempFileName, 0777);

                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                        $aSize = getimagesize($sTempFileName); // try to obtain image info

                        if (!$aSize) {
                            @unlink($sTempFileName);

                            return;

                        }

                                // check for image type
                        switch ($aSize[2]) {

                            case IMAGETYPE_JPEG:
                                $sExt = '.jpg';
                                        // create a new image from file
                                $vImg = @imagecreatefromjpeg($sTempFileName);
                                break;

                            case IMAGETYPE_PNG:
                                $sExt = '.png';
                                        // create a new image from file
                                $vImg = @imagecreatefrompng($sTempFileName);
                                break;
                            default:
                                @unlink($sTempFileName);

                                return;

                        }

                        $nameForSave = $nomeSave . $sExt;
                                // create a new true color image
                        $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);

                                // copy and resize part of an image with resampling
                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                                // define a result image filename
                        $sResultFileName = $sTempFileName . $sExt;

                                // output image to file
                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                        @unlink($sTempFileName);
                        unlink($pathImg);

                                //return $sResultFileName;

                    }

                }

            }

        }


        endif; // fim else catch .img/.jpg ....

        try {
            $sth = $this->db->prepare(' UPDATE 
                                                tb_img 
                                            SET 
                                                img_nick_name = :nickName, 
                                                img_name = :nome, 
                                                img_size = :size
                                            WHERE 
                                                img_id = :idImg
                                        ');

            $sth->execute(array(
                ':idImg' => $arrImg[0],
                ':nickName' => $_POST['nomeImg'],
                ':nome' => $nameForSave,
                ':size' => $size
            ));

            $sth->fetchAll();

                // Caminho de onde ficará a imagem
                //$caminho_imagem = "/Applications/MAMP/htdocs/SmartPeeper/admin/img/customer/" . $nome_imagem;

                //$caminho_imagem = $dirpath . "/admin/img/customer/" . $nome_imagem;
                // Faz o upload da imagem para seu respectivo caminho
                //move_uploaded_file($file['tmp_name'], $caminho_imagem);

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao atualizar imgaem: ' . $error);
        }

        endif; // fim if tmp_name !=r

        // inicio if tmp_name ==
        if ($file['name'] == '') {

            try {
                $sth = $this->db->prepare(' UPDATE 
                                                tb_img 
                                            SET 
                                                img_nick_name = :nickName 
                                            WHERE 
                                                img_id = :idImg
                                        ');

                $sth->execute(array(
                    ':idImg' => $arrImg[0],
                    ':nickName' => $_POST['nomeImg']
                ));

                $sth->fetchAll();

            } catch (PDOException $e) {
                return $e->getMessage('Erro ao atualizar imagem: ' . $error);
            }
        } // fim if tmp_name  == '' 
    }
    
/*
    public function insert() {
        $actn = 'inserir';
        try {
            $sth = $this->db->prepare('Insert into tb_categoria
                                        (cat_id, cat_nome) 
                                        VALUES 
                                        (:cat_id, :cat_nome)');                           

            $sth->execute(array(
                ':cat_id' => $_POST['id'] = Null,
                ':cat_nome' => $_POST['nome']
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar Produto: " . $_POST['nome']);
            //header('location: form');
        }
        
    }
    
    public function delete($param) {
        $idParam = $param;
        try{
            $sth = $this->db->prepare('DELETE FROM tb_categoria WHERE cat_id = :id');
            $sth->execute(array(':id' => $idParam));
            $sth->fetchAll();
        }  catch (PDOException $e){
            return $e->getMEsaage('Erro ao deletar esta categoria: ' . $_POST['nome']);
        }
    }
     */
}   