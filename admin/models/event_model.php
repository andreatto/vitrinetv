<?php

class Event_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    // Index
    public function getDataModulo( $idModulo ) {
        $sth = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_modulo 
                                    WHERE 
                                        mod_id = :id                                          
                                    AND
                                        cust_id = :userCust 
                                ');
        $sth->execute(array(
            ':id' => $idModulo,
            ':userCust' => Session::get('userCust')
        ));
        $arr = $sth->fetchAll();
        foreach ($arr as $value) {}
        return $value;
    }    
    
    public function getEvents( $arrModulo ) {
        $cart = 7;
        $sth = $this->db->prepare(' SELECT * FROM 
                                        tb_events as even
                                    inner join 
                                        tb_view as v
                                    on v.event_id = even.event_id
                                    AND
                                        v.mod_id = :modId
                                    AND
                                        v.cust_id = :userCust
                                    AND 
	                                v.cart_id = :cart
                                    ORDER 
                                        by v.view_id
                                ');
        $sth->execute(array (
            ':modId' => $arrModulo[0],
            ':cart' => $cart,
            ':userCust' => Session::get('userCust')
        ));
        return $arr = array( $arrModulo, $sth->fetchAll() );
    }
    
    // Form 
    public function listingEventsAll() {
        $actn = "form";
        $lst = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_events
                                    WHERE 
                                        cust_id = :id 
                                ');
        $lst->execute( array(
                ':id' => Session::get('userCust')
        ));
        
        return $arr = array( $actn, $lst->fetchAll() );
    }
    
    //Relacao 
    // Inserir 
    public function insertRelView( $idModulo ) {
        $cart = 7;
        $tv = $this->pegarModuloId( $idModulo );
        
        try {
            foreach($_REQUEST['cartucho'] as $event) {
                $sth = $this->db->prepare(' INSERT into tb_view
                                                ( view_id, cust_id, tela, mod_id, cart_id, prod_id, event_id ) 
                                            VALUES 
                                                ( :id, :custId, :tela, :modId, :cartId, :prodId, :eventId ) 
                                        ');                          
                $sth->execute(array(
                    ':id' => Null,
                    ':custId' => Session::get('userCust'),
                    ':tela' => $tv,
                    ':modId' => $idModulo,
                    ':cartId' => $cart,
                    ':prodId' => null,
                    ':eventId' => $event
                ));
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar modulo com eventos: ");
        }   
    } 
    public function pegarModuloId( $idModulo ) {
        $sth = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_modulo 
                                        WHERE 
                                            mod_id = :id                                          
                                        AND
                                            cust_id = :userCust 
                                    ');
        $sth->execute(array (
            ':id' => $idModulo,
            ':userCust' => Session::get('userCust')
        ));
        $arrTv = $sth->fetchAll();
        foreach ($arrTv as $tv) {
        }
       
        return $tv[3];
    }
    
    // Form edit
    public function editRelView( $arrModulo ) {
        $reqEvent = $_REQUEST['cartucho'];
        try {
            foreach ($_REQUEST['ids'] as $i => $arrIds) {
                $sth = $this->db->prepare(' UPDATE 
                                                tb_view 
                                            SET 
                                                event_id = :eventId
                                            WHERE 
                                                view_id = :viewId
                                            AND
                                                mod_id = :modId
                                            AND
                                                cust_id = :custId  
                                        ');

                $sth->execute(array(
                    ':eventId' => $reqEvent[$i],
                    ':viewId' => $arrIds,
                    ':modId' => $arrModulo[0],
                    ':custId' => Session::get('userCust')
                ));
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar produtos com o cartucho: ");
        }    
    }

}