<?php

class Eventos_Model extends Model {

    public function __construct($param) {
        parent::__construct();
    }

    public function listing() {

        $actn = 'inserir';

        $lst = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_events
                                    WHERE 
                                        cust_id = :id 
                                ');
        $lst->execute( array(
                ':id' => Session::get('userCust')
        ));
        return $arr = array( $actn, $lst->fetchAll() );
    }

    public function form() {
        $actn = 'inserir';
    }

    public function insert() {

        $img = $_FILES['imgs'];
        $valor1 = str_replace( '.', '', $_POST['valor'] );
        $valor = str_replace( ',', '.', $valor1 );

        try {
            $sth = $this->db->prepare(' INSERT INTO 
                                        tb_events
                                            (event_id, 
                                             event_nome, 
                                             event_valor, 
                                             event_data,
                                             event_datasemana,
                                             event_horario, 
                                             event_desc,
                                             cust_id) 
                                        VALUES 
                                            (:eventId, 
                                            :eventNome, 
                                            :eventValor,
                                            :eventData,
                                            :eventdatasemana,
                                            :eventHorario, 
                                            :eventDesc,
                                            :custId) 
                                    ');

            $sth->execute(array(
                ':eventId' => Null,
                ':eventNome' => $_POST['name'],
                ':eventValor' => $valor,
                ':eventData' => $_POST['date'],
                ':eventdatasemana' => $_POST['datesemana'],
                ':eventHorario' => $_POST['horario'],
                ':eventDesc' => $_POST['descricao'],
                ':custId' => Session::get('userCust')
            ));
            $arrEvents = $sth->fetchAll();
            $lastIdEvento = $this->db->lastInsertId();
            return $arr = array( $img, $arrEvents, $lastIdEvento );
            
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar Produto: ");
            //header('location: form');
        }
    }

    public function insertImg( $arr ) {

        $img = $arr[0];
        //$name = $img['name'];
        //$size = $img['size'];
        $nickName = $_POST['nomeImg'];
        // return $dirpath:   /home/nteck763/public_html/smartpeeper
        $dirpath = realpath( dirname( getcwd() ) ) ;
                
        //$percent = 0.5;
        //$iWidth = 767 * $percent;
        //$iHeight = 511 * $percent; // desired image result dimensions
        $iWidth = 800;
        $iHeight = 600;
        $iJpgQuality = 95;

        if ($_FILES) {
            // if no errors and size less than 250kb
            if ( !$_FILES['imgs']['error'] ) {

                if ( is_uploaded_file($_FILES['imgs']['tmp_name']) ) {

                    // new unique filename
                    $nomeSave = md5( time().rand() ) ;
                    $sTempFileName = $dirpath . '/admin/img/eventos/' . $nomeSave;

                    // move uploaded file into cache folder
                    move_uploaded_file( $_FILES['imgs']['tmp_name'], $sTempFileName );

                    // change file permission to 644
                    //@chmod($sTempFileName, 0644);
                    
                    if ( file_exists($sTempFileName) && filesize($sTempFileName ) > 0) {
                        $aSize = getimagesize( $sTempFileName ); // try to obtain image info
                            
                            if ( !$aSize ) {
                                @unlink( $sTempFileName );

                                return;

                            }

                        // check for image type
                        switch( $aSize[2] ) {
                            
                            case IMAGETYPE_JPEG:
                                $sExt = '.jpg';
                                // create a new image from file
                                $vImg = @imagecreatefromjpeg( $sTempFileName );
                            break;

                            case IMAGETYPE_PNG:
                                $sExt = '.png';
                                // create a new image from file
                                $vImg = @imagecreatefrompng( $sTempFileName );
                            break;
                            default:
                                @unlink( $sTempFileName );

                            return;

                        }

                        $nameForSave = $nomeSave . $sExt;
                        // create a new true color image
                        $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                        // copy and resize part of an image with resampling
                        imagecopyresampled( $vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h'] );

                        // define a result image filename
                        $sResultFileName = $sTempFileName . $sExt;

                        // output image to file
                        imagejpeg( $vDstImg, $sResultFileName, $iJpgQuality );
                        @unlink( $sTempFileName );

                        //return $sResultFileName;

                    }

                }

            } else{
                /*
                echo "error: " . $_FILES['imgs']['error'] . "<br />" ;
                echo "tamanho size: " . $_FILES['imgs']['size'] . "<br />";
                exit("Erro tamanho");
                */
            }

        }


        //$lastIdEvento = $this->db->lastInsertId();
        $lastIdEvento = $arr[2];
        
        try {
            $sth = $this->db->prepare(' INSERT INTO 
                                            tb_img
                                                (img_id, img_nick_name, img_name, img_size, tb_cadProduto_prod_id, tb_events_event_id, tb_user_user_id, cust_id)
                                        VALUES
                                            (:id, :nickName, :nome, :size, :prodId, :eventId, :userId, :custId)
                                     ');
            $sth->execute(array(
                ':id' => Null,
                ':nickName' => $nickName,
                ':nome' => $nameForSave,
                ':size' => $img['size'],
                ':prodId' => null,
                ':eventId' => $lastIdEvento,
                ':userId' => null,
                ':custId' => Session::get('userCust')
            ));
            
            // return $dirpath:   /home/nteck763/public_html/smartpeeper
            //$dirpath = realpath( dirname( getcwd() ) ) ;

            // Caminho de onde ficará a imagem
            //$caminho_imagem = "/Applications/MAMP/htdocs/SmartPeeper/admin/img/produtos/" . $nome_imagem;
            //$caminho_imagem = $dirpath . "/admin/img/produtos/" . $nome_imagem;
            // Faz o upload da imagem para seu respectivo caminho
            //move_uploaded_file($img['tmp_name'], $caminho_imagem);
            
            $sth->fetchAll();
            $lastIdImg = $this->db->lastInsertId();

            return $arrId = array( $lastIdEvento, $lastIdImg );
            
        } catch (PDOException $e) {
            return $e->getMessage($error, $img['nome']);
        }
    }

    public function formEdit( $idEvento ) {
        
        $id = $idEvento; //id evento a ser editado
        $actn = 'editar';

        $lst = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_events
                                    WHERE
                                        event_id = :idEvento
                                    AND
                                        cust_id = :id 
                                ');
        $lst->execute( array(
                ':idEvento' => $id,
                ':id' => Session::get('userCust')
        ));
        
        return $arr = array( $actn, $lst->fetchAll() );
        
    }
    
    public function edit( $eventoId ) {

        $imgFile = $_FILES['imgs'];
        $valor1 = str_replace( '.', '', $_POST['valor'] );
        $valor = str_replace( ',', '.', $valor1 );
        
        try {
            $sth = $this->db->prepare(' UPDATE 
                                            tb_events 
                                        SET 
                                            event_nome = :eventNome, 
                                            event_valor = :eventValor, 
                                            event_data = :eventData,
                                            event_datasemana = :eventdatasemana,
                                            event_horario = :eventHorario, 
                                            event_desc = :eventDesc
                                        WHERE 
                                            event_id = :eventId 
                                        AND
                                            cust_id = :custId
                                    ');

            $sth->execute(array(
                ':eventId' => $eventoId,
                ':eventNome' => $_POST['name'],
                ':eventValor' => $valor,
                ':eventData' => $_POST['date'],
                ':eventdatasemana' => $_POST['datesemana'],
                ':eventHorario' => $_POST['horario'],
                ':eventDesc' => $_POST['descricao'],
                ':custId' => Session::get('userCust')
            ));

            $imgEventos = $this->db->prepare(' SELECT 
                                                *
                                            FROM 
                                                tb_img 
                                            WHERE 
                                                tb_events_event_id = :id 
                                            AND
                                                cust_id = :custId
                                        ');
            $imgEventos->execute(array(
                ':id' => $eventoId,
                ':custId' => Session::get('userCust')
            ));

            return $ret = array( $eventoId, $imgFile, $imgEventos->fetchAll(), $sth->fetchAll() );
            
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar produto: ' . $_POST['nome']);
        }
    }

    public function editImg( $arrEventos ) {

       
        $idEvento = $arrEventos[0]; // id evetos
        $file = $arrEventos[1]; // img $_FILES
        $arrImgEvento = $arrEventos[2]; // Arr tb_img de eventos
        $nickName = $_POST['nomeImg']; // POST nome img        
        
        foreach ($arrImgEvento as $key => $value) {
            $imgEvento[0] = $value[0];
            $imgEvento[2] = $value[2];
        }

        // Se a foto estiver sido selecionada
        if ($file['name'] != ''):
            
            //$nome = $file['name'];
            $size = $file['size'];

            // return $dirpath:   /home/nteck763/public_html/smartpeeper
            $dirpath = realpath( dirname( getcwd() ) ) ;

            //caminho para deletar img da pasta
            $pathImgProd = $dirpath . '/admin/img/eventos/' . $imgEvento[2];

            // Verifica se o arquivo é uma imagem
            if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $file["type"])):
                return $error[1] = "Isso não é uma imagem.";
            else:
                 //$percent = 0.5;
                //$iWidth = 767 * $percent;
                //$iHeight = 511 * $percent; // desired image result dimensions
                $iWidth = 800;
                $iHeight = 600;
                $iJpgQuality = 95;

                if ( $_FILES ) {
                    // if no errors and size less than 250kb
                    if ( !$_FILES['imgs']['error'] ) {

                        if (is_uploaded_file($_FILES['imgs']['tmp_name'])) {

                            // new unique filename
                            $nomeSave = md5( time().rand() ) ;
                            $sTempFileName = $dirpath . '/admin/img/eventos/' . $nomeSave;

                            // move uploaded file into cache folder
                            move_uploaded_file( $_FILES['imgs']['tmp_name'], $sTempFileName );

                            // change file permission to 644
                            //@chmod($sTempFileName, 0644);
                            
                            if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                                $aSize = getimagesize($sTempFileName); // try to obtain image info
                                    
                                    if (!$aSize) {
                                        @unlink($sTempFileName);

                                        return;

                                    }

                                // check for image type
                                switch($aSize[2]) {
                                    
                                    case IMAGETYPE_JPEG:
                                        $sExt = '.jpg';
                                        // create a new image from file
                                        $vImg = @imagecreatefromjpeg($sTempFileName);
                                    break;

                                    case IMAGETYPE_PNG:
                                        $sExt = '.png';
                                        // create a new image from file
                                        $vImg = @imagecreatefrompng($sTempFileName);
                                    break;
                                    default:
                                        @unlink($sTempFileName);

                                    return;

                                }

                                $nameForSave = $nomeSave . $sExt;
                                // create a new true color image
                                $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                                // copy and resize part of an image with resampling
                                imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                                // define a result image filename
                                $sResultFileName = $sTempFileName . $sExt;

                                // output image to file
                                imagejpeg( $vDstImg, $sResultFileName, $iJpgQuality );
                                @unlink($sTempFileName);
                                unlink($pathImgProd);

                                //return $sResultFileName;

                            }

                        }

                    }

                }

            endif; // fim else catch .img/.jpg ....

            try {
                $sth = $this->db->prepare(' UPDATE 
                                                tb_img 
                                            SET 
                                                img_nick_name = :nickName, 
                                                img_name = :nome, 
                                                img_size = :size
                                            WHERE 
                                                img_id = :idImg
                                            AND
                                                tb_events_event_id = :eventosId 
                                            AND
                                                cust_id = :custId
                                        ');

                $sth->execute(array(
                    ':idImg' => $imgEvento[0],
                    ':nickName' => $nickName,
                    ':nome' => $nameForSave,
                    ':size' => $size,
                    ':eventosId' => $idEvento,
                    ':custId' => Session::get('userCust')
                ));

                $sth->fetchAll();

                // return $dirpath:   /home/nteck763/public_html/smartpeeper
                //$dirpath = realpath( dirname( getcwd() ) ) ;

                // Caminho de onde ficará a imagem
                //$caminho_imagem = "/Applications/MAMP/htdocs/SmartPeeper/admin/img/produtos/" . $nome_imagem;
                //$caminho_imagem = $dirpath  . "/admin/img/produtos/" . $nome_imagem;
                // Faz o upload da imagem para seu respectivo caminho
                //move_uploaded_file($file['tmp_name'], $caminho_imagem);

                //unlink($pathImgProd);
            } catch (PDOException $e) {
                return $e->getMessage('Erro ao atualizar imgaem: ' . $error);
            }

        endif; // fim if tmp_name != ''.

        // inicio if tmp_name == ''. VAZIO
        if ($file['name'] === '') {
            
            try {

                $sth1 = $this->db->prepare(' UPDATE 
                                                tb_img 
                                            SET 
                                                img_nick_name = :nickName 
                                            WHERE 
                                                img_id = :idImg
                                            AND
                                                tb_events_event_id = :idEvento 
                                            AND
                                                cust_id = :custId
                                        ');

                $sth1->execute(array(
                    ':idImg' => $imgEvento[0],
                    ':nickName' => $nickName,
                    ':idEvento' => $idEvento,
                    ':custId' => Session::get('userCust')
                ));

                $sth1->fetchAll();
                
            } catch (PDOException $e) {
                return $e->getMessage('Erro ao atualizar imgaem: ' . $error);
            }
        } // fim if tmp_name  == '' 
    }

    public function delete( $idEvento ) {
        try {
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_events 
                                        WHERE 
                                            event_id = :id 
                                        AND 
                                            cust_id = :custId 
                                     ');

            $sth->execute(array(
                    ':id' => $idEvento,
                    ':custId' => Session::get('userCust')
                ));

            //return  $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar este evento: ' . $_POST['nome']);
        }
    }

    public function deleteImg( $idEvento ) {

        try {
            $del = $this->db->prepare(' SELECT * FROM 
                                            tb_img 
                                        WHERE 
                                            tb_events_event_id = :id 
                                        AND 
                                            cust_id = :custId
                                    ');
            $del->execute(array(
                ':id' => $idEvento,
                ':custId' => Session::get('userCust')
            ));
            $arrImg = $del->fetchAll();

            foreach ($arrImg as $key => $value) :
                $img[2] = $value[2];
            endforeach;

            $sth = $this->db->prepare(' DELETE FROM 
                                            tb_img 
                                        WHERE 
                                            tb_events_event_id = :id 
                                        AND
                                            cust_id = :custId
                                     ');
            $sth->execute(array(
                    ':id' => $idEvento,
                    ':custId' => Session::get('userCust')
                ));
            $sth->fetchAll();

            // return $dirpath:   /home/nteck763/public_html/smartpeeper
            $dirpath = realpath( dirname( getcwd() ) ) ;

            // Caminho de onde ficará a imagem
            $pathImgProd = $dirpath . "/admin/img/eventos/" . $img[2];

            unlink($pathImgProd);

        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar este evento: ' . $_POST['nome']);
        }
    }

    public function imgEvento( $idEvento ) {
        $actn = 'editar';
        try {
            $sth = $this->db->prepare(' SELECT * FROM 
                                            tb_img 
                                        WHERE 
                                            tb_events_event_id = :id 
                                        AND
                                            cust_id = :custId
                                     ');
            $sth->execute(array(
                        ':id' => $idEvento,
                        ':custId' => Session::get('userCust')
                    ));

            return $arr = array( $actn, $sth->fetchAll() );

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao Selecionar esta imagem: ');
        }
    }

    public function emUsoTv( $idEvento ){
        //$cart = 7;
        $sth = $this->db->prepare(' SELECT * FROM 
                                        tb_view 
                                    WHERE
                                        event_id = :idEvento
                                    AND
                                        cust_id = :userCust
                                    ORDER 
                                        by view_id
                                ');
        $sth->execute(array (
            ':idEvento' => $idEvento,
            ':userCust' => Session::get('userCust')
        ));
        $arr = $sth->fetchAll();
        $cont = 0;
        foreach ($arr as $value) {
            $cont = 1;
        }
        if ( $cont > 0 ){
            $res = TRUE;
        } else{
            $res = FALSE;
        }
        return $res;
    } 

}
