<?php

class Login_Model extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {

        $sth = $this->db->prepare(' SELECT * 
                                    FROM 
                                        tb_user 
                                    WHERE 
                                        user_name = :login 
                                    AND 
                                        user_pw = md5(:password)
                                ');

        $sth->execute(array(
            ':login' => $_POST['login'],
            ':password' => $_POST['password']
        ));

        $count = $sth->rowCount();
        if ($count > 0) {

            $userData = $sth->fetchAll();
            foreach ($userData as $value) {
                $user[0] = $value[0];
                $user[1] = $value[1];
                $user[2] = $value[2];
                $user[3] = $value[3];
                $user[4] = $value[4];
            }

            $sth = $this->db->prepare(' SELECT * FROM 
                                            tb_img 
                                         WHERE 
                                            tb_user_user_id = :id 
                                         AND
                                            tb_cadProduto_prod_id is null
                                         AND 
                                            cust_id = :custId
                                    ');
            $sth->execute(array(
                ':id' => $user[0],
                ':custId' => $user[4]
            ));

            $imgUser = $sth->fetchAll();
            foreach ($imgUser as $value) {
                $userImg[2] = $value[2];
            }

            // get img customer
            $sthCust = $this->db->prepare(' SELECT 
                                                cust_fantasia, img_name
                                            from 
                                                tb_customer as c 
                                            inner join
                                                tb_img as img
                                                ON c.cust_id = img.cust_id
                                            where
                                                img.cust_id = :id
                                            AND
                                                tb_user_user_id is null
                                        ');
            $sthCust->execute(array(
                ':id' => $user[4]
            ));

            $imgCust = $sthCust->fetchAll();
            foreach ($imgCust as $value) {
                $custImg[0] = $value[0];
                $custImg[1] = $value[1];
            }

            // verificar tvs!
            $tvs = $this->db->prepare(' SELECT *
                                        FROM tb_customer c
                                        inner join tb_rel_cust_plano cp
                                        on cp.cust_id = c.cust_id
                                        inner join tb_planos p
                                        on p.pl_id = cp.pl_id 
                                        where
                                            c.cust_id = :id
                                    ');
            $tvs->execute(array(
                ':id' => $user[4]
            ));
            $arrTvs = $tvs->fetchAll();
            foreach ($arrTvs as $value) {
                $arrTv[11] = $value[11];

            }

            // login
            Session::init();
            Session::set('dataUrl', $user);
            Session::set('userId', $user[0]);
            Session::set('userPic', $userImg[2]);
            Session::set('userCust', $user[4]);
            Session::set('custName', $custImg[0]);
            Session::set('custImg', $custImg[1]);

            //TV
            Session::set('arrTv', $arrTvs);
            Session::set('qtdtv', $arrTv[11]);

            Session::set('loggedIn', true);
            Session::set('loginUser', $user[1]);
            header('location: ../dashboard');
        } else {
            // show an error
            header('location: ../login&msg_login=xdd527eq3ask34m4m3');
        }

    }

}