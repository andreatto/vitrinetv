<?php

class Medida_Model extends Model {

    public function __construct($param) {
        parent::__construct();
    }

    public function listing() {
        $qry = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_medida 
                                    WHERE 
                                        cust_id = :id
                                ');
        $qry->execute(array(
            ':id' => Session::get('userCust')
        ));
        return $qry->fetchAll();
    }
    
    public function form() {
        $actn = 'inserir';
        return $actn;
    }
   
    public function formEdit($param) {
       
        $idParam = $param;
        $actn = 'editar';
        
        try {
            $sth = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_medida 
                                        WHERE 
                                            med_id = :id 
                                        AND
                                            cust_id = :userCust 
                                    ');
            $sth->execute(array (
                ':id' => $idParam,
                ':userCust' => Session::get('userCust') 
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar listar categoria: ");
        }
        
    }
    
    public function insert() {
        $actn = 'inserir';
        try {
            $sth = $this->db->prepare(' Insert into tb_medida
                                            (med_id, med_nome, cust_id) 
                                        VALUES 
                                            (:med_id, :med_nome, :userCust) 
                                    ');                           

            $sth->execute(array(
                ':med_id' => $_POST['id'] = Null,
                ':med_nome' => $_POST['nome'],
                ':userCust' => Session::get('userCust')
            ));
            return $res = [$actn, $sth->fetchAll()];
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar Produto: " . $_POST['nome']);
            //header('location: form');
        }
        
    }
    
    public function edit($param) {
        $idParam = $param;
        
        try {
            $sth = $this->db->prepare(' UPDATE 
                                            tb_medida 
                                        SET 
                                            med_nome = :med_nome
                                        WHERE 
                                            med_id = :med_id
                                        AND
                                            cust_id = :userCust
                                      ');
            
            $sth->execute(array(
                ':med_id' => $idParam,
                ':med_nome' => $_POST['nome'],
                ':userCust' => Session::get('userCust')
            ));

            return $sth->fetchAll();
            
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar produto: ' . $_POST['nome']);
        }
    }
    
    public function delete($param) {
        $idParam = $param;
        try{
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_medida 
                                        WHERE 
                                            med_id = :id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                    ':id' => $idParam,
                    ':userCust' => Session::get('userCust')
                ));
            $sth->fetchAll();
        }  catch (PDOException $e){
            return $e->getMEsaage('Erro ao deletar esta categoria: ' . $_POST['nome']);
        }
    }

}   