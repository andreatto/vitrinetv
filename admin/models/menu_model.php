<?php

class Menu_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    // Index
    public function pegarTela($param) {
        $sth = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_modulo 
                                    WHERE 
                                        mod_id = :id                                          
                                    AND
                                        cust_id = :userCust 
                                ');
        $sth->execute(array(
            ':id' => $param,
            ':userCust' => Session::get('userCust')
        ));
        $arr = $sth->fetchAll();
        foreach ($arr as $value) {}
        return $value;
    }

    public function getProductMenu($param) {
        $cart = 4;
        $sth = $this->db->prepare(' SELECT * FROM tb_cadProduto as prod
                                    inner join tb_view as v
                                    on v.prod_id = prod.prod_id
                                    AND
                                        v.mod_id = :modId
                                    AND
                                        prod.cust_id = :userCust
                                    AND 
	                                    v.cart_id = :cart
                                    ORDER by v.view_id
                                ');
        $sth->execute(array (
            ':modId' => $param[0],
            ':cart' => $cart,
            ':userCust' => Session::get('userCust')
        ));
        return $arr = array( $param, $sth->fetchAll() );
    }

    // Form 
    public function listingProd($param) {

        $lst = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cadProduto as prod
                                    inner join tb_categoria as cat
                                        on cat.cat_id = prod.tb_categoria_cat_id
                                    inner join tb_medida as med
                                        on med.med_id = prod.tb_medida_med_id
                                    inner join tb_campanha as cam
                                        on cam.camp_id = prod.tb_campanha_camp_id
                                    WHERE 
                                        prod.cust_id = :id 
                                ');
        $lst->execute(array(
                ':id' => Session::get('userCust')
        ));
        return $arr = array( $param, $lst->fetchAll() );
    }

    // Inserir 
    public function insertRelView($param) {
        $tv = $this->pegarTelaId($param);
        $cart = 4;
        try {
            foreach($_REQUEST['cartucho'] as $prod) {
                $sth = $this->db->prepare(' INSERT into tb_view
                                                (view_id, cust_id, tela, mod_id, cart_id, prod_id) 
                                            VALUES 
                                                (:id, :custId, :tela, :modId, :cartId, :prodId) 
                                        ');                          
                $sth->execute(array(
                    ':id' => Null,
                    ':custId' => Session::get('userCust'),
                    ':tela' => $tv,
                    ':modId' => $param,
                    ':cartId' => $cart,
                    ':prodId' => $prod
                ));
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar modulo com cartuchos: ");
        }   
    }
    public function pegarTelaId($param) {
        $sth = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_modulo 
                                        WHERE 
                                            mod_id = :id                                          
                                        AND
                                            cust_id = :userCust 
                                    ');
        $sth->execute(array (
            ':id' => $param,
            ':userCust' => Session::get('userCust')
        ));
        $arrTv = $sth->fetchAll();
        foreach ($arrTv as $tv) {
        }
       
        return $tv[3];
    }

    // Form edit
    public function editRelView($param) {
        $reqProd = $_REQUEST['cartucho'];
        try {
            foreach ($_REQUEST['ids'] as $i => $arrIds) {
                $sth = $this->db->prepare(' UPDATE 
                                                tb_view 
                                            SET 
                                                prod_id = :prodId
                                            WHERE 
                                                view_id = :viewId
                                            AND
                                                mod_id = :modId
                                            AND
                                                cust_id = :custId  
                                        ');

                $sth->execute(array(
                    ':prodId' => $reqProd[$i],
                    ':viewId' => $arrIds,
                    ':modId' => $param[0],
                    ':custId' => Session::get('userCust')
                ));
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar produtos com o cartucho: ");
        }    
    }
  
}