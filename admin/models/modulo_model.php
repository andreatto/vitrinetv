<?php

class Modulo_Model extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listing($tela)
    {
        try {
            $lst = $this->db->prepare(' SELECT 
                                            m.*, 
                                            o.ord_mod_position
                                        FROM 
                                            tb_modulo as m
                                        inner join
                                            tb_order_models as o
                                            on m.mod_id = o.ord_mod_moduloId
                                        WHERE
                                            m.num_tela = :tela
                                        AND
                                            m.cust_id = :id
                                        Order by 
                                            o.ord_mod_position
                                    ');
            $lst->execute(array(
                ':tela' => $tela,
                ':id' => Session::get('userCust')
            ));

            $arrMod = $lst->fetchAll();
            foreach ($arrMod as $value) {
                $idMod = $value[0];
            }

            Session::init();
            Session::set("idTv", $param);
            Session::set("moduloId", $idMod);

            return $arrMod;

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao listar Modulos: ');
        }
    }

    public function form($param)
    {
        $actn = 'inserir';
        return $res = array($actn, $param);
    }

    public function insert($numTela)
    {

        $actn = 'inserir';
        try {
            $sth = $this->db->prepare(' INSERT into tb_modulo
                                            (mod_id, mod_nome, cust_id, num_tela, mod_ativo) 
                                        VALUES 
                                            (:id, :nome, :userCust, :num_tela, :ativo) 
                                    ');

            $sth->execute(array(
                ':id' => null,
                ':nome' => $_POST['nome'],
                ':userCust' => Session::get('userCust'),
                ':num_tela' => $numTela,
                ':ativo' => 1
            ));
            $result = $sth->fetchAll();
            $lastIdMod = $this->db->lastInsertId();
            return $res = array($actn, $numTela, $result, $lastIdMod);
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar o produto: " . $_POST['nome']);
            //header('location: form');
        }
    }

    public function insertRelModCart()
    {
        $actn = 'inserir';
        $lastIdMod = $this->db->lastInsertId();

        for ($i = 1; $i <= 7; $i++) {
            if (in_array($i, $_POST['chckCart'])) {
                $flag = 1;
            } else {
                $flag = 0;
            };

            $sth = $this->db->prepare(' INSERT into tb_rel_mod_cart
                                            (mod_cart_id, cust_id, mod_id, cart_id, mod_cart_flag_ativo) 
                                        VALUES 
                                            (:id, :custId, :modId, :cartId, :flagAtivo) 
                                    ');
            $sth->execute(array(
                ':id' => null,
                ':custId' => Session::get('userCust'),
                ':modId' => $lastIdMod,
                ':cartId' => $i,
                ':flagAtivo' => $flag
            ));
            $sth->fetchAll();
        }
        return;
    }

    public function formEdit($param)
    {
        $actn = 'editar';
        try {
            $sth = $this->db->prepare(' SELECT * FROM tb_modulo 
                                        WHERE
                                            mod_id = :modulo
                                        AND
                                            cust_id = :userCust 
                                    ');
            $sth->execute(array(
                ':modulo' => $param,
                ':userCust' => Session::get('userCust')
            ));
            return $res = array($actn, $param, $sth->fetchAll());
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar listar modulos: ");
        }
    }

    public function edit($param)
    {
        try {
            // Update modulo
            $sth = $this->db->prepare(' UPDATE 
                                            tb_modulo 
                                        SET 
                                            mod_nome = :nome
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                        ');

            $sth->execute(array(
                ':id' => $param,
                ':nome' => $_POST['nome'],
                ':userCust' => Session::get('userCust')
            ));
            $sth->fetchAll();

            $tela = $this->pegarTela($param);
            return $tela;
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar modulo: ' . $_POST['nome']);
        }
    }

    public function editRelCart($param)
    {

        $arrOld = array();
        //$arrPrx = array();

        $arrIdOld = $this->arrCartucho($param);
        foreach ($arrIdOld as $key => $arrIdBefore) {
            $arrOld[] = $arrIdBefore[1];
        }

        for ($i = 0; $i < count($_POST['chckCart']); $i++) {
            //$arrPrx = $_POST['chckCart'][$i];
            //array_search($i, $arrAtual)
            if (array_search($_POST['chckCart'][$i], $arrOld) !== false) {
                //code goes here (
               // echo "TRUE ";
                //echo $_POST['chckCart'][$i] . "<br />";
            } else {
                 //echo "FALSE ";
                 //echo $_POST['chckCart'][$i] . "<br />";
                $this->editAddRelCart(Session::get('userCust'), $param, $_POST['chckCart'][$i], 0);
            }
        }
        
        //$result = array_diff_assoc( $arrAtual, $arrPrx );
        //echo $result;
        
        //exit();
        $arrId = $this->arrCartucho($param);

        try {
            foreach ($arrId as $key => $arrIdCart) {
                if (in_array($arrIdCart[1], $_POST['chckCart'])) {
                    $flag = 1;
                } else {
                    $flag = 0;
                };

                $sth = $this->db->prepare(' UPDATE 
                                                tb_rel_mod_cart 
                                            SET 
                                                cart_id = :cartId,
                                                mod_cart_flag_ativo = :flag
                                            WHERE 
                                                mod_id = :param
                                            AND
                                                cust_id = :custId  
                                            AND
                                                mod_cart_id = :moduloId
                                        ');

                $sth->execute(array(
                    ':cartId' => $arrIdCart[1],
                    ':flag' => $flag,
                    ':moduloId' => $arrIdCart[0],
                    ':param' => $param,
                    ':custId' => Session::get('userCust')
                ));
            }
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage("Erro ao relacionar modulo com cartuchos: ");
        }
    }

    public function editAddRelCart($cust_id, $mod_id, $cart_id, $mod_cart_flag_ativo)
    {

        try {
            $sth = $this->db->prepare(' INSERT into tb_rel_mod_cart
                                            (mod_cart_id, cust_id, mod_id, cart_id, mod_cart_flag_ativo) 
                                        VALUES 
                                            (:id, :custId, :modId, :cartId, :flagAtivo) 
                                    ');
            $sth->execute(array(
                ':id' => null,
                ':custId' => $cust_id,
                ':modId' => $mod_id,
                ':cartId' => $cart_id,
                ':flagAtivo' => $mod_cart_flag_ativo
            ));

            return $sth->fetchAll();

        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function editModuloAtivar($modId, $target)
    {

        $sendModId = $_POST['modId'];
        $sendTarget = $_POST['target'];

        if ($sendTarget == 0) {
            $ativador = 1;
        } else if ($sendTarget == 1) {
            $ativador = 0;
        }

        try {
            $sth = $this->db->prepare(' UPDATE 
                                                tb_modulo 
                                            SET 
                                                mod_ativo = :ativo
                                            WHERE 
                                                mod_id = :modID
                                            AND
                                                cust_id = :custId
                                        ');

            $sth->execute(array(
                ':modID' => $sendModId,
                ':ativo' => $ativador,
                ':custId' => Session::get('userCust')
            ));
            $sth->fetchAll();

        } catch (PDOException $e) {

            return $e->getMessage("Erro ao atualizar modulo para ativado ou desativado : ");

        }
    }

    public function deleteRelCart($param)
    {
        try {
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_rel_mod_cart 
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                ':id' => $param,
                ':userCust' => Session::get('userCust')
            ));
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar esta relacao: ' . $_POST['nome']);
        }
    }

    public function deleteView($param)
    {
        foreach ($param as $arrViews) {
            $delView = $this->db->prepare(' DELETE 
                                                FROM 
                                                    tb_view 
                                                WHERE 
                                                    view_id = :viewId
                                                AND
                                                    cust_id = :userCust
                                            ');
            $delView->execute(array(
                ':viewId' => $arrViews[0],
                ':userCust' => Session::get('userCust')
            ));
            $delView->fetchAll();
        }
        return;
    }

    public function delete($modId)
    {
        try {
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_modulo 
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                ':id' => $modId,
                ':userCust' => Session::get('userCust')
            ));
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao deletar este modulo!');
        }
    }

    public function pegarTela($modId)
    {
        // Para retornar id tela
        $sth = $this->db->prepare(' SELECT 
                                            *
                                        FROM 
                                            tb_modulo 
                                        WHERE 
                                            mod_id = :id                                          
                                        AND
                                            cust_id = :userCust 
                                    ');
        $sth->execute(array(
            ':id' => $modId,
            ':userCust' => Session::get('userCust')
        ));
        $arrTela = $sth->fetchAll();
        foreach ($arrTela as $value) {
            $tela[0] = $value[3];
        }
        return $tela[0];
    }

    public function myCartucho($tela)
    {

        $sth = $this->db->prepare(' SELECT * FROM tb_modulo as modu
                                    inner join tb_rel_mod_cart as rel
                                    on modu.mod_id = rel.mod_id
                                    inner join tb_cartucho as cart
                                    on cart.cart_id = rel.cart_id
                                    WHERE
                                        modu.num_tela = :tela
                                    AND
                                        modu.cust_id = rel.cust_id 
                                    AND
                                        modu.cust_id = :userCust
                                    AND
	                                    rel.mod_cart_flag_ativo = 1
                                ');
        $sth->execute(array(
            ':tela' => $tela,
            ':userCust' => Session::get('userCust')
        ));

        return $sth->fetchAll();
    }

    public function cartucho()
    {
        // Para retornar id tela
        $sth = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cartucho 
                                    WHERE 
                                        cart_flag_ativo = 1
                                    AND
                                        cart_flag_free = 1 
                                ');
        $sth->execute();
        return $sth->fetchAll();
    }

    public function cartuchoBy()
    {
        // Para retornar id tela
        $sth = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cartucho 
                                    WHERE 
                                        cart_flag_ativo = 1
                                    AND
                                        cart_flag_buy = 1 
                                    AND
                                        cart_flag_free = 0
                                ');
        $sth->execute();
        return $sth->fetchAll();
    }

    public function getMyCartucho($param)
    {

        $sth = $this->db->prepare(' SELECT * 
                                    from tb_rel_mod_cart as rel
                                    inner join tb_cartucho as cart
                                    on cart.cart_id = rel.cart_id
                                    where 
                                        rel.mod_id = :modulo
                                    AND
                                        rel.cust_id = :userCust
                                    AND 
                                        rel.mod_cart_flag_ativo = 1
                                ');
        $sth->execute(array(
            ':modulo' => $param,
            ':userCust' => Session::get('userCust')
        ));
        return $sth->fetchAll();
    }

    public function arrCartucho($param)
    {
        $lst = $this->db->prepare(' SELECT rel.mod_cart_id, rel.cart_id 
                                    from tb_rel_mod_cart as rel
                                    inner join tb_cartucho as cart
                                    on cart.cart_id = rel.cart_id
                                    where 
                                        rel.mod_id = :modulo
                                    AND
                                        rel.cust_id = :id
                                    order by 
                                        cart.cart_id
                                ');
        $lst->execute(array(
            ':modulo' => $param,
            ':id' => Session::get('userCust')
        ));
        return $lst->fetchAll();
    }

    public function selectView($param)
    {
        try {
            $sth = $this->db->prepare(' SELECT
                                            * 
                                        FROM 
                                            tb_view 
                                        WHERE 
                                            mod_id = :id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                ':id' => $param,
                ':userCust' => Session::get('userCust')
            ));
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao deletar esta View !');
        }
    }

    public function getModulo($arg)
    {
        try {
            $lst = $this->db->prepare(' SELECT 
                                            * 
                                        FROM 
                                            tb_modulo
                                        WHERE
                                            mod_id = :modID
                                        AND
                                            cust_id = :id
                                    ');
            $lst->execute(array(
                ':modID' => $arg,
                ':id' => Session::get('userCust')
            ));

            $arrMod = $lst->fetchAll();

            return $arrMod;

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao listar Modulos: ');
        }
    }

    public function insertSequenceCarouselData($modId)
    {


        try {
            $sth = $this->db->prepare(' INSERT into tb_order_models
                                            ( ord_mod_id, ord_mod_moduloId, ord_mod_position, cust_id ) 
                                        VALUES 
                                            (:id, :modId, :position, :custId) 
                                    ');
            $sth->execute(array(
                ':id' => null,
                ':modId' => $modId,
                ':position' => 9999,
                ':custId' => Session::get('userCust')
            ));
            $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage("Erro ao cadastrar sequencia dos modulos! ");
        }
    }

    public function updateSequenceCarouselData($listCarousel)
    {

        // get the list of items id separated by cama (,)
        $list_order = $_POST['list_order'];
        // convert the string list to an array
        $list = explode(',', $list_order);
        $i = 1;

        try {

            foreach ($list as $id) {
                // Update modulo
                $sth = $this->db->prepare(' UPDATE 
                                                tb_order_models 
                                            SET 
                                                ord_mod_position = :position
                                            WHERE 
                                                ord_mod_moduloId = :id
                                            AND
                                                cust_id = :userCust
                                            ');

                $sth->execute(array(
                    ':id' => $id,
                    ':position' => $i,
                    ':userCust' => Session::get('userCust')
                ));
                $i++;

            }// fim foreach

            $sth->fetchAll();

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar sequencia dos módulos : ' . $_POST['nome']);
        }

    }

    public function deleteSequenceCarousel($ordModId)
    {
        try {
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_order_models 
                                        WHERE 
                                            ord_mod_id = :ord_mod_id
                                        AND
                                            cust_id = :userCust
                                    ');
            $sth->execute(array(
                ':ord_mod_id' => $ordModId,
                ':userCust' => Session::get('userCust')
            ));
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar esta sequencia do modulo: ' . $_POST['nome']);
        }
    }

    public function selectUniqueCarousel($modId)
    {
        try {
            $sth = $this->db->prepare(' SELECT 
                                            ord_mod_id
                                        FROM 
                                            tb_order_models 
                                        WHERE 
                                            ord_mod_moduloId = :mod_id                                          
                                        AND
                                            cust_id = :userCust 
                                    ');
            $sth->execute(array(
                ':mod_id' => $modId,
                ':userCust' => Session::get('userCust')
            ));

            $ordModId = $sth->fetchAll();

            return $ordModId[0][0];

        } catch (PDOException $e) {
            echo $e->getMessage("Erro ao selecionar as sequencias dos modulos");
        }
    }
}