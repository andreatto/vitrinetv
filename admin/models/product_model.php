<?php

class Product_Model extends Model
{

    public function __construct($param)
    {
        parent::__construct();
    }

    public function listing()
    {
        $lst = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cadProduto as prod
                                    inner join tb_categoria as cat
                                        on cat.cat_id = prod.tb_categoria_cat_id
                                    inner join tb_medida as med
                                        on med.med_id = prod.tb_medida_med_id
                                    inner join tb_campanha as cam
                                        on cam.camp_id = prod.tb_campanha_camp_id
                                    WHERE 
                                        prod.cust_id = :id 
                                ');
        $lst->execute(array(
            ':id' => Session::get('userCust')
        ));
        return $lst->fetchAll();
    }

    public function form()
    {
        $actn = 'inserir';

        $cat = $this->db->prepare(' SELECT 
                                    	* 
                                    FROM 
                                        tb_categoria
                                    WHERE
                                        cust_id = :id
                                ');
        $cat->execute(array(
            ':id' => Session::get('userCust')
        ));
        //$cat->fetch(PDO::FETCH_ASSOC);

        $med = $this->db->prepare(' SELECT 
                                        *
                                    FROM 
                                        tb_medida 
                                    WHERE
                                        cust_id = :id
                                ');
        $med->execute(array(
            ':id' => Session::get('userCust')
        ));

        $cam = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
	                                    tb_campanha
                                    WHERE
                                        cust_id = :id
                                ');
        $cam->execute(array(
            ':id' => Session::get('userCust')
        ));

        return $arr = array($actn, $cat->fetchAll(), $med->fetchAll(), $cam->fetchAll());
    }

    public function insert()
    {

        $img = $_FILES['imgs'];
        $valor1 = str_replace('.', '', $_POST['valor']);
        $valor = str_replace(',', '.', $valor1);

        try {
            $sth = $this->db->prepare(' INSERT INTO 
                                        tb_cadProduto
                                            (prod_id, 
                                             prod_nome, 
                                             prod_valor, 
                                             prod_desc,
                                             tb_campanha_camp_id, 
                                             tb_medida_med_id, 
                                             tb_categoria_cat_id,
                                             cust_id) 
                                        VALUES 
                                            (:prod_id, 
                                            :prod_nome, 
                                            :prod_valor,
                                            :prod_desc,
                                            :tb_campanha_camp_id, 
                                            :tb_medida_med_id, 
                                            :tb_categoria_cat_id,
                                            :custId) 
                                    ');

            $sth->execute(array(
                ':prod_id' => null,
                ':prod_nome' => $_POST['name'],
                ':prod_valor' => $valor,
                ':prod_desc' => $_POST['descricao'],
                ':tb_campanha_camp_id' => $_POST['campanha'],
                ':tb_medida_med_id' => $_POST['medida'],
                ':tb_categoria_cat_id' => $_POST['categoria'],
                ':custId' => Session::get('userCust')
            ));

            return $arr = array($img, $sth->fetchAll());

        } catch (PDOException $e) {
            return $e->getMessage("Erro ao cadastrar Produto: ");
            //header('location: form');
        }
    }

    public function insertImg($arr)
    {

        $img = $arr[0];
        //$name = $img['name'];
        //$size = $img['size'];
        $nickName = $_POST['nomeImg'];
        // return $dirpath:   /home/nteck763/public_html/vitrinetv
        $dirpath = realpath(dirname(getcwd()));
                
        //$percent = 0.5;
        //$iWidth = 767 * $percent;
        //$iHeight = 511 * $percent; // desired image result dimensions
        $iWidth = 800;
        $iHeight = 600;
        $iJpgQuality = 95;

        if ($_FILES) {
            // if no errors and size less than 250kb
            if (!$_FILES['imgs']['error']) {

                if (is_uploaded_file($_FILES['imgs']['tmp_name'])) {

                    // new unique filename
                    $nomeSave = md5(time() . rand());
                    $sTempFileName = $dirpath . '/admin/img/produtos/' . $nomeSave;

                    // move uploaded file into cache folder
                    move_uploaded_file($_FILES['imgs']['tmp_name'], $sTempFileName);

                    // change file permission to 644
                    //@chmod($sTempFileName, 0644);

                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                        $aSize = getimagesize($sTempFileName); // try to obtain image info

                        if (!$aSize) {
                            @unlink($sTempFileName);

                            return;

                        }

                        // check for image type
                        switch ($aSize[2]) {

                            case IMAGETYPE_JPEG:
                                $sExt = '.jpg';
                                // create a new image from file
                                $vImg = @imagecreatefromjpeg($sTempFileName);
                                break;

                            case IMAGETYPE_PNG:
                                $sExt = '.png';
                                // create a new image from file
                                $vImg = @imagecreatefrompng($sTempFileName);
                                break;
                            default:
                                @unlink($sTempFileName);

                                return;

                        }

                        $nameForSave = $nomeSave . $sExt;
                        // create a new true color image
                        $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);

                        // copy and resize part of an image with resampling
                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                        // define a result image filename
                        $sResultFileName = $sTempFileName . $sExt;

                        // output image to file
                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                        @unlink($sTempFileName);

                        //return $sResultFileName;

                    }

                }

            } else {
                /*
                echo "error: " . $_FILES['imgs']['error'] . "<br />" ;
                echo "tamanho size: " . $_FILES['imgs']['size'] . "<br />";
                exit("Erro tamanho");
                 */
            }

        }


        $lastIdProd = $this->db->lastInsertId();

        try {
            $sth = $this->db->prepare(' INSERT INTO 
                                            tb_img
                                                (img_id, img_nick_name, img_name, img_size, tb_cadProduto_prod_id, tb_events_event_id, tb_user_user_id, cust_id)
                                        VALUES
                                            (:id, :nickName, :nome, :size, :prodId, :eventId, :userId, :custId)
                                     ');
            $sth->execute(array(
                ':id' => null,
                ':nickName' => $nickName,
                ':nome' => $nameForSave,
                ':size' => $img['size'],
                ':prodId' => $lastIdProd,
                ':eventId' => null,
                ':userId' => null,
                ':custId' => Session::get('userCust')
            ));
            
            // return $dirpath:   /home/nteck763/public_html/vitrinetv
            //$dirpath = realpath( dirname( getcwd() ) ) ;

            // Caminho de onde ficará a imagem
            //$caminho_imagem = "/Applications/MAMP/htdocs/vitrinetv/admin/img/produtos/" . $nome_imagem;
            //$caminho_imagem = $dirpath . "/admin/img/produtos/" . $nome_imagem;
            // Faz o upload da imagem para seu respectivo caminho
            //move_uploaded_file($img['tmp_name'], $caminho_imagem);

            $sth->fetchAll();
            $lastIdImg = $this->db->lastInsertId();

            return $arrId = array($lastIdProd, $lastIdImg);

        } catch (PDOException $e) {
            return $e->getMessage($error, $img['nome']);
        }
    }

    public function formEdit($param)
    {
        $id = $param; //id produto a ser editado
        $actn = 'editar';

        $cat = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_categoria 
                                    WHERE
                                        cust_id = :custId
                                ');
        $cat->execute(array(
            ':custId' => Session::get('userCust')
        ));
        //$cat->fetch(PDO::FETCH_ASSOC);

        $med = $this->db->prepare(' SELECT 
                                        * 
                                    FROM
	                                    tb_medida 
                                    WHERE
                                        cust_id = :custId
                                ');
        $med->execute(array(
            ':custId' => Session::get('userCust')
        ));
        //$med->$cat->fetch(PDO::FETCH_ASSOC);

        $cam = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_campanha 
                                    WHERE
                                        cust_id = :custId 
                                    ');
        $cam->execute(array(
            ':custId' => Session::get('userCust')
        ));
        //$cam->fetch(PDO::FETCH_ASSOC);

        $img = $this->db->prepare(' SELECT 
                                        *
                                    FROM 
                                        tb_img 
                                    WHERE 
                                        tb_cadProduto_prod_id = :id 
                                    AND 
                                        cust_id = :custId
                                ');
        $img->execute(array(
            ':id' => $id,
            ':custId' => Session::get('userCust')
        ));

        $prod = $this->db->prepare(' SELECT 
                                        * 
                                    FROM 
                                        tb_cadProduto 
                                    WHERE 
                                        prod_id = :id 
                                    AND
                                        cust_id = :custId
                                ');
        $prod->execute(array(
            ':id' => $id,
            ':custId' => Session::get('userCust')
        ));
        return $arr = array($actn, $cat->fetchAll(), $med->fetchAll(), $cam->fetchAll(), $prod->fetchAll(), $img->fetchAll());
    }

    public function edit($param)
    {

        $imgFile = $_FILES['imgs'];
        $valor1 = str_replace('.', '', $_POST['valor']);
        $valor = str_replace(',', '.', $valor1);

        try {
            $sth = $this->db->prepare(' UPDATE 
                                            tb_cadProduto 
                                        SET prod_nome = :prod_nome, 
                                            prod_valor = :prod_valor,
                                            prod_desc = :prod_desc,
                                            tb_campanha_camp_id = :tb_campanha_camp_id, 
                                            tb_medida_med_id = :tb_medida_med_id, 
                                            tb_categoria_cat_id = :tb_categoria_cat_id
                                        WHERE 
                                            prod_id = :prod_id 
                                        AND
                                            cust_id = :custId
                                    ');

            $sth->execute(array(
                ':prod_id' => $param,
                ':prod_nome' => $_POST['name'],
                ':prod_valor' => $valor,
                ':prod_desc' => $_POST['descricao'],
                ':tb_campanha_camp_id' => $_POST['campanha'],
                ':tb_medida_med_id' => $_POST['medida'],
                ':tb_categoria_cat_id' => $_POST['categoria'],
                ':custId' => Session::get('userCust')
            ));

            $imgProd = $this->db->prepare(' SELECT 
                                                *
                                            FROM 
                                                tb_img 
                                            WHERE 
                                                tb_cadProduto_prod_id = :id 
                                            AND
                                                cust_id = :custId
                                        ');
            $imgProd->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));

            return $ret = array($param, $imgFile, $imgProd->fetchAll(), $sth->fetchAll());

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao editar produto: ' . $_POST['nome']);
        }
    }

    public function editImg($param)
    {

        $idProd = $param[0]; // id Produtos
        $file = $param[1]; // img $_FILES
        $arrImgProd = $param[2]; // Arr tb_img de prod
        $nickName = $_POST['nomeImg']; // POST nome img

        foreach ($arrImgProd as $key => $value) {
            $imgProd[0] = $value[0];
            $imgProd[2] = $value[2];
        }

        // Se a foto estiver sido selecionada
        if ($file['name'] != '') :

            //$nome = $file['name'];
        $size = $file['size'];

            // return $dirpath:   /home/nteck763/public_html/vitrinetv
        $dirpath = realpath(dirname(getcwd()));

            //caminho para deletar img da pasta
        $pathImgProd = $dirpath . "/admin/img/produtos/" . $imgProd[2];

            // Verifica se o arquivo é uma imagem
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $file["type"])) :
            return $error[1] = "Isso não é uma imagem.";
        else :
                 //$percent = 0.5;
                //$iWidth = 767 * $percent;
                //$iHeight = 511 * $percent; // desired image result dimensions
        $iWidth = 800;
        $iHeight = 600;
        $iJpgQuality = 95;

        if ($_FILES) {
                    // if no errors and size less than 250kb
            if (!$_FILES['imgs']['error']) {

                if (is_uploaded_file($_FILES['imgs']['tmp_name'])) {

                            // new unique filename
                    $nomeSave = md5(time() . rand());
                    $sTempFileName = $dirpath . '/admin/img/produtos/' . $nomeSave;

                            // move uploaded file into cache folder
                    move_uploaded_file($_FILES['imgs']['tmp_name'], $sTempFileName);

                            // change file permission to 644
                            //@chmod($sTempFileName, 0644);

                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                        $aSize = getimagesize($sTempFileName); // try to obtain image info

                        if (!$aSize) {
                            @unlink($sTempFileName);

                            return;

                        }

                                // check for image type
                        switch ($aSize[2]) {

                            case IMAGETYPE_JPEG:
                                $sExt = '.jpg';
                                        // create a new image from file
                                $vImg = @imagecreatefromjpeg($sTempFileName);
                                break;

                            case IMAGETYPE_PNG:
                                $sExt = '.png';
                                        // create a new image from file
                                $vImg = @imagecreatefrompng($sTempFileName);
                                break;
                            default:
                                @unlink($sTempFileName);

                                return;

                        }

                        $nameForSave = $nomeSave . $sExt;
                                // create a new true color image
                        $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);

                                // copy and resize part of an image with resampling
                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                                // define a result image filename
                        $sResultFileName = $sTempFileName . $sExt;

                                // output image to file
                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                        @unlink($sTempFileName);
                        unlink($pathImgProd);

                                //return $sResultFileName;

                    }

                }

            }

        }

        endif; // fim else catch .img/.jpg ....

        try {
            $sth = $this->db->prepare(' UPDATE 
                                                tb_img 
                                            SET 
                                                img_nick_name = :nickName, 
                                                img_name = :nome, 
                                                img_size = :size
                                            WHERE 
                                                img_id = :idImg
                                            AND
                                                tb_cadProduto_prod_id = :prodId 
                                            AND
                                                cust_id = :custId
                                        ');

            $sth->execute(array(
                ':idImg' => $imgProd[0],
                ':nickName' => $nickName,
                ':nome' => $nameForSave,
                ':size' => $size,
                ':prodId' => $idProd,
                ':custId' => Session::get('userCust')
            ));

            $sth->fetchAll();

                // return $dirpath:   /home/nteck763/public_html/vitrinetv
                //$dirpath = realpath( dirname( getcwd() ) ) ;

                // Caminho de onde ficará a imagem
                //$caminho_imagem = "/Applications/MAMP/htdocs/vitrinetv/admin/img/produtos/" . $nome_imagem;
                //$caminho_imagem = $dirpath  . "/admin/img/produtos/" . $nome_imagem;
                // Faz o upload da imagem para seu respectivo caminho
                //move_uploaded_file($file['tmp_name'], $caminho_imagem);

                //unlink($pathImgProd);
        } catch (PDOException $e) {
            return $e->getMessage('Erro ao atualizar imgaem: ' . $error);
        }

        endif; // fim if tmp_name != ''.

        // inicio if tmp_name == ''. VAZIO
        if ($file['name'] === '') {

            try {

                $sth1 = $this->db->prepare(' UPDATE 
                                                tb_img 
                                            SET 
                                                img_nick_name = :nickName 
                                            WHERE 
                                                img_id = :idImg
                                            AND
                                                tb_cadProduto_prod_id = :prodId 
                                            AND
                                                cust_id = :custId
                                        ');

                $sth1->execute(array(
                    ':idImg' => $imgProd[0],
                    ':nickName' => $nickName,
                    ':prodId' => $idProd,
                    ':custId' => Session::get('userCust')
                ));

                $sth1->fetchAll();

            } catch (PDOException $e) {
                return $e->getMessage('Erro ao atualizar imagem: ' . $error);
            }
        } // fim if tmp_name  == '' 
    }

    public function delete($param)
    {
        try {
            $sth = $this->db->prepare(' DELETE 
                                        FROM 
                                            tb_cadProduto 
                                        WHERE 
                                            prod_id = :id 
                                        AND 
                                            cust_id = :custId');

            $sth->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));

            //return  $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar este produto: ' . $_POST['nome']);
        }
    }

    public function deleteImg($param)
    {

        try {
            $del = $this->db->prepare(' SELECT * FROM 
                                            tb_img 
                                        WHERE 
                                            tb_cadProduto_prod_id = :id 
                                        AND 
                                            cust_id = :custId
                                    ');
            $del->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));
            $arrImg = $del->fetchAll();

            foreach ($arrImg as $key => $value) :
                $img[2] = $value[2];
            endforeach;

            $sth = $this->db->prepare(' DELETE FROM 
                                            tb_img 
                                        WHERE 
                                            tb_cadProduto_prod_id = :id 
                                        AND
                                            cust_id = :custId
                                     ');
            $sth->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));
            $sth->fetchAll();

            // return $dirpath:   /home/nteck763/public_html/vitrinetv
            $dirpath = realpath(dirname(getcwd()));

            // Caminho de onde ficará a imagem
            $pathImgProd = $dirpath . "/admin/img/produtos/" . $img[2];

            unlink($pathImgProd);

        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar este produto: ' . $_POST['nome']);
        }
    }

    public function emUsoTv($param)
    {
        $sth = $this->db->prepare(' SELECT * FROM 
                                        tb_view 
                                    WHERE
                                        prod_id = :param
                                    AND
                                        cust_id = :userCust
                                    ORDER 
                                        by view_id
                                ');
        $sth->execute(array(
            ':param' => $param,
            ':userCust' => Session::get('userCust')
        ));
        $arr = $sth->fetchAll();
        $cont = 0;
        foreach ($arr as $value) {
            $cont = 1;
        }
        if ($cont > 0) {
            $res = true;
        } else {
            $res = false;
        }
        return $res;
    }

    public function imgProd($param)
    {
        try {
            $sth = $this->db->prepare(' SELECT * FROM 
                                            tb_img 
                                        WHERE 
                                            tb_cadProduto_prod_id = :id 
                                     ');
            $sth->execute(array(
                ':id' => $param
            ));

            return $arr = $sth->fetchAll();

        } catch (PDOException $e) {
            return $e->getMessage('Erro ao Selecionar esta imagem: ');
        }
    }

    public function insertRelacao($arrIds)
    {
        //$lastIdProd = $this->db->lastInsertId();
        print_r($arrIds);
        exit();
    }
}
