<?php

class User_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function listing() {

        try {
            $sth = $this->db->prepare(' SELECT * FROM
                                            tb_user
                                        WHERE
                                            cust_id = :id
                                    ');
            $sth->execute(array(
                ':id' => Session::get('userCust')
            ));
            return $arr = array( $actn, $sth->fetchAll() );
        } catch (PDOException $e) {
            return $e->getMessage();
        }

    } // fim listig

    public function form() {
       $actn = '';
       return $arr = array( $actn );
    }

    public function insert() {

        $img = $_FILES['imgs'];
        $actn = 'inserir';

        try {
            $sth = $this->db->prepare(' INSERT INTO
                                            tb_user
                                            (user_id, user_name, user_email, user_pw, cust_id)
                                        VALUES
                                            (:id, :nome, :email, md5(:senha), :custId)
                                     ');

            $sth->execute(array(
                ':id' => null,
                ':nome' => $_POST['name'],
                ':email' => $_POST['email'],
                ':senha' => $_POST['pw'],
                ':custId' => Session::get('userCust')
            ));

            return $arr = array( $actn, $sth->fetchAll(), $img );

        } catch (Exception $e) {
            return $e->getMessage("Erro ao cadastrar usuário: ");
        }

    } // fim insert

    public function insertImg($arr) {

        $img = $arr[2];
        $nickName = $_POST['nomeImg'];
        $dirpath = realpath( dirname( getcwd() ) ) ;

        //$percent = 0.5;
        //$iWidth = 767 * $percent;
        //$iHeight = 511 * $percent; // desired image result dimensions
        $iWidth = 200;
        $iHeight = 220;
        $iJpgQuality = 95;

        if ($_FILES) {
            // if no errors and size less than 250kb
            if (! $_FILES['imgs']['error'] ) {

                if (is_uploaded_file($_FILES['imgs']['tmp_name'])) {

                    // new unique filename
                    $nomeSave = md5( time().rand() ) ;
                    $sTempFileName = $dirpath . '/admin/img/user/' . $nomeSave;

                    // move uploaded file into cache folder
                    move_uploaded_file( $_FILES['imgs']['tmp_name'], $sTempFileName );

                    // change file permission to 644
                    //@chmod($sTempFileName, 0644);

                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                        $aSize = getimagesize($sTempFileName); // try to obtain image info

                            if (!$aSize) {
                                @unlink($sTempFileName);

                                return;

                            }

                        // check for image type
                        switch($aSize[2]) {

                            case IMAGETYPE_JPEG:
                                $sExt = '.jpg';
                                // create a new image from file
                                $vImg = @imagecreatefromjpeg($sTempFileName);
                            break;

                            case IMAGETYPE_PNG:
                                $sExt = '.png';
                                // create a new image from file
                                $vImg = @imagecreatefrompng($sTempFileName);
                            break;
                            default:
                                @unlink($sTempFileName);

                            return;

                        }

                        $nameForSave = $nomeSave . $sExt;
                        // create a new true color image
                        $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                        // copy and resize part of an image with resampling
                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                        // define a result image filename
                        $sResultFileName = $sTempFileName . $sExt;

                        // output image to file
                        imagejpeg( $vDstImg, $sResultFileName, $iJpgQuality );
                        @unlink($sTempFileName);

                        //return $sResultFileName;

                    }

                }

            }

        }


        $lastIdUser = $this->db->lastInsertId();

        try {
            $sth = $this->db->prepare(' INSERT INTO
                                            tb_img
                                                (img_id, img_nick_name, img_name, img_size, tb_user_user_id, cust_id)
                                        VALUES
                                            (:id, :nickName, :nome, :size, :userId, :custId)
                                     ');
            $sth->execute(array(
                ':id' => Null,
                ':nickName' => $nickName,
                ':nome' => $nameForSave,
                ':size' => $img['size'],
                ':userId' => $lastIdUser,
                ':custId' => Session::get('userCust')
            ));

            //$shmod = chmod($dirpath . "/admin/img/user/", 0777);
           // $caminho_imagem = $dirpath . "/admin/img/userCust/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            //move_uploaded_file( $img['tmp_name'], $caminho_imagem );

            $sth->fetchAll();
            $lastIdImg = $this->db->lastInsertId();

            return $arrId = array( $lastIdProd, $lastIdImg );

        } catch (PDOException $e) {
            return $e->getMessage($error, $img['nome']);
        }
    }

    /*
        public function insertImg($arr) {

        $img = $arr[2];
        //$name = $img['name'];
        //$size = $img['size'];
        $nickName = $_POST['nomeImg'];

        // return $dirpath:   /home/nteck763/public_html/smartpeeper
        $dirpath = realpath( dirname( getcwd() ) ) ;

        // Se a foto estiver sido selecionada
        if (!empty($img['name'])):
            // Verifica se o arquivo é uma imagem
            if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $img["type"])):
                $error[1] = "Isso não é uma imagem.";
            else:
                // Largura máxima em pixels
                $largura = 200;
                // Altura máxima em pixels
                $altura = 220;
                // Tamanho máximo do arquivo em bytes
                $tamanho = 900000;

                // Pegar as dimensões da imagem
                $dimensoes = getimagesize($img["tmp_name"]);
                // Verifica se a largura da imagem é maior que a largura permitida
                if ($dimensoes[0] > $largura):
                    $error[2] = "A largura da imagem não deve ultrapassar " . $largura . " pixels";
                endif;
                // Verifica se a altura da imagem é maior que a altura permitida
                if ($dimensoes[1] > $altura):
                    $error[3] = "Altura da imagem não deve ultrapassar " . $altura . " pixels";
                endif;
                // Verifica se o tamanho da imagem é maior que o tamanho permitido
                if ($img["size"] > $tamanho):
                    $error[4] = "A imagem deve ter no máximo " . $tamanho . " bytes";
                endif;

                // Se não houver nenhum erro
                if (count($error) == 0):
                    // Pega extensão da imagem
                    preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $img["name"], $ext);
                    // Gera um nome único para a imagem
                    $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
                endif;

                // Se houver mensagens de erro, exibe-as
                if (count($error) != 0) {
                    foreach ($error as $erro) {
                        $error = $erro;
                        echo $error;
                    }
                }

            endif;
        endif;

        $lastIdUser = $this->db->lastInsertId();

        try {
            $sth = $this->db->prepare(' INSERT INTO
                                            tb_img
                                                (img_id, img_nick_name, img_name, img_size, tb_user_user_id, cust_id)
                                        VALUES
                                            (:id, :nickName, :nome, :size, :userId, :custId)
                                     ');
            $sth->execute(array(
                ':id' => Null,
                ':nickName' => $nickName,
                ':nome' => $nome_imagem,
                ':size' => $img['size'],
                ':userId' => $lastIdUser,
                ':custId' => Session::get('userCust')
            ));

            // Caminho de onde ficará a imagem
            //$caminho_imagem = "/Applications/MAMP/htdocs/SmartPeeper/admin/img/user/" . $nome_imagem;
            //$caminho_imagem = "/home/nteck763/public_html/smartPeeper/admin/img/user/" . $nome_imagem;
            $shmod = chmod($dirpath . "/admin/img/user/", 0777);
            $caminho_imagem = $dirpath . "/admin/img/userCust/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($img['tmp_name'], $caminho_imagem);

            $sth->fetchAll();
            $lastIdImg = $this->db->lastInsertId();

            return $arrId = array( $lastIdProd, $lastIdImg );

        } catch (PDOException $e) {
            return $e->getMessage($error, $img['nome']);
        }
        }
    */
    public function formEdit($param) {
        $actn = 'editar';
        try{
            $sth = $this->db->prepare(' SELECT * FROM
                                            tb_user
                                        WHERE
                                            user_id = :id
                                        AND
                                            cust_id = :custId
                                    ');
            $sth->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));

            $img = $this->db->prepare(' SELECT
                                            *
                                        FROM
                                            tb_img
                                        WHERE
                                            tb_user_user_id = :id
                                    ');
            $img->execute(array(
                ':id' => $param
            ));

            return $res = array( $actn, $sth->fetchAll(), $img->fetchAll() );

        }catch(PDOException $e){
            return $e->getMessage("Erro ao editar dados da empresa: ");
        }
    }

    public function edit($param) {
        $file = $_FILES['imgs'];
        if(empty($_POST['pw'])) {
            try {
                $sth = $this->db->prepare(' UPDATE
                                                tb_user
                                            SET
                                                user_name = :user_nome,
                                                user_email = :user_email
                                            WHERE
                                                user_id = :user_id
                                            AND
                                                user_id = :userId
                                            AND
                                                cust_id = :custId
                                        ');

                $sth->execute(array(
                    ':user_id' => $param,
                    ':user_nome' => $_POST['name'],
                    ':user_email' => $_POST['email'],
                    ':userId' => Session::get('userId'),
                    ':custId' => Session::get('userCust')
                ));

                $imgUser = $this->db->prepare(' SELECT
                                                     *
                                                FROM
                                                    tb_img
                                                WHERE
                                                    tb_user_user_id = :id
                                            ');
                $imgUser->execute(array(
                    ':id' => $param
                ));

                return $ret = array( $param, $file, $sth->fetchAll(), $imgUser->fetchAll() );

            } catch (PDOException $e) {
                return $e->getMessage('Erro ao editar usuário: ' . $_POST['nome']);
            }
        }

        if(!empty($_POST['pw'])) {


            try {
                $sth = $this->db->prepare(' UPDATE
                                                tb_user
                                            SET
                                                user_name = :user_nome,
                                                user_email = :user_email,
                                                user_pw = md5(:user_pw)
                                            WHERE
                                                user_id = :user_id
                                            AND
                                                cust_id = :custId
                                        ');

                $sth->execute(array(
                    ':user_id' => $param,
                    ':user_nome' => $_POST['name'],
                    ':user_email' => $_POST['email'],
                    ':user_pw' => $_POST['pw'],
                    ':custId' => Session::get('userCust')
                ));

                $imgUser = $this->db->prepare(' SELECT
                                                *
                                                FROM
                                                    tb_img
                                                WHERE
                                                    tb_user_user_id = :id
                                            ');
                $imgUser->execute(array(
                    ':id' => $param
                ));

                return $ret = array( $param, $file, $sth->fetchAll(), $imgUser->fetchAll() );

            } catch (PDOException $e) {
                return $e->getMessage('Erro ao editar usuário: ' . $_POST['nome']);
            }

        }
        $sth->fetchAll();

    }

    public function editImg($param) {

        $id = $param[0]; // id user
        $file = $param[1]; // img $_FILES
        $arrImg = $param[3]; // Arr tb_img de user

        foreach ($arrImg as $key => $value) {
            $imgUser[0] = $value[0];
            $imgUser[2] = $value[2];
        }

        // return $dirpath:   /home/nteck763/public_html/smartpeeper
        $dirpath = realpath( dirname( getcwd() ) ) ;
        //echo "function 10 :" . $dirpath ;

        // Se a foto estiver sido selecionada
        if ($file['tmp_name'] != ''):

            //$nome = $file['name'];
            $size = $file['size'];
            //caminho para deletar img da pasta
            //$pathImg = "/home/nteck763/public_html/smartpeeper/admin/img/user/" . $imgUser[2];
            $pathImg = $dirpath . "/admin/img/user/" . $imgUser[2];

            // Verifica se o arquivo é uma imagem
            if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $file["type"])):
                return $error[1] = "Isso não é uma imagem.";
            else:


                //$percent = 0.5;
                //$iWidth = 767 * $percent;
                //$iHeight = 511 * $percent; // desired image result dimensions
                $iWidth = 200;
                $iHeight = 220;
                $iJpgQuality = 95;

                if ($_FILES) {
                    // if no errors and size less than 250kb
                    if ( ! $_FILES['imgs']['error'] ) {

                        if (is_uploaded_file($_FILES['imgs']['tmp_name'])) {

                            // new unique filename
                            $nomeSave = md5( time().rand() ) ;
                            $sTempFileName = $dirpath . '/admin/img/user/' . $nomeSave;

                            // move uploaded file into cache folder
                            move_uploaded_file( $_FILES['imgs']['tmp_name'], $sTempFileName );

                            // change file permission to 644
                            //@chmod($sTempFileName, 0644);

                            if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                                $aSize = getimagesize($sTempFileName); // try to obtain image info

                                    if (!$aSize) {
                                        @unlink($sTempFileName);

                                        return;

                                    }

                                // check for image type
                                switch($aSize[2]) {

                                    case IMAGETYPE_JPEG:
                                        $sExt = '.jpg';
                                        // create a new image from file
                                        $vImg = @imagecreatefromjpeg($sTempFileName);
                                    break;

                                    case IMAGETYPE_PNG:
                                        $sExt = '.png';
                                        // create a new image from file
                                        $vImg = @imagecreatefrompng($sTempFileName);
                                    break;
                                    default:
                                        @unlink($sTempFileName);

                                    return;

                                }

                                $nameForSave = $nomeSave . $sExt;
                                // create a new true color image
                                $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                                // copy and resize part of an image with resampling
                                imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                                // define a result image filename
                                $sResultFileName = $sTempFileName . $sExt;

                                // output image to file
                                imagejpeg( $vDstImg, $sResultFileName, $iJpgQuality );
                                @unlink($sTempFileName);
                                unlink($pathImg);

                                //return $sResultFileName;

                            }

                        }

                    }

                }

            endif; // fim else catch .img/.jpg ....

            try {
                $sth = $this->db->prepare(' UPDATE
                                                tb_img
                                            SET
                                                img_nick_name = :nickName,
                                                img_name = :nome,
                                                img_size = :size
                                            WHERE
                                                img_id = :idImg
                                        ');

                $sth->execute(array(
                    ':idImg' => $imgUser[0],
                    ':nickName' => $_POST['nomeImg'],
                    ':nome' => $nameForSave,
                    ':size' => $size
                ));

                $sth->fetchAll();

                // Caminho de onde ficará a imagem
                //$caminho_imagem = "/Applications/MAMP/htdocs/SmartPeeper/admin/img/user/" . $nome_imagem;
                //$caminho_imagem = $dirpath . "/admin/img/user/" . $nome_imagem;
                // Faz o upload da imagem para seu respectivo caminho
                //move_uploaded_file($file['tmp_name'], $caminho_imagem);

            } catch (PDOException $e) {
                return $e->getMessage('Erro ao atualizar imgaem: ' . $error);
            }

        endif; // fim if tmp_name != ''.
        // inicio if tmp_name == ''.
        if ($file['tmp_name'] == '') {

            try {
                $sth = $this->db->prepare(' UPDATE
                                                tb_img
                                            SET
                                                img_nick_name = :nickName
                                            WHERE
                                                img_id = :idImg
                                            AND
                                                tb_user_user_id = :userId
                                        ');

                $sth->execute(array(
                    ':idImg' => $imgUser[0],
                    ':nickName' => $_POST['nomeImg'],
                    ':userId' => $id
                ));

                $sth->fetchAll();

            } catch (PDOException $e) {
                return $e->getMessage('Erro ao atualizar imgaem: ' . $error);
            }
        } // fim if tmp_name  == ''
    }

    public function delete($param) {
        try {
            $sth = $this->db->prepare(' DELETE FROM tb_user WHERE user_id = :id AND cust_id = :custId ');
            $sth->execute(array(
                    ':id' => $param,
                    ':custId' => Session::get('userCust')
                ));
            //return  $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar este usuário: ' . $_POST['nome']);
        }
    }

    public function deleteImg($param) {

        try {
            $del = $this->db->prepare(' SELECT * FROM
                                            tb_img
                                        WHERE
                                            tb_user_user_id = :id
                                        AND
                                            cust_id = :custId
                                    ');
            $del->execute(array(
                ':id' => $param,
                ':custId' => Session::get('userCust')
            ));
            $arrImg = $del->fetchAll();

            foreach ($arrImg as $key => $value) :
                $img[0] = $value[0];
                $img[2] = $value[2];
            endforeach;

            $sth = $this->db->prepare(' DELETE FROM
                                            tb_img
                                        WHERE
                                            img_id = :id
                                        ');
            $sth->execute(array(
                    ':id' => $img[0]
                ));
            $sth->fetchAll();

            // return $dirpath:   /home/nteck763/public_html/smartpeeper
            $dirpath = realpath( dirname( getcwd() ) ) ;

            // Caminho de onde ficará a imagem
            $pathImg = $dirpath . "/admin/img/user/" . $img[2];
            unlink($pathImg);

        } catch (PDOException $e) {
            return $e->getMEsaage('Erro ao deletar img ste produto: ' . $_POST['nome']);
        }
    }

    /*
        public function exemplo($param) {
            try{
                $sth = $this->db->prepare();
                $sth->execute(array(
                    ' ' =>
                ));
            }catch(PDOException $e){
                return $e->getMessage("Erro ao editar dados da empresa: ");
            }
        }
    */
}
