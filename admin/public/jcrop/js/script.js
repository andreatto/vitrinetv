// convert bytes into friendly format
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];

    if (bytes == 0)
        return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

// check for selected crop region
/*
 function checkForm() {
 if (parseInt($('#w').val())) return true;
 $('.error').html('Selecione uma região para recortar a imagem').show();
 return false;
 };
 */
// update info by cropping (onChange and onSelect events handler)
function updateInfo(e) {
    $('#x1').val(e.x);
    $('#y1').val(e.y);
    $('#x2').val(e.x2);
    $('#y2').val(e.y2);
    $('#w').val(e.w);
    $('#h').val(e.h);
}
;

// clear info by cropping (onRelease event handler)
function clearInfo() {
    $('.info #w').val('');
    $('.info #h').val('');
}
;

// Create variables (in this scope) to hold the Jcrop API and image size
var jcrop_api, boundx, boundy;

function fileUploadUserCust() {
    // get selected file
    var oFile = $('#imgs')[0].files[0];
    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;

    if (!rFilter.test(oFile.type)) {
        $('.error').html('Selecione um arquivo de imagem válido (jpg ou png)').show();
        return;
    }

    // check for file size
    /*	
     if (oFile.size > 250 * 1024) {
     $('.error').html('Voce selecionou uma imagem muito grande, escolha uma menor!').show();
     return;
     }
     */

    // preview element
    var oImage = document.getElementById('preview');
    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function (e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

            var tamanhoImg = oImage.naturalWidth;
            if (tamanhoImg <= 800) {
                //alert( "Pixel: " + tamanhoImg +  "\n Tamanho menor que 600 px" );
                var newBoxWidth = 350;
            } else {
                //alert( "Pixel: " + tamanhoImg + "\n Tamanho maior que 600 px" );
                var newBoxWidth = 750;
            }

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#preview').width(oImage.naturalWidth);
                $('#preview').height(oImage.naturalHeight);
            }

            setTimeout(function () {
                // initialize Jcrop
                $('#preview').Jcrop({
                    minSize: [32, 32], // min crop size
                    //aspectRatio : 1, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    boxWidth: newBoxWidth,
                    //boxHeight: 511,
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    setSelect: [0, 0, 200, 220],
                    //aspectRatio: 16 / 9,
                    onRelease: clearInfo
                }, function () {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                });
            }, 2000);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}


function fileUploadProduct() {
    // get selected file
    var oFile = $('#imgs')[0].files[0];
    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;

    if (!rFilter.test(oFile.type)) {
        $('.error').html('Selecione um arquivo de imagem válido (jpg ou png)').show();
        return;
    }

    //  check for file size
    /*
     if (oFile.size > 250 * 1024) {
     $('.error').html('Voce selecionou uma imagem muito grande, escolha uma menor!').show();
     return;
     }
     
     */


    // preview element
    var oImage = document.getElementById('preview');
    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function (e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

            var tamanhoImg = oImage.naturalWidth;
            if (tamanhoImg >= 1024) {
                //alert( "Pixel: " + tamanhoImg +  "\n Tamanho menor que 600 px" );
                var newBoxWidth = 920;
            } else {
                //alert( "Pixel: " + tamanhoImg + "\n Tamanho maior que 600 px" );
                var newBoxWidth = 920;
            }

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#preview').width(oImage.naturalWidth);
                $('#preview').height(oImage.naturalHeight);
            }

            setTimeout(function () {
                // initialize Jcrop
                $('#preview').Jcrop({
                    minSize: [32, 32], // min crop size
                    //aspectRatio : 1, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    boxWidth: newBoxWidth,
                    //boxHeight: 511,
                    //trueSize: [450,300],
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    setSelect: [0, 0, 800, 600],
                    //aspectRatio: 16 / 9,
                    onRelease: clearInfo
                }, function () {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                });
            }, 3000);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}


function fileUploadEventos() {
    // get selected file
    var oFile = $('#imgs')[0].files[0];
    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;

    if (!rFilter.test(oFile.type)) {
        $('.error').html('Selecione um arquivo de imagem válido (jpg ou png)').show();
        return;
    }

    //  check for file size
    /*
     if (oFile.size > 250 * 1024) {
     $('.error').html('Voce selecionou uma imagem muito grande, escolha uma menor!').show();
     return;
     }
     
     */


    // preview element
    var oImage = document.getElementById('preview');
    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function (e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

            var tamanhoImg = oImage.naturalWidth;
            if (tamanhoImg >= 1024) {
                //alert( "Pixel: " + tamanhoImg +  "\n Tamanho menor que 600 px" );
                var newBoxWidth = 920;
            } else {
                //alert( "Pixel: " + tamanhoImg + "\n Tamanho maior que 600 px" );
                var newBoxWidth = 920;
            }

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#preview').width(oImage.naturalWidth);
                $('#preview').height(oImage.naturalHeight);
            }

            setTimeout(function () {
                // initialize Jcrop
                $('#preview').Jcrop({
                    minSize: [32, 32], // min crop size
                    //aspectRatio : 1, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    boxWidth: newBoxWidth,
                    //boxHeight: 511,
                    //trueSize: [450,300],
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    setSelect: [0, 0, 800, 600],
                    //aspectRatio: 16 / 9,
                    onRelease: clearInfo
                }, function () {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                });
            }, 3000);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}
