// Cartucho
// Inserir produtos dentro do text area para os cartuchos em edicao
// Ao clicar em inserir na lista de produtos, o mesmo devera ser adicionado dentro do text area do cartucho.
//       onde exirtira um botao para depois salvar o cartucho com os produtos.

function pushCartucho(id, prodNome, valor, categoria) {
        var n = document.getElementById("cartucho").length;
        if(document.getElementById("cartucho").length < 12){
            var add = ' '+prodNome+' | R$ '+valor+' ';
            var x = document.getElementById("cartucho");
            var option = document.createElement("option");
            option.setAttribute('value', id);
            option.setAttribute('selected', 'selected');
            option.text = add;
            x.add(option);
            x.setAttribute('selected', 'selected');
        }

        if(document.getElementById("cartucho").length == 12){
            if(document.getElementById('spanMsg').style.visibility == "hidden"){
                document.getElementById('spanMsg').style.visibility = "visible";
            }else{
                var msg = document.getElementById('spanMsg');
                msg.innerHTML = 'Cartucho pronto ! ';
            }

            var n = document.getElementById("cartucho").length;
            for(var i = 0; i < n; i++) {
                document.getElementById("cartucho").options[i].selected=true;
            }

            document.getElementById("btnSalvar").disabled = false;

        }
}
function remover() {
    var x = document.getElementById("cartucho");
    x.remove(x.selectedIndex);
    document.getElementById("btnSalvar").disabled = true;
    document.getElementById('spanMsg').style.visibility = "hidden";
}

function pushCartuchoDesc(id, prodNome, valor, categoria) {
        var n = document.getElementById("cartucho").length;
        if(document.getElementById("cartucho").length < 3){
            var add = ' '+prodNome+' | R$ '+valor+' ';
            var x = document.getElementById("cartucho");
            var option = document.createElement("option");
            option.setAttribute('value', id);
            option.setAttribute('selected', 'selected');
            option.text = add;
            x.add(option);
            x.setAttribute('selected', 'selected');
        }

        if(document.getElementById("cartucho").length == 3){
            if(document.getElementById('spanMsg').style.visibility == "hidden"){
                document.getElementById('spanMsg').style.visibility = "visible";
            }else{
                var msg = document.getElementById('spanMsg');
                msg.innerHTML = 'Cartucho pronto ! ';
            }

            var n = document.getElementById("cartucho").length;
            for(var i = 0; i < n; i++) {
                document.getElementById("cartucho").options[i].selected=true;
            }

            document.getElementById("btnSalvar").disabled = false;

        }
}
function removerDesc() {
    var x = document.getElementById("cartucho");
    x.remove(x.selectedIndex);
    document.getElementById("btnSalvar").disabled = true;
    document.getElementById('spanMsg').style.visibility = "hidden";
}

function pushCartuchoAlbum(id, prodNome, valor, categoria) {
        var n = document.getElementById("cartucho").length;
        if(document.getElementById("cartucho").length < 8){
            var add = ' '+prodNome+' | R$ '+valor+' ';
            var x = document.getElementById("cartucho");
            var option = document.createElement("option");
            option.setAttribute('value', id);
            option.setAttribute('selected', 'selected');
            option.text = add;
            x.add(option);
            x.setAttribute('selected', 'selected');
        }

        if(document.getElementById("cartucho").length == 8){
            if(document.getElementById('spanMsg').style.visibility == "hidden"){
                document.getElementById('spanMsg').style.visibility = "visible";
            }else{
                var msg = document.getElementById('spanMsg');
                msg.innerHTML = 'Cartucho pronto ! ';
            }

            var n = document.getElementById("cartucho").length;
            for(var i = 0; i < n; i++) {
                document.getElementById("cartucho").options[i].selected=true;
            }

            document.getElementById("btnSalvar").disabled = false;

        }
}
function removerAlbum() {
    var x = document.getElementById("cartucho");
    x.remove(x.selectedIndex);
    document.getElementById("btnSalvar").disabled = true;
    document.getElementById('spanMsg').style.visibility = "hidden";
}


function pushCartuchoEvents( id, eventName, valor, date ) {
        var n = document.getElementById("cartucho").length;
        if(document.getElementById("cartucho").length < 3){
            var add = ' '+eventName+' | Data: ' + date;
            var x = document.getElementById("cartucho");
            var option = document.createElement("option");
            option.setAttribute('value', id);
            option.setAttribute('selected', 'selected');
            option.text = add;
            x.add(option);
            x.setAttribute('selected', 'selected');
        }

        if(document.getElementById("cartucho").length == 3){
            if(document.getElementById('spanMsg').style.visibility == "hidden"){
                document.getElementById('spanMsg').style.visibility = "visible";
            }else{
                var msg = document.getElementById('spanMsg');
                msg.innerHTML = 'Cartucho pronto ! ';
            }

            var n = document.getElementById("cartucho").length;
            for(var i = 0; i < n; i++) {
                document.getElementById("cartucho").options[i].selected=true;
            }

            document.getElementById("btnSalvar").disabled = false;

        }
}
function removeEvents() {
    var x = document.getElementById("cartucho");
    x.remove(x.selectedIndex);
    document.getElementById("btnSalvar").disabled = true;
    document.getElementById('spanMsg').style.visibility = "hidden";
}




/* 
FORM Cadastrar produtos
function txtAreaProd() {
    document.getElementById("descricao").maxLength = "68";
}
*/




