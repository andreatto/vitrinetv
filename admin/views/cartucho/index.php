<?php
foreach ($this->dataCart as $cart) {}
//print_r($this->dataCart);
?>
<div class="clearfix"></div>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">

        <!-- Cartucho carregando -->
        <div class="x_panel">
            <div class="x_title">
                <h2>Cartucho <?=$cart[cart_nome]?> <small>Selecione 12 entre os diferentes produtos quais deseja promver !</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <!--
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        -->
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                    
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <form  action="<?=URL?>cartucho/inserir/" method="post" enctype="multipart/form-data" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label>Produtos inseridos </label><br/>
                        <label>
                             <button type="submit" class="btn btn-primary" >Salvar</button>
                        </label>
                        
                        <!-- 
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea name="cartucho" class="form-control" rows="12" placeholder='rows="12"'></textarea>
                            </div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <textarea name="cartucho" class="form-control" rows="12" placeholder='rows="12"'></textarea>
                            </div>
                        -->
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <select multiple id="cartucho" name="cartucho[]" class="form-control" size="12">
                            <!--
                                <option value="volvo">Volvo</option>
                                <option value="saab">Saab</option>
                                <option value="opel">Opel</option>
                                <option value="audi">Audi</option>
                            -->
                            </select>
                        </div>

                    </div>
                </form/>
            </div>
        </div>

        <!-- Produtos -->
        <div class="x_panel">
            <div class="x_title">
                <h2>Cartucho <?=$cart[cart_nome]?> <small>Selecione 12 entre os diferentes produtos quais deseja promver !</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <!-- 
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        -->
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="tableflat">
                            <!--
                            <th>
                                <input type="checkbox" class="tableflat">
                            </th>
                            <th>Codigo </th>
                            <th>Order </th>
                            -->
                            <th >Nome </th>
                            <th>Valor R$</th>
                            <th>Categoria </th>
                            <th>Medida </th>
                            <th>Campanha </th>
                            <th class=" no-link last">
                                <span class="nobr">Ações</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>   
                        <?php foreach($this->dataUrl as $arrProd): ?>
                            
                            <tr class="tableflat">
                                <!--
                                    <td class="a-center ">
                                        <input type="checkbox" class="tableflat">
                                    </td>
                                    
                                    <td class=" "><?=$arrProd[prod_id] ?></td> 
                                    <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                                -->
                                
                                <td class=" " id="prodNome"><?=$arrProd[prod_nome] ?></td>     
                                <td class="a-right">R$ <?=number_format($arrProd['prod_valor'],2) ?></td>
                                <td class=" "><?=$arrProd[cat_nome] ?></td>
                                <td class=" "><?=$arrProd[med_nome] ?></td>
                                <td class=" "><?=$arrProd[camp_nome] ?></td>
                                <!-- 
                                    <td class=" last"><a href="#">Editar</a>
                                    <td class=" last"><a href="#">Excluir</a> 
                                -->
                                
                                <td class="">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-round btn-primary" onClick="javascript: pushCartucho('<?=$arrProd[prod_id] ?>', '<?=$arrProd[prod_nome] ?>', '<?=number_format($arrProd['prod_valor'],2) ?>', '<?=$arrProd[cat_nome] ?>');">Inserir</button>
                                        <!--
                                            <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Ações <span class="caret"></span>
                                            </button>
                                        
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Inserir</a>
                                                </li>
                                                <li><a href="<?=URL?>product/delete/<?=$arrProd[prod_id]?>">Excluir</a>
                                                </li>
                                               
                                                <li class="divider"></li>
                                                <li><a href="#">Retirar</a>
                                                </li>
                                            </ul>
                                        -->
                                    </div>
                                </td>
                                
                            </tr>
                            
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <br />
    <br />
    <br />

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>