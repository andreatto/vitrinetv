<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Smart Peeper | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo URLLINK; ?>css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo URLLINK; ?>fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo URLLINK; ?>css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo URLLINK; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo URLLINK; ?>css/icheck/flat/green.css" rel="stylesheet">


        <script src="<?php echo URLLINK; ?>js/jquery.min.js"></script>

        <!--[if lt IE 9]>
              <script src="../assets/js/ie8-responsive-file-warning.js"></script>
              <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->

    </head>

    <body style="background:#F7F7F7;">

        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <div id="wrapper">
                <div id="login" class="animate form">
                    <section class="login_content">

                        <form action="login/run" method="post" enctype="multipart/form-data" role="form">
                            <h1>Login</h1>
                            <div>
                                <input name="login" type="text" class="form-control" placeholder="Login" required="" />
                            </div>
                            <div>
                                <input name="password" type="password" class="form-control" placeholder="Senha" required="" />
                            </div>
                            <div>
                                <!-- <a class="btn btn-default submit" href="login/run" >Entrar</a> -->
                                <input type="submit" class="btn btn-default submit" value="Entrar">
                                <!-- <a class="reset_pass" href="#">Esqueceu sua senha ?</a> -->
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <!-- <p class="change_link">Novo no sistema ?
                                     <a href="#toregister" class="to_register"> Criar conta </a>                                     <a href="#toregister" class="to_register"> Recuperar senha </a>
                                </p> -->
                                <p class="change_link">Esqueceu sua senha ?
                                    <!-- <a href="#toregister" class="to_register"> Criar conta </a> -->
                                    <a href="#toregister" class="to_register"> Recuperar </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-eye" style="font-size: 26px;"></i> Smart Peeper !</h1>

                                    <p>©2016 All Rights Reserved. Smart Peeper! is a Bootstrap 3 template. Privacy and Terms</p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                        
                        
                    </section>
                    <!-- content -->
                </div>
                <div id="register" class="animate form">
                    <section class="login_content">
                        <form action="login/run" method="post" enctype="multipart/form-data" role="form">
                            <h1>Recuperar Senha</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="Nome" required="" />
                            </div>
                            <div>
                                <input type="email" class="form-control" placeholder="E-mail" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Senha" required="" />
                            </div>
                            <div>
                                <!-- <a class="btn btn-default submit" href="login/recuperar">Recuperar</a> -->
                                <input type="submit" value="Recuperar" class="btn btn-default submit">
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Já sou cadastrado ?
                                    <a href="#tologin" class="to_register"> Entrar </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-eye" style="font-size: 26px;"></i> Smart Peeper !</h1>

                                    <p>©2016 All Rights Reserved. Smart Peeper! is a Bootstrap 3 template. Privacy and Terms</p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>
            </div>
        </div>

    </body>

</html>
