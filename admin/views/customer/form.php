<?php
if (isset($this->dataUrl)):
    if (!empty($this->dataUrl)):

        $actn = $this->dataUrl[0];
        $cust = $this->dataUrl[1];

        foreach ($cust as $customer) {}

    else:
        echo $dataUrl = Session::get('dataUrl');
    endif;
endif;
?>
<div class="">
    <!--
        <div class="page-title">
            <div class="title_left">
                <h3></h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">

                        </span>
                    </div>
                </div>
            </div>
        </div>
    -->
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <!--
                    <div class="x_title">
                        <h2>Form Wizards <small>Sessions</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                -->

                <div class="x_content">

                    <!--
                        <h2>Example </2>: Vertical Style</h2>
                    Tabs -->
                    <form action="<?=URL?>customer/editar/<?=$customer[0]?>" method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-label-left">

                    <input type="hidden" id="x1" name="x1" />
                    <input type="hidden" id="y1" name="y1" />
                    <input type="hidden" id="x2" name="x2" />
                    <input type="hidden" id="y2" name="y2" />


                        <div id="wizard_verticle" class="form_wizard wizard_verticle">
                            <ul class="list-unstyled wizard_steps">
                                <li>
                                    <a href="#step-11">
                                        <span class="step_no">1</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-22">
                                        <span class="step_no">2</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-33">
                                        <span class="step_no">3</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-44">
                                        <span class="step_no">4</span>
                                    </a>
                                </li>

                            </ul>


                            <div id="step-11">
                                <h2 class="StepTitle">Empresa</h2>


                                <span class="section">Informações importantes</span>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3" for="first-name">Razão Social <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" id="first-name2" name="rs" required="required" class="form-control col-md-7 col-xs-12" value="<?= $customer[1] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3" for="last-name">Fantasia <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                          <input type="text" id="last-name2" name="fantasia" required="required" class="form-control col-md-7 col-xs-12" value="<?= $customer[2] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">CNPJ <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="cnpj" required="required" value="<?= $customer[3] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Inscrição Est. <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="inscEst" required="required" value="<?= $customer[4] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Contato <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="contato" required="required" value="<?= $customer[5] ?>">
                                    </div>
                                </div>



                            </div>

                            <div id="step-22">
                                <h2 class="StepTitle">Empresa</h2>


                                <span class="section">Endereço</span>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3" for="first-name">Rua <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" id="street" name="street" required="required" class="form-control col-md-7 col-xs-12" value="<?= $customer[7] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3" for="last-name">Número <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" id="number" name="number" required="required" class="form-control col-md-7 col-xs-12" value="<?= $customer[8] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Bairro <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="bairro" required="required" value="<?= $customer[9] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">CEP <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="cep" class="form-control col-md-7 col-xs-12" type="text" name="cep" value="<?= $customer[10] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Cidade <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="cidade" required="required" value="<?= $customer[11] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Estado <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="estado" required="required" value="<?= $customer[12] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">País <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="pais" required="required" value="<?= $customer[13] ?>">
                                    </div>
                                </div>
                            </div>

                            <div id="step-33">
                                <h2 class="StepTitle">Empresa</h2>


                                <span class="section">Contato</span>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3" for="first-name">Telefone: <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" id="first-name2" name="telefone" required="required" class="form-control col-md-7 col-xs-12" value="<?= $customer[16] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3" for="last-name">Celular <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" id="last-name2" name="celular" required="required" class="form-control col-md-7 col-xs-12" value="<?= $customer[17] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">E-mail <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="email" required="required" value="<?= $customer[18] ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Site </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input id="middle-name2" class="form-control col-md-7 col-xs-12" type="text" name="site" required="required" value="<?= $customer[19] ?>">
                                    </div>
                                </div>

                            </div>

                            <div id="step-44">
                                <h2 class="StepTitle">Considerações</h2>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">Imagem<span class="required"> *</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="nomeImg" id="nomeImg" class="form-control" data-validate-length-range="6" data-validate-words="2" value="<?=$customer[22]?>" placeholder="Nome da imagem" required="required">
                                        <input type="file" name="imgs" id="imgs" onchange="fileUploadUserCust()" >

                                        <br />
                                        <p>Imagem: Mantenha o tamanho indicado na selecao</p>

                                    </div>
                                </div>



                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3">
                                        <br />
                                        <div class="error"></div>
                                            <br />
                                            <div class="step2">
                                                <div class="info">
                                                    <label>BT/KB</label>
                                                        <input type="text" id="filesize" name="filesize" size="9"/><br />
                                                    <label>Tipo</label>
                                                        <input type="text" id="filetype" name="filetype" size="9"/><br />
                                                    <label>Dim</label>
                                                        <input type="text" id="filedim" name="filedim" size="9"/><br />
                                                    <label>Larg</label>
                                                        <input type="text" id="w" name="w" size="9"/><br />
                                                    <label>Alt</label>
                                                        <input type="text" id="h" name="h" size="9"/><br />
                                                </div>

                                            </div>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="step2">
                                            <img id="preview"  />
                                        </div>

                                    </div>
                                </div>


                            </div>

                        </div>


                    </form>
                    <!-- End SmartWizard Content -->


                </div>
            </div>
        </div>

    </div>
</div>


<!-- JCrop IMG -->
<!-- add styles -->
<!-- <link href="<?=URLLINK?>jcrop/css/main.css" rel="stylesheet" type="text/css" /> -->
<link href="<?=URLLINK?>jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
<!-- add scripts -->
<!-- <script src="<?=URLLINK?>jcrop/js/jquery.min.js"></script> -->
<script src="<?=URLLINK?>jcrop/js/jquery.Jcrop.min.js"></script>
<script src="<?=URLLINK?>jcrop/js/script.js"></script>
