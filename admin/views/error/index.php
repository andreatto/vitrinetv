<!-- page content -->
      <div class="col-md-12">
        <div class="col-middle">
          <div class="text-center text-center">
            <h1 class="error-number">404</h1>
            <h2>Erro:</h2>
            <p><?= $this->msg; ?> <a href="#">Comunique este erro ?</a>
            </p>
            <div class="mid_center">
              <h3>Pesquisar</h3>
              <form>
                <div class="col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="pesquisar ...">
                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->
