<?php

// Pegando e atribuindo valores a action
$actn = $this->dataUrl[0];
$dataUrl = $this->dataUrl[1];
$arrImgn = $this->dataimg[1];

if(isset($this->dataUrl)):
    foreach($dataUrl as $key => $value):
        $arr[0] = $value[0];
        $arr[1] = $value[1];
        $arr[2] = $value[2];
        $arr[3] = $value[3];
        $arr[4] = $value[4];
        $arr[5] = $value[5];
        $arr[6] = $value[6];
        $arr[7] = $value[7];
        $arr[8] = $value[8];
    endforeach; 

    foreach ($arrImgn as $key => $value):
        $arrImg[0] = $value[0];
        $arrImg[1] = $value[1];
        $arrImg[2] = $value[2];
        $arrImg[3] = $value[3];
        $arrImg[4] = $value[4];
        $arrImg[5] = $value[5];
        $arrImg[6] = $value[6];
        $arrImg[7] = $value[7];
        $arrImg[8] = $value[8];
    endforeach;

endif; 

if($actn == '') {
    $actn = 'inserir';
}
?>


    <div class="">
      
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                
                <div class="x_panel">
            
                    <div class="x_title">
                        <h2>Eventos <small>SESSION [<?= ucfirst($actn) ?>]</small></h2> 


                        <ul class="nav navbar-right panel_toolbox">
                            <?php
                            if($actn != 'inserir'):
                                echo '<a href="../form"><button type="button" class="btn btn-primary">Novo</button></a>';
                                echo '<a href="../delete/'.$arr[0].'"><button type="button" class="btn btn-primary">Excluir</button></a>';
                            endif;
                            ?>
                            
                            <a href="
                            <?php
                                if($actn != 'inserir'){
                                    echo'../index';     
                                }else{
                                    echo'index';
                                }
                            ?>"><button type="button" class="btn btn-primary">Listar</button></a>
                        </ul>

                        <ul class="nav navbar-right panel_toolbox">
                            <li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <!--
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a></li>
                                        <li><a href="#">Settings 2</a></li>
                                    </ul>

                                </li>
                            -->
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div> <!-- fim x_title -->
           
                    <div class="x_content">
                    <br />
                    <form  id="formEventos"
                        action=" 
                            <?php
                                if($actn == 'inserir'):
                                    echo $actn;
                                elseif($actn == 'editar'):
                                    echo '../' . $actn . '/' . $arr[0];
                                endif;
                            ?>
                        " 
                        method="post" class="form-horizontal form-label-left" enctype="multipart/form-data" role="form">

                        <!-- hidden crop params -->
                        <input type="hidden" id="x1" name="x1" />
                        <input type="hidden" id="y1" name="y1" />
                        <input type="hidden" id="x2" name="x2" />
                        <input type="hidden" id="y2" name="y2" />

                        <!-- Smart Wizard -->
                        <p>Cadastro de eventos </p>
                        
                        <div id="wizard" class="form_wizard wizard_horizontal">
                        
                            <ul class="wizard_steps">
                                <li>
                                    <a href="#step-1">
                                        <span class="step_no">1</span>
                                        <span class="step_descr"> Passo 1<br />
                                            <small>Dados </small>
                                        </span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="#step-2">
                                        <span class="step_no">2</span>
                                        <span class="step_descr">
                                            Passo 2<br />
                                            <small>Descrição </small>
                                        </span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="#step-3">
                                        <span class="step_no">3</span>
                                        <span class="step_descr">
                                            Passo 3<br />
                                            <small>Imagem </small>
                                        </span>
                                    </a>
                                </li>
                                
                                
                            </ul>

                            <div id="step-1">
                        
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="name" id="name" class="form-control col-md-7 col-xs-12" value="<?=$arr[1]?>" placeholder="Nome" required="required">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Valor 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="valor" id="valor" class="form-control col-md-7 col-xs-12" value="<?=number_format($arr[2],2, ',', '.')?>" placeholder="Valor $" required="required">
                                    </div>
                                </div>
                            
                            <!--   
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">E-Mail 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" name="email" id="email" class="form-control col-md-7 col-xs-12" value="" placeholder="E-Mail" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Senha 
                                        <span  class="required">*</span> 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="pw" id="pw" class="form-control col-md-7 col-xs-12" placeholder="Senha" required="required" >
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="gender" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="gender" value="male"> &nbsp; Male &nbsp;
                                            </label>
                                            <label class="btn btn-primary active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="gender" value="female" checked=""> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                    </div>
                                </div>
                            -->

                            </div> <!-- fim step 1 -->
                            
                            <div id="step-2">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Data <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="date" name="date" class="date-picker form-control col-md-7 col-xs-12" value="<?=$arr[3]?>" required="required" type="text">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Dia/Semana <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="datesemana" name="datesemana" class="date-picker form-control col-md-7 col-xs-12" value="<?=$arr[4]?>" required="required" type="text">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Horário <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="horario" name="horario" class="date-picker form-control col-md-7 col-xs-12" value="<?=$arr[5]?>" required="required" type="text">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descrição 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="6" maxlength="215" name="descricao" id="descricao" class="form-control col-md-7 col-xs-12" value="<?=$arr[6]?>" placeholder="Descricao Max 190 caracteres"><?=$arr[6]?></textarea>
                                    </div>
                                </div>
                                
                            </div> <!-- fim step 2 -->
                            
                            
                            <div id="step-3">

                                <div class="form-group">
                                    
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Imagem 
                                        <span class="required">*</span> 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <input type="text" name="nomeImg" id="nomeImg" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" value="<?=$arrImg[1]?>" placeholder="Tamanho edição 800x600 pixel" required="required">
                                        <input type="file" name="imgs" id="imgs" onchange="fileUploadEventos()" >
                                        <br />
                                        <p>Imagem: Mantenha o tamanho indicado na seleção</p>
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> 
                                         <div class="error"></div>
                                         <br />
                                    </label>
                                        <br />
                                    </div>
                                    
                                </div>


                                <div class="form-group">

                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Tamanho</label> <input type="text" id="filesize" name="filesize" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Tipo</label> <input type="text" id="filetype" name="filetype" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Dimensão</label> <input type="text" id="filedim" name="filedim" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Largura</label> <input type="text" id="w" name="w" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Altura</label> <input type="text" id="h" name="h" />
                                    </label>
                                    
                                </div>
                            </div> <!-- fim step 3 -->

                            </div> <!-- End SmartWizard Content -->
                                <div class="col-md-12 col-sm-12 col-xs-18">
                                    <div class="step2"> 
                                        <img id="preview"  />                             
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End SmartWizard Content -->


                    </form>
                    </div> <!-- FIM x_content -->

                </div> <!-- FIM x_panel -->

            </div>

        </div> <!-- FIM row -->

    </div> <!-- FIM class vazia -->

<!-- JCrop IMG -->
<!-- add styles -->
<!-- <link href="<?=URLLINK?>jcrop/css/main.css" rel="stylesheet" type="text/css" /> -->
<link href="<?=URLLINK?>jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
<!-- add scripts -->
<!-- <script src="<?=URLLINK?>jcrop/js/jquery.min.js"></script> -->
<script src="<?=URLLINK?>jcrop/js/jquery.Jcrop.min.js"></script>
<script src="<?=URLLINK?>jcrop/js/script.js"></script>

<script src="<?=URLLINK?>maskmoney/src/jquery.maskMoney.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
          $("#valor").maskMoney( { showSymbol:true, symbol:"R$", decimal:",", thousands:"." } );
    });
</script>

<script src="<?=URLLINK?>mask/mask.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($){
$("#date").mask("99/99/9999");
//$("#campoTelefone").mask("(999) 999-9999");
$("#horario").mask("99:99");
});
</script>