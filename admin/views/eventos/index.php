<?php
$actn = $this->dataUrl[0];
$dataUrl = $this->dataUrl[1];

?>
<!--
        <div class="page-title"> 
            
            <div class="title_left">
                <h3>
                    Produtos
                    <small>
                        Some examples to get you started
                    </small>
                </h3>
            </div>
           
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
            
        </div> -->
            
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    
                    <div class="x_title">
                        <h2>Eventos <!-- <small>[Sessions]</small> --> </h2>
  
                        <ul class="nav navbar-right panel_toolbox">
                             <div class="x_content">
                                <a href="<?=URL?>eventos/form"><button type="button" class="btn btn-primary">Novo</button></a>
                             </div>
                                <!--
                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class<!--="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-close"></i></a>
                            </li>
                            -->
                        </ul> 
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="x_content">
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="tableflat">
                                <!--
                                    <th>
                                        <input type="checkbox">
                                    </th>
                                    
                                     <th>Order </th> 
                                -->
                                    <th>Codigo </th>
                                    <th>Nome </th>
                                    <th>Valor R$</th>
                                    <th>Data</th>
                                    <th>Horário</th>
                                    <th>Descrição</th>
                                    <th class=" no-link last">
                                        <span class="nobr">Ações</span>
                                    </th>
                                </tr>
                            </thead>

                            
                            <tbody>   
                                <?php foreach($dataUrl as $arrEventos): ?>
                                    
                                    <tr class="tableflat">
                                    <!--
                                        <td class="a-center ">
                                            <input type="checkbox" >
                                        </td>
                                        <td class=" "><?=$arrEventos[0] ?></td> 
                                        <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td> 
                                    -->
                                        <td class=" "><?=$arrEventos[0]?></td> 
                                        <td class=" "><?=substr( $arrEventos[1], 0, 25 )  ?></td> 
                                        <td class=" "><?=number_format( $arrEventos[2],2, ',', '.') ?></td>
                                        <td class=" " id="date"><?=$arrEventos[3] ?></td>  <!-- data -->
                                        <td class=" " id="horario"><?=$arrEventos[5] ?></td>  <!-- horaario -->
                                        <td class=" "><?=substr( $arrEventos[6], 0, 40 ) ?></td> 
                                        <!-- <td class="a-right">R$ </td> -->
                                        <!-- <td class=" last"><a href="#">Editar</a>
                                        <td class=" last"><a href="#">Excluir</a> -->
                                        
                                        <td class="last">
                                            <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Ações <span class="caret"></span>
                                                </button>
                                                    <ul role="menu" class="dropdown-menu">
                                                      <li><a href="<?=URL?>eventos/formEdit/<?=$arrEventos[0]?>">Editar</a>
                                                      </li>
                                                      
                                                      <li><a href="<?=URL?>eventos/delete/<?=$arrEventos[0]?>">Excluir</a>
                                                      </li>
                                                      <li class="divider"></li>
                                                      <li><a href="#">Visualizar</a>
                                                      </li>
                                                    </ul>
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    
                                <?php endforeach; ?>
                           </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
        
<script src="<?=URLLINK?>mask/mask.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($){
$("#date").mask("99/99/9999");
//$("#campoTelefone").mask("(999) 999-9999");
$("#horario").mask("99:99")
});
</script>