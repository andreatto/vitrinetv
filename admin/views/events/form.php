<?php
if(empty($this->actn)):
    $actn = 'carregar';
    $mthds = 'inserir';
    $arrMod = $this->dataModulo[0];
    $dataEvent = $this->dataEvento[1];
else:
    $actn = $this->actn;
    $mthds = 'editar';
    $arrMod = $this->dataModulo[0];
    $dataEvent = $this->dataEvento[1]; // todos os eventos cadastados para o cartucho
    $dataUrl = $this->dataUrl[1]; // todos eventos
endif;

//foreach ($this->dataCart as $cart) {}
//print_r($this->dataCart);
?>
<div class="clearfix"></div>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Evento <small>Complete o cartucho Eventos com 3 eventos !!</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="x_content">
                            <a href="<?=URL?>event/index/<?=$arrMod?>"><button type="button" class="btn btn-primary">Visualizar</button></a>
                        </div>
                        <!--
                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-close"></i></a>
                            </li>
                        -->
                    </ul> 
                <div class="clearfix"></div>
            </div>
        <!-- Inseridos na Descrição -->
        <div class="x_content">
            <form id="formCart" action="<?=URL?>event/<?=$mthds?>/<?=$arrMod?>" method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-label-left">
                <div class="form-group">
                    <label id="msg" class="control-label col-md-3 col-sm-3 col-xs-12">Produtos: <span id="spanMsg"></span></label> <br/>
                    
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <button type="button" class="btn btn-round btn-danger" onClick="javascript: removeEvents();">
                            Retirar 
                        </button>    

                            <?php 
                                if(!empty($this->actn)):
                                    echo '<select multiple id="ids" type="hidden" name="ids[]" size="3" >';
                                    foreach ($dataEvent as $i => $event): 
                            ?>
                                        <option value="<?=$event[view_id]?>"  selected="selected">  
                                            <?=$i+1?>
                                        </option>
	                        <?php 
                                    endforeach; 
                                    echo ' </select>';
                                endif;
                             ?>

                        <select multiple id="cartucho" name="cartucho[]" size="3">
                            <?php 
                                if(!empty($this->actn)):
                                    foreach ($dataEvent as $event):
                            ?>
                                <option value="<?=$event[0]?>"  selected="selected"> 
                                    <?=$event[event_nome]?> | <?=$event[event_data]?>  
                                </option>
	                        <?php
                                    endforeach; 
                                endif;
                             ?>
                        </select>

                        <button id="btnSalvar" type="submit" class="btn btn-round btn-primary" disabled>Salvar</button>
                        

                    </div>
                </div>
            </form>
        </div>
    </div>

        <!-- Produtos -->
        <div class="x_panel">
            <div class="x_title">
                <h2>Produtos <small>Selecione 3 entre os diferentes eventos que deseja promover !</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <!-- 
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        -->
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="tableflat">
                            <!--
                                <th>
                                    <input type="checkbox" class="tableflat">
                                </th>
                                <th>Codigo </th>
                                <th>Order </th>
                            -->
                            <th >Nome </th>
                            <th>Valor R$</th>
                            <th>Data</th>
                            <th>Horário </th>
                            <th>Descrição </th>
                            <th class=" no-link last">
                                <span class="nobr">Ações</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>   
                        <?php foreach($this->dataUrl[1] as $arrEvent): ?>
                            
                            <tr class="tableflat">
                                <!--
                                    <td class="a-center ">
                                        <input type="checkbox" class="tableflat">
                                    </td>
                                    
                                    <td class=" "><?=$arrEvent[event_id] ?></td> 
                                    <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                                -->
                                
                                <td class=" " id="prodNome"><?=$arrEvent[event_nome] ?></td>     
                                <td class="a-right" id="valor">R$ <?=number_format($arrEvent['event_valor'],2, ',', '.') ?></td>
                                <td class=" "><?=$arrEvent[event_data] ?></td>
                                <td class=" "><?=$arrEvent[event_horario] ?></td>
                                <td class=" "><?= substr($arrEvent[event_desc], 0, 20 ); ?> .....</td>
                                
                                <!-- 
                                    <td class=" last"><a href="#">Editar</a>
                                    <td class=" last"><a href="#">Excluir</a> 
                                -->

                                <td class="">
                                    <div class="btn-group">
                                        <button id="btnInserir" type="button" class="btn btn-round btn-primary" onClick="javascript: pushCartuchoEvents('<?=$arrEvent[event_id] ?>', '<?=$arrEvent[event_nome] ?>', '<?=number_format($arrEvent['event_valor'],2) ?>', '<?=$arrEvent[event_data] ?>');">Inserir</button>
                                        <!--
                                            <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Ações <span class="caret"></span>
                                            </button>
                                        
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Inserir</a>
                                                </li>
                                                <li><a href="<?=URL?>product/delete/<?=$arrEvent[event_id]?>">Excluir</a>
                                                </li>
                                            
                                                <li class="divider"></li>
                                                <li><a href="#">Retirar</a>
                                                </li>
                                            </ul>
                                        -->
                                    </div>
                                </td>

                            </tr>
                            
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <br />
    <br />
    <br />

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?=URLLINK?>maskmoney/src/jquery.maskMoney.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
          $("#valor").maskMoney( { showSymbol:true, symbol:"R$", decimal:",", thousands:"." } );
    });
</script>

<script src="<?=URLLINK?>mask/mask.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($){
$("#date").mask("99/99/9999");
//$("#campoTelefone").mask("(999) 999-9999");
$("#horario").mask("99:99");
});
</script>