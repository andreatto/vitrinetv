<?php
$arrModulo = $this->dataEvento[0];
if(empty($this->dataEvento[1])):
    $actn = 'Carregar';
    $mthds = 'form';
else:
    $actn = 'Editar';
    $mthds = 'formEdit';
    $dataEvento = $this->dataEvento[1];
endif;

?>
<div class="clearfix"></div>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">

        <!-- Produtos -->
        <div class="x_panel">
            <div class="x_title">
                <h2>Evento <small>[Carousel : <?=$arrModulo[1]?>]</small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                            <div class="x_content">
                            <a href="<?=URL?>modulo/index/<?=$arrModulo[3]?>"><button type="button" class="btn btn-primary">Carousel</button></a>                            
                            <!-- <a href="<?=URL?>modulo/index/<?=$arrModulo[3]?>"><button type="button" class="btn btn-primary">Modulo : <?=$arrModulo[1]?></button></a> -->
                            <a href="<?=URL?>event/<?=$mthds?>/<?=$arrModulo[0]?>"><button type="button" class="btn btn-primary"><?=$actn?></button></a>
                            </div>
                        <!--
                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class<!--="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-close"></i></a>
                            </li>
                        -->
                    </ul> 
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="tableflat">
                            <!--
                                <th>
                                    <input type="checkbox" class="tableflat">
                                </th>
                                <th>Codigo </th>
                                <th>Order </th>
                            -->
                            <th >Nome </th>
                            <th>Valor R$</th>
                            <th>Descrição </th>
                          <!--
                            <th>Categoria </th>
                            <th>Medida </th>
                            <th>Campanha </th>
                          -->
                            <th class=" no-link last">
                                <span class="nobr">Ações</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>   
                        <?php foreach($dataEvento as $arrEventos): ?>
                            
                            <tr class="tableflat">
                                <!--
                                    <td class="a-center ">
                                        <input type="checkbox" class="tableflat">
                                    </td>
                                    
                                    <td class=" "><?=$arrEventos[event_id] ?></td> 
                                    <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                                -->
                                
                                <td class=" " id="prodNome"><?=$arrEventos[event_nome] ?></td>     
                                <td class="a-right" id="valor">R$ <?=number_format($arrEventos[event_valor],2, ',', '.')?></td>
                                <td class=" "><?= substr($arrEventos[event_desc], 0, 20 ); ?> .....</td>
                               <!-- 
                                <td class=" "><?=$arrEventos[tb_campanha_camp_id] ?></td>
                                <td class=" "><?=$arrEventos[tb_medida_med_id] ?></td>
                                <td class=" "><?=$arrEventos[tb_categoria_cat_id] ?></td>
                               --> 
                                <!-- 
                                    <td class=" last"><a href="#">Editar</a>
                                    <td class=" last"><a href="#">Excluir</a> 
                                -->
                                
                                <td class="">
                                    <div class="btn-group">
                                        <a href="<?=URL?>eventos/formEdit/<?=$arrEventos[event_id]?>"><button id="inserir" type="button" class="btn btn-round btn-primary" >Editar</button></a>
                                        <!--
                                            <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Ações <span class="caret"></span>
                                            </button>
                                        
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Inserir</a>
                                                </li>
                                                <li><a href="<?=URL?>product/delete/<?=$arrProd[prod_id]?>">Excluir</a>
                                                </li>
                                               
                                                <li class="divider"></li>
                                                <li><a href="#">Retirar</a>
                                                </li>
                                            </ul>
                                        -->
                                    </div>
                                </td>
                                
                            </tr>
                            
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <br />
    <br />
    <br />

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
        
<script src="<?=URLLINK?>mask/mask.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($){
$("#date").mask("99/99/9999");
//$("#campoTelefone").mask("(999) 999-9999");
$("#horario").mask("99:99")
});
</script>