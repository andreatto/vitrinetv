<?php
if(empty($this->actn)):
    $actn = 'carregar';
    $mthds = 'inserir';
    $arrMod = $this->dataProd[0];
    $dataProd = $this->dataProd[1];
else:
    $actn = $this->actn;
    $mthds = 'editar';
    $arrMod = $this->dataUrl[0];
    $dataUrl = $this->dataUrl[1];
    $dataProd = $this->dataProd[1];
endif;

//foreach ($this->dataCart as $cart) {}
//print_r($this->dataCart);
?>
<div class="clearfix"></div>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Figura <small>Complete o cartucho Figura com 8 produtos</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="x_content">
                        <a href="<?=URL?>figura/index/<?=$arrMod[0]?>"><button type="button" class="btn btn-primary">Visualizar</button></a>
                        </div>
                        <!--
                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-close"></i></a>
                            </li>
                        -->
                    </ul> 
                <div class="clearfix"></div>
            </div>
        <!-- Inseridos na vitrine -->
        <div class="x_content">
            <form id="formCart" action="<?=URL?>figura/<?=$mthds?>/<?=$arrMod[0]?>" method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-label-left">
                <div class="form-group">
                    <label id="msg" class="control-label col-md-3 col-sm-3 col-xs-12">Produtos: <span id="spanMsg"></span></label> <br/>
                    
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <button type="button" class="btn btn-round btn-danger" onClick="javascript: removerAlbum();">
                            Retirar 
                        </button>    

                            <?php 
                                if(!empty($this->actn)):
                                    echo '<select multiple id="ids" type="hidden" name="ids[]" size="8" >';
                                    foreach ($dataUrl as $i => $prod): 
                            ?>
                                        <option value="<?=$prod[view_id]?>"  selected="selected">  
                                            <?=$i+1?>
                                        </option>
	                        <?php 
                                    endforeach; 
                                    echo ' </select>';
                                endif;
                             ?>

                        <select multiple id="cartucho" name="cartucho[]" size="8">
                            <?php 
                                if(!empty($this->actn)):
                                    foreach ($dataUrl as $prod):
                            ?>
                                <option value="<?=$prod[0]?>"  selected="selected"> 
                                    <?=$prod[prod_nome]?> | R$ <?=$prod[prod_valor]?>  
                                </option>
	                        <?php
                                    endforeach; 
                                endif;
                             ?>
                        </select>

                        <button id="btnSalvar" type="submit" class="btn btn-round btn-primary" disabled>Salvar</button>
                        

                    </div>
                </div>
            </form>
        </div>
    </div>

        <!-- Produtos -->
        <div class="x_panel">
            <div class="x_title">
                <h2>Produtos <small>Selecione 8 entre os diferentes produtos que deseja promver</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <!-- 
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        -->
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="tableflat">
                            <!--
                                <th>
                                    <input type="checkbox" class="tableflat">
                                </th>
                                <th>Codigo </th>
                                <th>Order </th>
                            -->
                            <th >Nome </th>
                            <th>Valor R$</th>
                            <th>Descrição </th>
                            <th>Categoria </th>
                            <th>Medida </th>
                            <th>Campanha </th>
                            <th class=" no-link last">
                                <span class="nobr">Ações</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>   
                        <?php foreach($this->dataProd[1] as $arrProd): ?>
                            
                            <tr class="tableflat">
                                <!--
                                    <td class="a-center ">
                                        <input type="checkbox" class="tableflat">
                                    </td>
                                    
                                    <td class=" "><?=$arrProd[prod_id] ?></td> 
                                    <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                                -->
                                
                                <td class=" " id="prodNome"><?=$arrProd[prod_nome] ?></td>     
                                <td class="a-right">R$ <?=number_format($arrProd['prod_valor'],2) ?></td>
                                <td class=" "><?= substr($arrProd[prod_desc], 0, 20 ); ?> .....</td>
                                <td class=" "><?=$arrProd[cat_nome] ?></td>
                                <td class=" "><?=$arrProd[med_nome] ?></td>
                                <td class=" "><?=$arrProd[camp_nome] ?></td>
                                <!-- 
                                    <td class=" last"><a href="#">Editar</a>
                                    <td class=" last"><a href="#">Excluir</a> 
                                -->

                                <td class="">
                                    <div class="btn-group">
                                        <button id="btnInserir" type="button" class="btn btn-round btn-primary" onClick="javascript: pushCartuchoAlbum('<?=$arrProd[prod_id] ?>', '<?=$arrProd[prod_nome] ?>', '<?=number_format($arrProd['prod_valor'],2) ?>', '<?=$arrProd[cat_nome] ?>');">Inserir</button>
                                        <!--
                                            <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Ações <span class="caret"></span>
                                            </button>
                                        
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Inserir</a>
                                                </li>
                                                <li><a href="<?=URL?>product/delete/<?=$arrProd[prod_id]?>">Excluir</a>
                                                </li>
                                            
                                                <li class="divider"></li>
                                                <li><a href="#">Retirar</a>
                                                </li>
                                            </ul>
                                        -->
                                    </div>
                                </td>

                            </tr>
                            
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <br />
    <br />
    <br />

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>