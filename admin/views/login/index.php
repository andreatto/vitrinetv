<?php echo "Teste"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Vitrine Tv</title>

  <!-- Bootstrap core CSS -->

  <link href="<?= URLLINK ?>css/bootstrap.min.css" rel="stylesheet">

  <link href="<?= URLLINK ?>fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= URLLINK ?>css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?= URLLINK ?>css/custom.css" rel="stylesheet">
  <link href="<?= URLLINK ?>css/icheck/flat/green.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?= URLLINK ?>js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body>
    <!-- 
        <body style="background-image: url(bg-2.jpg); background-size: 100%;
            background-repeat: no-repeat;" ONCONTEXTMENU="return false">
    -->
  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper" >
      <div id="login" class="animate form ">
        <section class="login_content">
            
            <h1 style="color: cornsilk">Painel Administrador </h1>
           
            <form action="login/run" method="post" enctype="multipart/form-data" role="form" >
            <h1 style="color: cornsilk">Login</h1>
            <div>
              <input name="login" type="text" class="form-control" placeholder="Login" required="" />
            </div>
            <div>
              <input name="password" type="password" class="form-control" placeholder="Senha" required="" />
            </div>
            <div>
                <?php
                    if( isset( $_GET[msg_login] ) && $_GET[msg_login] !== 0 ){
                ?>
                    <div class="x_content bs-example-popovers">
                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <strong>Erro: </strong> Usuário ou senha incorreto !
                        </div>
                    </div>
                <?php } ?>
                    
            </div>
            <div>
                <button  type="submit" class="btn btn-default submit" > Entrar </button>

            </div>
            
          </form>
          <!-- form -->
          
          <div style=" bottom: 0; padding-left: 2%; padding-top: 10%;">
            <h1 style="color: cornsilk"><i class="fa fa-eye" style="font-size: 26px;"></i> Vitrine Tv !</h1> 

            <p style="color: cornsilk">©2017 Todos os direitos reservados.</p>
        </div>
        
        </section>
        <!-- content -->
        
        
      </div>
      
    </div>
  </div>
    
        

</body>

</html>