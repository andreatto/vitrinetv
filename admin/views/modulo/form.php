<?php
$actn = $this->dataUrl[0];

$arrCart = $this->dataCart;
$arrCartBy = $this->dataCartBy;

if($actn == 'inserir'):
    $idTv = $this->dataUrl[1];
endif;

if($actn == 'editar'):
    $dataUrls = $this->dataUrl[2];
    foreach ($dataUrls as $key => $arrUrl):
        $arr[0] = $arrUrl[0];
        $arr[1] = $arrUrl[1];
        $arr[2] = $arrUrl[2];
        $arr[3] = $arrUrl[3];
    endforeach; 

    //echo '00000: <br/>';
    //$dataRelCartu = $this->dataRelCart;
    //foreach ($dataRelCartu as $key => $value):
        //$arrCartMod[0] = $value[0];
        //$arrCartMod[3] = $value[3];
        //$arrCartMod[6] = $value[6];
    //endforeach;
   //exit();

    
endif;

?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Carousel <small>SESSION [<?= ucfirst($actn) ?>]</small></h2> 

            <!--
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                    </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul> -->

            <ul class="nav navbar-right panel_toolbox">
                <?php
                if($actn != 'inserir'):
                    echo '<a href="'.URL.'modulo/form/'.$arr[3].'"><button type="button" class="btn btn-primary">Novo</button></a>';
                    echo '<a href="'.URL.'modulo/delete/'.$arr[0].'"><button type="button" class="btn btn-primary">Excluir</button></a>';
                endif;
                ?>
                
                <a href="
                <?php 
                    if($actn != 'inserir'){
                        echo URL.'modulo/index/'.$arr[3];     
                    }else{ 
                        // inserir
                        echo URL.'modulo/index/'.$idTv;
                    }
                ?>"><button type="button" class="btn btn-primary">Listar</button></a>
                
            </ul>

            <div class="clearfix"></div>

        </div>
        <div class="x_content">
            <br />
            <form 
                action=
                " 
                <?php
                if ($actn == 'inserir'):
                    echo URL.'modulo/'.$actn.'/'.$idTv;
                elseif ($actn == 'editar'):
                    echo URL.'modulo/'.$actn.'/'.$arr[0];
                endif;
                ?>
                " 
                method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-label-left">

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="nome" id="nome" class="form-control" value="<?=$arr[1]?>" placeholder="Nome">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Cartuchos Free</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php foreach ($arrCart as $cart): ?>
                        <div class="">
                            <label>
                                <input name="chckCart[]" value="<?=$cart[0]?>" type="checkbox" class="js-switch" 
                                <?php 
                                    if($actn == 'inserir'):                  
                                        if($cart[0] == 1):                                                  
                                                echo 'checked="checked" ';
                                        endif; 
                                    endif;
                                    if($actn != 'inserir'):
                                        foreach ($this->dataRelCart as $key => $value): 
                                            $cartValue[3] = $value[3];
                                                if($cartValue[3] == $cart[0]):                                                  
                                                        echo 'checked="checked" ';
                                                endif; 
                                        endforeach;
                                    endif;
                                         
                                    ?>
                                />
                                <?= $cart[1]; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
                <div class="ln_solid"></div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Cartuchos By</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php foreach ($arrCartBy as $cartBy): ?>
                        <div class="">
                            <label>
                                <input name="chck_<?=$cartBy[1]?>" value="<?=$cartBy[0]?>" type="checkbox" class="js-switch" disabled="disabled"/> <?=$cartBy[1].'   = ';?>
                                <label> R$  <?=$cartBy[5];?>
                                </label>
                            </label>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>

                    <!--
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" /> Unchecked
                            </label>
                        </div>
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" disabled="disabled" /> Disabled
                            </label>
                        </div>
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" disabled="disabled" checked="checked" /> Disabled Checked
                            </label>
                        </div>
                    -->
                    </div>
                </div>

                <!--
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Valor</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="valor" id="valor" class="form-control" value="" placeholder="Valor">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Imagem</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="file" name="imgs" id="imgs">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        
                        <select class="form-control" name="categoria">
                              
                            <option value=""</option>
                            
                        </select>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Medida</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="medida">
                            
                                <option value="" </option>
                            
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Campanha</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="campanha"    >
                            
                                <option value=""> </option>
                            
                        </select>
                    </div>
                </div>
                -->

                <!-- 
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Custom</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="select2_single form-control" tabindex="-1">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                            <option value="AZ">Arizona</option>
                            <option value="CO">Colorado</option>
                            <option value="ID">Idaho</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NM">New Mexico</option>
                            <option value="ND">North Dakota</option>
                            <option value="UT">Utah</option>
                            <option value="WY">Wyoming</option>
                            <option value="AR">Arkansas</option>
                            <option value="IL">Illinois</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="OK">Oklahoma</option>
                            <option value="SD">South Dakota</option>
                            <option value="TX">Texas</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Grouped</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="select2_group form-control">
                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                            </optgroup>
                            <optgroup label="Pacific Time Zone">
                                <option value="CA">California</option>
                                <option value="NV">Nevada</option>
                                <option value="OR">Oregon</option>
                                <option value="WA">Washington</option>
                            </optgroup>
                            <optgroup label="Mountain Time Zone">
                                <option value="AZ">Arizona</option>
                                <option value="CO">Colorado</option>
                                <option value="ID">Idaho</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NM">New Mexico</option>
                                <option value="ND">North Dakota</option>
                                <option value="UT">Utah</option>
                                <option value="WY">Wyoming</option>
                            </optgroup>
                            <optgroup label="Central Time Zone">
                                <option value="AL">Alabama</option>
                                <option value="AR">Arkansas</option>
                                <option value="IL">Illinois</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="OK">Oklahoma</option>
                                <option value="SD">South Dakota</option>
                                <option value="TX">Texas</option>
                                <option value="TN">Tennessee</option>
                                <option value="WI">Wisconsin</option>
                            </optgroup>
                            <optgroup label="Eastern Time Zone">
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="IN">Indiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="OH">Ohio</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WV">West Virginia</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Multiple</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="select2_multiple form-control" multiple="multiple">
                            <option>Choose option</option>
                            <option>Option one</option>
                            <option>Option two</option>
                            <option>Option three</option>
                            <option>Option four</option>
                            <option>Option five</option>
                            <option>Option six</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Input Tags</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input id="tags_1" type="text" class="tags form-control" value="social, adverts, sales" />
                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Checkboxes and radios
                        <br>
                        <small class="text-navy">Normal Bootstrap elements</small>
                    </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""> Option one. select more than one options
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""> Option two. select more than one options
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" checked="" value="option1" id="optionsRadios1" name="optionsRadios"> Option one. only select one option
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" value="option2" id="optionsRadios2" name="optionsRadios"> Option two. only select one option
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Checkboxes and radios
                        <br>
                        <small class="text-navy">Normal Bootstrap elements</small>
                    </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="flat" checked="checked"> Checked
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="flat"> Unchecked
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="flat" disabled="disabled"> Disabled
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="flat" disabled="disabled" checked="checked"> Disabled & checked
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" class="flat" checked name="iCheck"> Checked
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" class="flat" name="iCheck"> Unchecked
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" class="flat" name="iCheck" disabled="disabled"> Disabled
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" class="flat" name="iCheck3" disabled="disabled" checked> Disabled & Checked
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Switch</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" checked /> Checked
                            </label>
                        </div>
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" /> Unchecked
                            </label>
                        </div>
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" disabled="disabled" /> Disabled
                            </label>
                        </div>
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch" disabled="disabled" checked="checked" /> Disabled Checked
                            </label>
                        </div>
                    </div>
                </div>
                -->

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <input type="hidden" name="tela" id="tela" value="<?php if($actn=='inserir'){echo $idTv;}else{echo $arr[3];} ?>" /> 
                        <button type="reset" class="btn btn-primary">Limpar</button>
                        <button type="submit" class="btn btn-success"><?=ucfirst($actn)?></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>