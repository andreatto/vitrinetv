
<!--
        <div class="page-title"> 
            
            <div class="title_left">
                <h3>
                    Produtos
                    <small>
                        Some examples to get you started
                    </small>
                </h3>
            </div>
           
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
            
        </div> 
-->
            
    <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    
                    <div class="x_title">
                        <h2>Carousel <small>[Sessions]</small></h2>
  
                        <ul class="nav navbar-right panel_toolbox">
                             <div class="x_content">
                                <a href="<?php echo URL ?>modulo/form/<?php echo $this->idReturnTela; ?>"><button type="button" class="btn btn-primary">Novo</button></a>
                             </div>
                                <!--
                                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class<!--="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><i class="fa fa-close"></i></a>
                                    </li>
                                -->
                        </ul> 
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="x_content">
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="tableflat">
                                <!--
                                    <th>
                                        <input type="checkbox" >
                                    </th>
                                --> 

                                    <th type="hidden" ></th>
                                    <th>Id </th>
                                    <th>Nome </th>
                                    <th>Status </th>
                                    <th>Cartuchos </th>
                                    <!-- <th>Valor R$</th> -->
                                    <th class=" no-link last">
                                        <span class="nobr">Ações</span>
                                    </th>
                                </tr>
                            </thead>

                            
                            <tbody id="sortable">   
                                <?php foreach($this->dataUrl as $arr): ?>
                                    
                                    <tr class="tableflat" id="<?=$arr[mod_id] ?>">
                                    <!--
                                        <td class="a-center ">
                                            <input type="checkbox" >
                                        </td>
                                    --> 
                                    <td   type="hidden" value="<?=$arr[ord_mod_position] ?>"> </td>
                                        
                                        <td id="mod_id" value="<?=$arr[mod_id] ?>" class=" "><?=$arr[mod_id] ?>
                                        </td> 

                                        <td class=" "><?= $arr[mod_nome] ?></td>     
                                        <td class=" ">
                                        
                                            <div class=""  >
                                                
                                                <label id="chckModW">
                                                    
                                                    <input id="chckMod_<?=$arr[mod_id]?>" name="chckMod[]" value="<?=$arr[mod_ativo]?>" type="checkbox" class="js-switch" onclick="javascript: updateSequenceModule( <?=$arr[mod_id]?>, <?=$arr[mod_ativo]?> )" 
                                                        <?php
                                                            if( $arr[mod_ativo] == 0 ) {

                                                            } else if( $arr[mod_ativo] == 1 ) {
                                                                echo 'checked="checked" ';
                                                            }                                          
                                                        ?>     
                                                    />
                                                   
                                                               
                                                    <?php
                                                        if( $arr[mod_ativo] == 0 ) {
                                                                echo 'Off ';
                                                            } else if( $arr[mod_ativo] == 1 ) {
                                                                echo 'On ';
                                                            }
                                                    ?>
                                                           
                                                      
                                                </label>
                                                
                                                
                                                
                                                
                                            </div>
                                            
                                        </td>

                                        <!--  <i class="success fa fa-long-arrow-up"></i></td> --> 
                                        <td class="">
                                            <p>
                                                <?php                                               
                                                    foreach ($this->dataCart as $cart):
                                                        if($cart[mod_id] == $arr[mod_id]):
                                                            echo '<a href="'.URL.$cart[cart_nome].'/index/'.$arr[mod_id].'">'.ucwords( utf8_encode( $cart[cart_nickname] ) ) . '</a> | ';
                                                        endif; 
                                                    endforeach; 
                                                ?>
                                            </p>
                                        </td>
                                        <!-- <td class="a-right">R$ </td> -->
                                        <!-- <td class=" last"><a href="#">Editar</a>
                                        <td class=" last"><a href="#">Excluir</a> -->
                                        
                                        <td class="last">
                                            <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Ações <span class="caret"></span>
                                                </button>
                                                    <ul role="menu" class="dropdown-menu">
                                                      <li><a href="<?=URL?>modulo/formEdit/<?=$arr[0]?>">Editar</a>
                                                      </li>
                                                      
                                                      <li><a href="<?=URL?>modulo/delete/<?=$arr[0]?>">Excluir</a>
                                                      </li>
                                                      <!--
                                                      <li class="divider"></li>
                                                      <li><a href="<?=URL?>modulo/editModuloAtivarConfig/<?=$arr[0]?>"><?php if( $arr[mod_ativo] == 1 ) {  echo 'Desativar'; } else { echo 'Ativar'; } ?></a>
                                                      -->
                                                      </li>
                                                    </ul>
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    
                                <?php endforeach; ?>
                           </tbody>

                        </table>
                    </div>

                </div>
            </div>

            <br />
            <br />
            <br />

        </div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
    

<!-- Sortable drag/drop   -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
if (isset($this->js)) {
    //echo "isset" . "<br />";
    foreach ($this->js as $js) {
        //echo "foreach: " . URL .'views/' . $js . "<br />";
        //echo "<script></script>";
        echo '<script type="text/javascript" src="' . URL . 'views/' . $js . '"></script>';
    }
}
?>