
$(function () {

    $("#sortable").sortable({
        axis: 'y',
        opacity: 0.7,
        //handle: 'tableflat',
        update: function (event, ui) {
           // var url_atual = window.location;
           

            var list_sortable = $(this).sortable('toArray').toString();
            //var URL = "home/nteck763/public_html/vitrinetv/admin/controller/";
            URL = 'http://localhost:8888/vitrinetv/admin/';
            // change order in the database using Ajax
            $.ajax({
                url: URL + 'modulo/updateSequenceCarousel',
                type: 'POST',
                data: {list_order: list_sortable},
                success: function (response) {
                    //finished
                },

                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Erro! \n" +
                            " XMLHttpRequest: " + XMLHttpRequest +
                            "\n textStatus: " + textStatus +
                            "\n errorThrown: " + errorThrown);
                }

            });
        }
    });

    $("#sortable").disableSelection();

});


function updateSequenceModule( modId, target ) {
            
    URL = 'http://localhost:8888/vitrinetv/admin/';
    var sendModId = modId;
    var sendTarget = target;
    
    $.ajax({
        url: URL + 'modulo/editModuloAtivarConfig',
        type: 'POST',
        data: { modId: sendModId, target: sendTarget },
        success: function (response) {
            location.reload();
        },

        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro! \n" +
                    " XMLHttpRequest: " + XMLHttpRequest +
                    "\n textStatus: " + textStatus +
                    "\n errorThrown: " + errorThrown);
        }

    });
}