<?php

    $actn = $this->dataUrl[0];
    $arrCat = $this->dataUrl[1];
    $arrMed = $this->dataUrl[2];
    $arrCam = $this->dataUrl[3];
    $arrProd = $this->dataUrl[4];
    $arrImgn = $this->dataUrl[5];
   
if(isset($this->dataUrl)):
    foreach($arrProd as $key => $arrP):
        $arrProd[0] = $arrP[0];
        $arrProd[1] = $arrP[1];
        $arrProd[2] = $arrP[2];
        $arrProd[3] = $arrP[3];
        $arrProd[4] = $arrP[4];
        $arrProd[5] = $arrP[5];
        $arrProd[6] = $arrP[6];
    endforeach; 
    
    foreach ($arrImgn as $key => $value):
        $arrImg[0] = $value[0];
        $arrImg[1] = $value[1];
        $arrImg[2] = $value[2];
        $arrImg[3] = $value[3];
        $arrImg[4] = $value[4];
    endforeach;
endif; 

// Se a $actn for vazia recebe Inserir, senao sera inserir ou editar !
if($actn == ""){
    $actn = 'inserir';
}

//exit();
?>

<!-- Nwe FORM -->

    <div class="">
      
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                
                <div class="x_panel">
            
                    <div class="x_title">
                        
                        <h2>Produto<small>SESSION [<?=ucfirst($actn)?>]</small></h2> 

                        <ul class="nav navbar-right panel_toolbox">
                            <?php
                            if($actn != 'inserir'):
                                echo '<a href="../form"><button type="button" class="btn btn-primary">Novo</button></a>';
                                echo '<a href="../delete/'.$arrProd[0].'"><button type="button" class="btn btn-primary">Excluir</button></a>';
                            endif;
                            ?>
                            
                            <a href="<?=URL . 'product/index'?>">
                                <button type="button" class="btn btn-primary">Listar</button>
                            </a>
                        </ul>

                        <ul class="nav navbar-right panel_toolbox">
                            <li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <!--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>

                            </li>
                            -->
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div> <!-- fim x_title -->
           
                    <div class="x_content">
                    <br />
                    <form id="formProduct"
                        action=
                            " 
                                <?php
                                    if($actn == 'inserir'):
                                        echo $actn;
                                    elseif($actn == 'editar'):
                                        echo '../' . $actn . '/' . $arrProd[0];
                                    endif;
                                ?>
                            " 
                        method="post" class="form-horizontal form-label-left" enctype="multipart/form-data" role="form">

                        <!-- hidden crop params -->
                        <input type="hidden" id="x1" name="x1" />
                        <input type="hidden" id="y1" name="y1" />
                        <input type="hidden" id="x2" name="x2" />
                        <input type="hidden" id="y2" name="y2" />

                        <!-- Smart Wizard -->
                        <p>Cadastro de produtos </p>
                        
                        <div id="wizard" class="form_wizard wizard_horizontal">
                        
                            <ul class="wizard_steps">
                                <li>
                                    <a href="#step-1">
                                        <span class="step_no">1</span>
                                        <span class="step_descr"> Passo 1<br />
                                            <small>Dados </small>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-2">
                                        <span class="step_no">2</span>
                                        <span class="step_descr">
                                            Passo 2<br />
                                            <small>Categorias </small>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-3">
                                        <span class="step_no">3</span>
                                        <span class="step_descr">
                                            Passo 3<br />
                                            <small>Imagem </small>
                                        </span>
                                    </a>
                                </li>
                                
                            </ul>

                            <div id="step-1">
                        
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="name" id="name" class="form-control col-md-7 col-xs-12" value="<?=$arrProd[1]?>" placeholder="Nome" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Valor 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="valor" id="valor" class="form-control col-md-7 col-xs-12" value="<?=number_format($arrProd[2],2, ',', '.')?>" placeholder="Valor $" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descrição 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="6" maxlength="215" name="descricao" id="descricao" class="form-control col-md-7 col-xs-12" value="<?=$arrProd[3]?>" placeholder="Descrição max 133 caracteres"><?=$arrProd[3]?></textarea>
                                    </div>
                                </div>
                                

                            <!--
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="gender" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="gender" value="male"> &nbsp; Male &nbsp;
                                            </label>
                                            <label class="btn btn-primary active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="gender" value="female" checked=""> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                    </div>
                                </div>
                            -->

                            </div> <!-- fim step 1 -->

                            <div id="step-2">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Categoria 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="categoria">
                                            <?php foreach($arrCat as $arrC): ?>  
                                            <option value="<?=$arrC[0]?>" <?php if($arrC[0] == $arrProd[6]){echo "selected";} ?>> <?=$arrC[1]?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Medida 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="medida">
                                            <?php foreach($arrMed as $arrM):?>
                                                <option value="<?=$arrM[0]?>" <?php if($arrM[0] == $arrProd[5]){echo "selected";} ?>> <?=$arrM[1]?> </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Campanha</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        
                                        <select class="form-control" name="campanha">
                                            <?php foreach($arrCam as $arrCp):?>
                                                <option value="<?=$arrCp[0]?>" <?php if($arrCp[0] == $arrProd[4]){echo "selected";} ?>> <?=$arrCp[1]?> </option>
                                            <?php endforeach;?>
                                        </select>
                                        
                                    </div>
                                </div>



                            </div> <!-- fim step 2 -->

                            <div id="step-3">

                                <div class="form-group">
                                    
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Imagem 
                                        <span class="required">*</span> 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <input type="text" name="nomeImg" id="nomeImg" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" value="<?=$arrImg[1]?>" placeholder="Tamanho min 800x600 pixel" required="required">
                                        <input type="file" name="imgs" id="imgs" onchange="fileUploadProduct()" >
                                        <br />
                                        <p>Imagem: Abaixo, preserve o tamanho indicado na seleção</p>
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> 
                                         <div class="error"></div>
                                         <br />
                                    </label>
                                        <br />
                                    </div>
                                    
                                </div>


                                <div class="form-group">

                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Tamanho</label> <input type="text" id="filesize" name="filesize" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Tipo</label> <input type="text" id="filetype" name="filetype" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Dimensão</label> <input type="text" id="filedim" name="filedim" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Largura</label> <input type="text" id="w" name="w" />
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        
                                    </label>
                                    <label for="middle-name" class="control-label col-md- col-sm- col-xs-1"> 
                                        <label>Altura</label> <input type="text" id="h" name="h" />
                                    </label>
                                    
                                </div>
                            </div> <!-- fim step 3 -->
                                                    
                        </div> <!-- End SmartWizard Content -->
                            <div class="col-md-12 col-sm-12 col-xs-18">
                                <div class="step2"> 
                                    <img id="preview"  />                             
                                </div>
                            </div>
                        </div>
                    </form>
                    </div> <!-- FIM x_content -->

                </div> <!-- FIM x_panel -->

            </div>

        </div> <!-- FIM row -->

    </div> <!-- FIM class vazia -->

    
<!-- JCrop IMG -->
<!-- add styles -->
<!-- <link href="<?=URLLINK?>jcrop/css/main.css" rel="stylesheet" type="text/css" /> -->
<link href="<?=URLLINK?>jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
<!-- add scripts -->
<!-- <script src="<?=URLLINK?>jcrop/js/jquery.min.js"></script> -->
<script src="<?=URLLINK?>jcrop/js/jquery.Jcrop.min.js"></script>
<script src="<?=URLLINK?>jcrop/js/script.js"></script>

<script src="<?=URLLINK?>maskmoney/src/jquery.maskMoney.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
          $("#valor").maskMoney( { showSymbol:true, symbol:"R$", decimal:",", thousands:"." } );
    });
</script>