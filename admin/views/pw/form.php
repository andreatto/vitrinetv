<?php 
$actn = $this->arr[0];
$dataUrl = $this->dataUrl[1];
foreach($dataUrl as $value):
    $user = $value;
endforeach;

echo 'Action ' . $actn . '<br/>';

?>
<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
               
                <div class="x_content">

                    <form action="
                        <?php 
                            if(empty($actn) || $actn != 'editar'): 
                                echo 'editar/'.$user['user_id'];
                            elseif($actn == 'editar'):
                                echo '../editar/'.$user['user_id'];
                            endif;
                        ?>" method="post" class="form-horizontal form-label-left" novalidate>
                        
                        <span class="section">Perfil </span>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="" required="required" type="text" value="<?=$user['user_name']?>">
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?=$user['user_email']?>">
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label for="password" class="control-label col-md-3">Password</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required" value="<?=$user['user_pw']?>">
                            </div>
                        </div>
                        
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="reset" class="btn btn-primary">Cancel</button>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
