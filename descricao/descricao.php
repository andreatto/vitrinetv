<section id="events" class="section-events">
<!--
    <section id="home">
        <div class="home-inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text-center">
                        <h3 style="color:black; font-size:50px;">Preparadas e Temperados</h3>
                        <div class="flexslider intro-slider">
                            <ul class="slides" style="color:black;">
                                <li style="color:black; font-size:30px;">
                                    Carnes frescas e selecionadas
                                </li>
                                <li style="color:black; font-size:30px;">
                                    Variedades, temperadas e pecas
                                </li>
                                <li style="color:black; font-size:30px;">
                                    Qualidade no maneijo da carne
                                </li>
                            </ul>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
-->

    <div class="container">
        <div class="row">
            <?php

                //$link = mysql_connect( '108.167.188.55', 'nteck763_admin', 'smartpeeper1@' ) or die ( "Erro ao conectar servidor Model_telas" );
                //$db = mysql_select_db( "nteck763_smartpeeper" , $link ) or die ( "Erro ao conectar Model rodizio" );
                
                include_once 'con/Database.php';
                $db = new Database();
                $obj = $db->conectarApp();
                //$db->testar();
                
                $tela = $_REQUEST["tela"];
                $customer = $_REQUEST["customer"];
                $modulo = $_REQUEST["modulo"];
                
                $posicao = $_REQUEST["posicao"] + 1;
                $arrCart = $_REQUEST["arrCart"];

                $sqlProd = " SELECT 
                                p.prod_nome, p.prod_valor, p.prod_desc,
                                m.med_nome,
                                c.cat_nome,
                                cph.camp_nome,
                                i.img_name
                                    FROM tb_cadProduto as p
                                        inner join tb_medida as m ON m.med_id = p.tb_medida_med_id
                                        inner join tb_img as i ON i.tb_cadProduto_prod_id = p.prod_id
                                        inner join tb_categoria as c ON p.tb_categoria_cat_id = c.cat_id
                                        inner join tb_campanha as cph ON p.tb_campanha_camp_id = cph.camp_id
                                        inner join tb_view as v
                                        WHERE
                                            v.mod_id = '$modulo'
                                        ANd
                                            v.tela = '$tela'
                                        AND 
                                            v.cust_id = '$customer'
                                        AND 
                                            v.cart_id = 5
                                        AND 
                                            p.prod_id = v.prod_id
                                        ORDER BY p.prod_nome asc
                        ";
                $qryProd = $obj->query( $sqlProd );

                while ($arrProd = $qryProd->fetch_array( MYSQLI_BOTH )) {
            ?>
            <div class="col-sm-4 margin-b-30" style="margim-left:10px; color:dimgray">
                <div class="event-thumb">
                    <img src="admin/img/produtos/<?=$arrProd['img_name'];?>" alt="" class="img-responsive" >
                </div><!--event thumbnail-->
                <div class="event-desc clearfix" style="padding-top:5px; padding-bottom:0px; border: 1px solid lightgrey;">
                    <h2 style="padding-top:0px;"><?=substr( $arrProd["prod_nome"], 0, 19); ?></h2>
                    <?php
                        $total = strlen( (string) $arrProd["prod_valor"] );
                        //echo $total;
                        if( $total < 4 ) {  ?>
                            <span class="date"><h1>R$ <?=number_format($arrProd["prod_valor"],2, ',', '.');?></h1></span>
                    <?php } ?>

                    <?php
                        if( $total > 3 ) {  ?>
                            <span class="date"><h2>R$ <?=number_format($arrProd["prod_valor"],2, ',', '.');?></h2></span>
                    <?php } ?>
                    
                    <div style="color: chocolate; padding-top:5px;" >
                    <?= ucfirst ( strtolower( substr( $arrProd["cat_nome"], 0, 10 ) ) ); ?> / <?= ucfirst ( strtolower( substr( $arrProd["med_nome"], 0, 5 ) ) ); ?> / <?= ucfirst ( strtolower( substr( $arrProd["camp_nome"], 0, 8 ) ) ); ?>
                    </div>  
                    
                        <div>
                            <p style="font-size: 1.25em; padding-top:0px; padding-bottom:0px;">
                                <?php 
                                    //substr( $arrProd["prod_desc"], 0, 136 );
                                    echo substr($arrProd["prod_desc"], 0, 225);
                                ?>
                            </p>  

                        </div> 
                            
                    
                    <!-- <a href="#" class="fa fa-angle-right"><i>Confira + produtos </i></a> -->
                </div>
            </div><!--event entry column-->
            <?php } ?>
        </div>
    </div>

</section><!--events-->
<section id="foo"></section>

<script type="text/javascript">
    (function () {
        setTimeout(enviar, 15000);
    }());

    function enviar() {
        getModulo( "<?= $posicao ?>" ,  "<?= $tela ?>", "<?= $customer ?>" );
    }
</script>