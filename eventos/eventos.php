
<section id="events" class="section-events">
    <div class="container">

        <!--
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 center-title text-center">
                <h3>Eventos - Jantares </h3>
                <span class="center-line"></span>
                 <p>
                    Checkout the upcoming events
                 </p> 
                
            </div>
        </div>
        --> 
        <div class="row"> 
        
        <?php
        //$link = mysql_connect( '108.167.188.55', 'nteck763_admin', 'smartpeeper1@' ) or die ( "Erro ao conectar servidor Model_telas" );
        //$db = mysql_select_db( "nteck763_smartpeeper" , $link ) or die ( "Erro ao conectar Model rodizio" );

        include_once 'con/Database.php';
        $db = new Database();
        $obj = $db->conectarApp();
        //$db->testar();

        $tela = $_REQUEST["tela"];
        $customer = $_REQUEST["customer"];
        $modulo = $_REQUEST["modulo"];

        $posicao = $_REQUEST["posicao"] + 1;
        $arrCart = $_REQUEST["arrCart"];

        $sqlEvento = " 
                        SELECT  
                            even.event_id, 
                            even.event_nome, 
                            even.event_valor, 
                            even.event_horario, 
                            even.event_data, 
                            even.event_datasemana, 
                            even.event_desc,
                            i.img_name
                        FROM 
                            tb_events as even
                        inner join 
                            tb_view as v on v.event_id = even.event_id
                        inner join 
                            tb_img as i on i.tb_events_event_id = even.event_id
                        AND
                            v.mod_id = '$modulo'
                        AND
                            v.cust_id = '$customer'
                        AND 
                            v.cart_id = 7
                        ORDER 
                            by v.view_id
                    ";
        $qryEvento = $obj->query($sqlEvento);

        while ($arrEvento = $qryEvento->fetch_array(MYSQLI_BOTH)) {

            //$dateMonth = 
            ?>

            <div class="col-sm-4 margin-b-30 " >
                <div class="event-thumb">
                    <img src="admin/img/eventos/<?= $arrEvento['img_name']; ?>" alt="" class="img-responsive">
                    <!-- 
                    <div class="overlay">
                        <div class="overlay-inner">
                            <h4></h4>
                            <p>Lorem ipsum dolor</p>
                        </div>
                    </div>
                    -->
                </div><!--event thumbnail-->
                <div class="event-desc clearfix "  style="border: 1px solid lightgrey; color:dimgray;">
                    <h3><b><?= $arrEvento['event_nome']; ?></h3></b>

                    <i class="fa fa-calendar-plus-o" style="color: chocolate;"></i>
                    <a href="#" style="font-size: 17px; color: chocolate; padding-left: 7px;"> <b> <?= $arrEvento['event_datasemana']; ?> </b></a> 

                    <i class="fa fa-clock-o" style="color: chocolate; padding-left: 20px;"></i> 
                    <span class="" style="color: chocolate; font-size: 16px;"><b> <?= $arrEvento['event_horario']; ?> </b></span> <br />


                    <i class="fa fa-calendar" style="color: chocolate; "></i> 
                    <span class="" style="font-size: 18px; color: chocolate;"><b> <?= $arrEvento['event_data']; ?> </b></span> 

                    <i class="" style="color: chocolate; padding-left: 8px;"></i>
                    <span class="date" style="font-size: 25px;">R$<b> <?= number_format($arrEvento['event_valor'], 2, ',', '.'); ?> </b></span> <br />

                    <div>
                        <p style="font-size: 1.25em; padding-top:0px; padding-bottom:0px;">
                            <?= substr($arrEvento['event_desc'], 0, 190); ?>
                        </p>  

                    </div>

                    <!-- <a href="#">View Details <i class="fa fa-angle-right"></i></a> -->
                </div>
            </div><!--event entry column-->
        <?php } ?>
        <!--
            <div class="col-sm-4 margin-b-30">
                <div class="event-thumb">
                    <img src="img/resto/img-5.jpg" alt="" class="img-responsive">
                    <div class="overlay">
                        <div class="overlay-inner">
                            <h4>New Restaurant Opening</h4>
                            <p>Lorem ipsum dolor</p>
                        </div>
                    </div>
                </div><!--event thumbnail
                <div class="event-desc clearfix">
                    <h4>New Restaurant Opening</h4>

                    <span class="date">26 August 2015</span><span class="time">11:15 AM</span>
                    <p>
                        Nam pharetra diam eu dolor vestibulum volutpat.
                    </p>  
                    <a href="#">View Details <i class="fa fa-angle-right"></i></a>
                </div>
            </div><!--event entry column
        -->
    </div>
</div>
</section><!--events-->


<!--
<section id="events" class="section-events">

    <section id="home">
        <div class="home-inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text-center">
                        <h3 style="color:black; font-size:50px;">Preparadas e Temperados</h3>
                        <div class="flexslider intro-slider">
                            <ul class="slides" style="color:black;">
                                <li style="color:black; font-size:30px;">
                                    Carnes frescas e selecionadas
                                </li>
                                <li style="color:black; font-size:30px;">
                                    Variedades, temperadas e pecas
                                </li>
                                <li style="color:black; font-size:30px;">
                                    Qualidade no maneijo da carne
                                </li>
                            </ul>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
 
</section>
-->


<script type="text/javascript">
    (function () {
        setTimeout(enviar, 15000);
    }());

    function enviar() {
        getModulo("<?= $posicao ?>", "<?= $tela ?>", "<?= $customer ?>");
    }
</script>
