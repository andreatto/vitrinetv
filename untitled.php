        <!-- JCrop IMG -->
        <!-- add styles -->
	<!-- <link href="<?=URLLINK?>jcrop/css/main.css" rel="stylesheet" type="text/css" /> -->
	<link href="<?=URLLINK?>jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
	<!-- add scripts -->
	<!-- <script src="<?=URLLINK?>jcrop/js/jquery.min.js"></script> -->
	<script src="<?=URLLINK?>jcrop/js/jquery.Jcrop.min.js"></script>
	<script src="<?=URLLINK?>jcrop/js/script.js"></script>


<?php

// Pegando e atribuindo valores a action
$actn = $this->dataUrl[0];
$dataUrl = $this->dataUrl[1];
$arrImgn = $this->dataUrl[2];

if(isset($this->dataUrl)):
    foreach($dataUrl as $key => $value):
        $arr[0] = $value[0];
        $arr[1] = $value[1];
        $arr[2] = $value[2];
        $arr[3] = $value[3];
        $arr[4] = $value[4];
    endforeach; 

    foreach ($arrImgn as $key => $value):
        $arrImg[0] = $value[0];
        $arrImg[1] = $value[1];
        $arrImg[2] = $value[2];
        $arrImg[3] = $value[3];
        $arrImg[4] = $value[4];
    endforeach;

endif; 

if($actn == '') {
    $actn = 'inserir';
}
?>


    <div class="">
      
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                
                <div class="x_panel">
            
                    <div class="x_title">
                        <h2>Usuário <small>SESSION [<?= ucfirst($actn) ?>]</small></h2> 


                        <ul class="nav navbar-right panel_toolbox">
                            <?php
                            if($actn != 'inserir'):
                                echo '<a href="../form"><button type="button" class="btn btn-primary">Novo</button></a>';
                                echo '<a href="../delete/'.$arr[0].'"><button type="button" class="btn btn-primary">Excluir</button></a>';
                            endif;
                            ?>
                            
                            <a href="
                            <?php
                                if($actn != 'inserir'){
                                    echo'../index';     
                                }else{
                                    echo'index';
                                }
                            ?>"><button type="button" class="btn btn-primary">Listar</button></a>
                        </ul>

                        <ul class="nav navbar-right panel_toolbox">
                            <li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <!--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>

                            </li>
                            -->
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div> <!-- fim x_title -->
           
                    <div class="x_content">
                    <br />
                    <form  action=
                        " 
                            <?php
                                if($actn == 'inserir'):
                                    echo $actn;
                                elseif($actn == 'editar'):
                                    echo '../' . $actn . '/' . $arr[0];
                                endif;
                            ?>
                        " 
                        method="post" class="form-horizontal form-label-left" enctype="multipart/form-data" role="form">

                        <!-- hidden crop params -->
                        <input type="hidden" id="x1" name="x1" />
                        <input type="hidden" id="y1" name="y1" />
                        <input type="hidden" id="x2" name="x2" />
                        <input type="hidden" id="y2" name="y2" />

                        <!-- Smart Wizard -->
                        <p>Cadastro de usuários </p>
                        
                        <div id="wizard" class="form_wizard wizard_horizontal">
                        
                            <ul class="wizard_steps">
                                <li>
                                    <a href="#step-1">
                                        <span class="step_no">1</span>
                                        <span class="step_descr"> Passo 1<br />
                                            <small>Dados </small>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-2">
                                        <span class="step_no">2</span>
                                        <span class="step_descr">
                                            Passo 2<br />
                                            <small>Imagem </small>
                                        </span>
                                    </a>
                                </li>
                                
                                
                            </ul>

                            <div id="step-1">
                        
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="name" id="name" class="form-control col-md-7 col-xs-12" value="<?=$arr[1]?>" placeholder="Nome" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">E-Mail 
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" name="email" id="email" class="form-control col-md-7 col-xs-12" value="<?=$arr[2]?>" placeholder="E-Mail" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Senha 
                                        <span  class="required">*</span> 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="pw" id="pw" class="form-control col-md-7 col-xs-12" placeholder="Senha" required="required" >
                                    </div>
                                </div>
                            <!--
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="gender" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="gender" value="male"> &nbsp; Male &nbsp;
                                            </label>
                                            <label class="btn btn-primary active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="gender" value="female" checked=""> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                    </div>
                                </div>
                            -->

                            </div> <!-- fim step 1 -->

                            <div id="step-2">

                                <div class="form-group">
                                    
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Imagem 
                                        <span class="required">*</span> 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <input type="text" name="nomeImg" id="nomeImg" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" value="<?=$arrImg[1]?>" placeholder="Tamanho maximo 220x200 pix" required="required">
                                        <input type="file" name="imgs" id="imgs" onchange="fileUploadUserCust()" >
                                        <br />
                                        <p>Imagem: Mantenha o tamanho indicado na selecao</p>
                                        <br />
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> 
                                         <div class="error"></div>
                                         <br />
                                        <div class="step2">
                                            <div class="info">
                                                <label>Tamanho</label> <input type="text" id="filesize" name="filesize" />
                                                <label>Tipo</label> <input type="text" id="filetype" name="filetype" />
                                                <label>Dimensão</label> <input type="text" id="filedim" name="filedim" />
                                                <label>Largura</label> <input type="text" id="w" name="w" />
                                                <label>Altura</label> <input type="text" id="h" name="h" />
                                            </div>
                                            
                                        </div>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="step2"> 
                                            <img id="preview"  />                             
                                        </div>
                                    </div>
                                    
                                </div>
                                    
                             
                            </div> <!-- fim step 2 -->

                        
                        </div> <!-- End SmartWizard Content -->


                    </form>
                    </div> <!-- FIM x_content -->

                </div> <!-- FIM x_panel -->

            </div>

        </div> <!-- FIM row -->

    </div> <!-- FIM class vazia -->
