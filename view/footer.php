<footer class="footer text-center">
    <div class="container">
         <section class="our-chefs">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-4 margin-b-30">
                        <div class="chef-box">
                            
                            <div class="chef-desc">
                                <h4>Juliyana</h4>
                                <em>Master Chef</em>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisc Pellentesque vel enim.
                                </p>
                            </div>

                        </div><!--chef desc-->
                    </div><!--chef column-->
                    <div class="col-sm-4 ">
                        <div class="chef-box">
                            
                            <div class="chef-desc">
                                <iframe scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="150" height="170" src="http://selos.climatempo.com.br/selos/MostraSelo.php?CODCIDADE=558&SKIN=laranja"></iframe>                         </div>
                        </div><!--chef desc-->
                    </div><!--chef column-->
                    <div class="col-sm-4 margin-b-30">
                        <div class="chef-box">
                           
                            <div class="chef-desc">
                                <h4>Acougue Carne fresca.</h4>
                                <em>Master Acougue</em>
                                <p>
                                    <span>Acougue Carne fresca. &copy; 2016 - Copyright © gabriel.andreatto.1@gmail.com</span>
                                </p>
                            </div>

                        </div><!--chef desc-->
                    </div><!--chef column-->
                </div>
            </div>
        </section><!--Chefs section-->
    </div>
    
</footer>

      <!-- Funcao para carousel de paginas -->
        <script src="js/fncProjeto.js" type="text/javascript"></script>
     
        <!--contact modal end-->
        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <script src="js/funcoes.js"></script>
        <!-- Bootstrap js-->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!--easing plugin for smooth scroll-->
        <script src="js/jquery.easing.1.3.min.js" type="text/javascript"></script>
        <script src="js/jquery.backstretch.min.js" type="text/javascript"></script>
        <!--flex slider plugin-->
        <script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
        <!--owl carousel slider js-->
        <script src="js/owl.carousel.min.js" type="text/javascript"></script>
        <!-- jQuery UI -->
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <!--pace plugin-->
        <script src="js/pace.min.js" type="text/javascript"></script>

        <!--popup js-->
        <script src="lightbox2/dist/js/lightbox.min.js" type="text/javascript"></script>
        
        <!--restaurant custom js-->
        <script src="js/restaurant-custom.js" type="text/javascript"></script>
    </body>
</html>
<!-- Localized -->