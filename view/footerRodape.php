<footer class="footer text-center">
    <div class="container">
         <section class="our-chefs">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-4 margin-b-30">
                        <div class="chef-box">
                            
                            <div class="chef-desc">
                                <h4>Juliyana</h4>
                                <em>Master Chef</em>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisc Pellentesque vel enim.
                                </p>
                            </div>

                        </div><!--chef desc-->
                    </div><!--chef column-->
                    <div class="col-sm-4 ">
                        <div class="chef-box">
                            
                            <div class="chef-desc">
                                <iframe scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="150" height="170" src="http://selos.climatempo.com.br/selos/MostraSelo.php?CODCIDADE=558&SKIN=laranja"></iframe>                         </div>
                        </div><!--chef desc-->
                    </div><!--chef column-->
                    <div class="col-sm-4 margin-b-30">
                        <div class="chef-box">
                           
                            <div class="chef-desc">
                                <h4>Acougue Carne fresca.</h4>
                                <em>Master Acougue</em>
                                <p>
                                    <span>Acougue Carne fresca. &copy; 2016 - Copyright © gabriel.andreatto.1@gmail.com</span>
                                </p>
                            </div>

                        </div><!--chef desc-->
                    </div><!--chef column-->
                </div>
            </div>
        </section><!--Chefs section-->
    </div>
    
</footer>