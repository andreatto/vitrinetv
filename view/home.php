<section class="opening-hours">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 center-title text-center">
                
                <p style="color:black; font-size:300%;">
                    Horario de atendimento 
                </p>
                <span class="center-line"></span>
                <p style="color:black; font-size:300%;">
                    Ligue e agende uma reserva !
                </p>

            </div>
        </div><!--section title--> 
        <div class="row">
            <div class="col-sm-5 col-sm-offset-1">
                <div class="opening-hours-box">
                    <h1>Segunda a sabado</h1>
                    <h3>08:00 AM - 19:00 PM</h3>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="opening-hours-box">
                    <h1>Domingo</h1>
                    <h3>08:00 AM - 12:00 PM</h3>
                </div>
            </div>
        </div>
        <div class="row margin-b-30">
            <div  class="col-sm-12 text-center">
                <a class="btn btn-lg btn-yellow" style="color:black;">Telefone: +55 3641 XXXX</a>
            </div>
        </div>
    </div>
</section><!--section opening hours-->


<script type="text/javascript">
    (function () {
        document.getElementById('header_id').style.display="block";
        setTimeout(carousel, 5000);
    }());
</script>

